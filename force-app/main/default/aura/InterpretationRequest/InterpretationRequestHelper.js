({
	getInterpretationRequest : function(component, event) {

		// call the apex class method 
		var action = component.get("c.getRecord");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var storeResponse = response.getReturnValue();
				// if storeResponse size is equal null ,display No Records Found... message on screen.                }
				if (storeResponse == null) {

					component.set("v.showSpinner", false);
					component.set("v.message",  $A.get("{!$Label.c.Inter_Req_Mes}"));

				} else {

					component.set("v.interpretationRequestId", storeResponse); 
					component.set("v.showSpinner", false);
					component.set("v.hiddenStartButton", true); 
				}
			}
		});
		// enqueue the Action  
		$A.enqueueAction(action);
	},
	updateRecord : function(component) {

		component.set("v.showSpinner", true);

		// call the apex class method 
		var action = component.get("c.updateRecord");
		var recordId = component.get('v.interpretationRequestId');
		var isStart =  component.get('v.isStart');
		//set params
		action.setParams({
			'recordId': recordId,   
			'isStart': isStart
		});
		
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var storeResponse = response.getReturnValue();
				if(isStart == true){

					component.set("v.isStart", false); 
					component.set("v.interpretationRequestId", storeResponse); 
					component.set("v.hiddenStartButton", false); 
					component.set("v.hiddenEndButton", true); 
					component.set("v.showSpinner", false);

				}else{
					component.set("v.isStart", true); 

					event = $A.get("e.force:navigateToSObject");
					event.setParams({
						"recordId": storeResponse,
						"slideDevName": "detail"
					});
					event.fire();
				}

				var storeResponse = response.getReturnValue();
				
			}
		});
		$A.enqueueAction(action);
	}
})