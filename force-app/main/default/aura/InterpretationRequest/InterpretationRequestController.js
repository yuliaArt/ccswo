({
	doInit: function (component, event, helper) {
		helper.getInterpretationRequest(component, helper, event);
	},
	handlerOnSubmit: function(component, event, helper){
		helper.updateRecord(component, helper, event);
	}
})