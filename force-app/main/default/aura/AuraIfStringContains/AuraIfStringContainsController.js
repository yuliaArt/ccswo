({
	doInit: function(component) {
        var container = component.get('v.container');
        var substring = component.get('v.substring');

        console.log('in IfComponent. container: ' + container);
        console.log('in IfComponent. SearchTerm: ' + substring);
        var lowerCaseContainer = container.toLowerCase();
        var substringLowerCase = substring.toLowerCase();
        var substringIndex = lowerCaseContainer.indexOf(substringLowerCase);

        if(substringIndex !== -1 || substring === ''){
            component.set('v.condition', true);
        }else{
            component.set('v.condition',false);
        }
    }
})