({
	doInit: function (component, event, helper) {
		helper.getFieldType(component);
	},

	valueByHeared: function(component, event, helper){
		helper.valueByHeared(component);
	},

	recordValue: function(component, event, helper){
		helper.recordValue(component);
	},

})