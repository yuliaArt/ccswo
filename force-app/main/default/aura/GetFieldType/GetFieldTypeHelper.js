({
	getFieldType: function (component) {
		var record = component.get("v.record");
		var fieldApiName =  component.get("v.fieldApiName");

		if(record != null){
			component.set("v.recordId", record.Id);
			component.set("v.recordValue", record[fieldApiName]);
		}

		var action = component.get("c.getFieldType");
		action.setParams({
			"objectName": component.get("v.objectName"),
            "fieldName": component.get("v.fieldApiName")
		});
		action.setCallback(this, function(a) {
			switch(a.getState()){
				case "SUCCESS":
					var response = a.getReturnValue();
					component.set('v.fieldType', response);
				 break;
                case "ERROR":
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Error",
                        "title": "Error",
                        "message": "Unable to load field type"
                    });
                    resultsToast.fire();
                    console.log('Error in calling getFieldType:\n' +
                        a.getError()[0].message);
                    break;
                default:
                    console.log('Unhandled problem in calling getFieldType.');
            }
        });
        $A.enqueueAction(action);
		
    },

	valueByHeared: function(component){
		var fieldApiName =  component.get("v.fieldApiName");
		var value = component.get("v.recordValue");
		
		var event = $A.get("e.c:DailyAttendanceChangeValueEvent");
		event.setParams({
			"fieldName":fieldApiName,
			"value":value
		});
		event.fire();
	},
	recordValue: function(component ){
		var fieldApiName =  component.get("v.fieldApiName");
		var value = component.get("v.recordValue");
		var recordId = component.get("v.recordId");
		
		var ev = $A.get("e.c:DailyAttendanceChangeValueEvent");
		ev.setParams({
			"fieldName":fieldApiName,
			"value":value,
			"recordId": recordId
		});
		ev.fire();
	}
})