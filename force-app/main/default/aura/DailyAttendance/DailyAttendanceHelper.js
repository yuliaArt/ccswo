/*
*Provisio Partners
* Author : Pulkit Nautiyal
* Description : LEX component helper
* Created Date : March 26th 2018.
*/
({
    getAttendanceRecords: function (component) {
		component.set('v.toggle',true);
        var action = component.get('c.serverGetAttendanceRecords');
		
        action.setParams({
            'classId': component.get('v.recordId'),
            'dateString': component.get('v.date')
        });

        action.setCallback(this, function (a) {
            switch (a.getState()) {
                case "SUCCESS":
					console.log('success');
                    var response = a.getReturnValue();
					
                    component.set('v.attendanceList', response);
					if(response == null){
					}
						component.set('v.toggle',false);
                    break;
                case "ERROR":
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Error",
                        "title": "Error",
                        "message": "Unable to load attendances"
                    });
                    resultsToast.fire();
                    console.log('Error in calling serverGetAttendanceRecords:\n' +
                        a.getError()[0].message);
					component.set('v.toggle',false);
                    break;
                default:
                    console.log('Unhandled problem in calling serverGetAttendanceRecords.');
            }
        });
        $A.enqueueAction(action);
    },

	getAttendanceListFieldName: function(component){
		var attendanceListFieldNameHeader = component.get('v.attendanceListFieldNameHeader');
		var attendanceListFieldName = component.get('v.attendanceListFieldName');
		var divWidth = component.get('v.divWidth');

		var action = component.get('c.dailyAttendanceSettings');
		action.setCallback(this, function(a){
			switch(a.getState()){
				case "SUCCESS":
					var response = a.getReturnValue();
					for(var i in response){
						if(response[i].IsHeader__c){
							attendanceListFieldNameHeader.push(response[i]);
						} else {
							attendanceListFieldName.push(response[i]);
						}
					}
					var width = attendanceListFieldName.length * 100;
					var newWidth = divWidth+width;
					if(newWidth >1200){
						newWidth = 1200;
					}
					component.set('v.divWidth', newWidth);
					component.set('v.attendanceListFieldName', attendanceListFieldName);
					component.set('v.attendanceListFieldNameHeader', attendanceListFieldNameHeader);
					component.set('v.toggle',false);
					
				break;
                case "ERROR":
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Error",
                        "title": "Error",
                        "message": "Unable to load daily Attendance settings"
                    });
                    resultsToast.fire();
                    console.log('Error in calling dailyAttendanceSettings:\n' +
                        a.getError()[0].message);
                    break;
                default:
                    console.log('Unhandled problem in calling dailyAttendanceSettings.');
            }
		});
		$A.enqueueAction(action);
	},
    updateAttendanceRecords: function (component) {
        var action = component.get("c.serverUpdateAttendanceRecord");
        action.setParams({
            "attendanceUpdate": component.get('v.attendanceList')
        });

        action.setCallback(this, function (a) {
            if(a.getState() == "SUCCESS") {
                console.log('In success ');
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "type": "Success",
                    "title": "Success",
                    "message": "Records were updated!"
                });
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
            } else if (a.getState() == "Error"){
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Error",
                        "title": "Error",
                        "message": "Unable to update records."
                    });
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    console.log('Error in calling serverUpdateAttendanceRecord:\n' +
                                a.getError()[0].message);
             } else {
                   console.log('Unhandled problem in calling serverUpdateAttendanceRecord.');
             }
        });
        $A.enqueueAction(action);
    },
    
    getToday: function () {
        var today = new Date();
        var monthDigit = today.getMonth() + 1;
        if (monthDigit <= 9) {
            monthDigit = '0' + monthDigit;
        }
        var dayDigit = today.getDate();
        if (dayDigit <= 9) {
            dayDigit = '0' + dayDigit;
        }
        return today.getFullYear() + '-' + monthDigit + '-' + dayDigit;
    },
		
    setValueByColumn: function (component, event) {
        var templist = component.get("v.attendanceList");
		var value = event.getParam('value');
		var fieldApiName = event.getParam("fieldName");
		var recordId = event.getParam("recordId");
		
		if (recordId == undefined){
			for(var i=0; i< templist.length; i++){
				templist[i][fieldApiName] = value;              
			}       
			component.set('v.attendanceList', templist);
		}
    },

	setValue: function(component, event){
		var value = event.getParam('value');
		var fieldApiName = event.getParam("fieldName");
		var recordId = event.getParam("recordId");
		var templist = component.get("v.attendanceList");

		for(var i=0; i< templist.length; i++){
			var listItemId = templist[i]['Id'];
			if(listItemId==recordId){
				templist[i][fieldApiName] = value;              
			}
        }       
		component.set('v.attendanceList', templist);
	},

});