/*
*Provisio Partners
* Author : Pulkit Nautiyal
* Description : LEX component controller
* Created Date : March 26th 2018.
*/

 
({  
    // doInit method
    doInit: function (component, event, helper) {
        component.set('v.date', new Date().toISOString());
		//component.set('v.date', '2019-02-16');
		
        helper.getAttendanceRecords(component);
	
		helper.getAttendanceListFieldName(component);
		

    },
	getAttendanceListFieldName : function(component, event, helper){
		helper.getAttendanceListFieldName(component);
	},

    //Method will be called after date selection
    getAttendanceRecords : function(component, event, helper) {
        helper.getAttendanceRecords(component);
    },

    //Method called when submit for saving record
    handleClick : function(component, event, helper) {
        helper.updateAttendanceRecords(component,event,helper);
    },
  
    //Method called on cancel button clicked
    closeWindow : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
	
    setValueByColumn:function(component, event, helper) {
       helper.setValueByColumn(component, event);
    },
	
	setValue : function(component, event, helper){
		helper.setValue(component, event);
	},

});