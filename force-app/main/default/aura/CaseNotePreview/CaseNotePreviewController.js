({
	// doInit method
    doInit: function (component, event, helper) {
		helper.getStartData(component, event, helper);		
    },

    //Method called when submit for saving record
    handleClick : function(component, event, helper) {
        helper.getRecords(component,event,helper);
    },
  
    //Method called on cancel button clicked
    closeWindow : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    //Method called onchange Dates
    handlerFilter: function(component, event, helper){
        helper.filterWithDates(component,event,helper);
    }
})