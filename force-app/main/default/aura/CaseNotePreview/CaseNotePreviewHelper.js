({
	getStartData: function(component, event, helper){
		component.set('v.spinner',true);
		var recordId = component.get('v.recordId');

		var action = component.get('c.getStartData');
        action.setParams({
			'recordId': component.get('v.recordId'),
			'filter' : false,
            'startDate': null,
            'endDate': null
        });
        action.setCallback(this, function (a) {
            switch (a.getState()) {
                case "SUCCESS":
                    var response = a.getReturnValue();
		
                    component.set('v.recordTypesList', response.recordTypesList);
					component.set('v.startDate',response.minDate);
					component.set('v.endDate',response.maxDate);
					component.set('v.minDate',response.minDate);
					component.set('v.maxDate',response.maxDate);

					component.set('v.printPageSettings', response.cleanPrintPageFieldSettings);

					var recordWrapper = [];	
					if(response.recordsWrapper != null){
						for(var i=0; i<response.recordsWrapper.length; i++){
							var recordName = response.recordsWrapper[i].recordName;
							var map = response.recordsWrapper[i].recordsFieldWrapperByRowNumber;
							var mapWarpper=[];
							for(var key in map){
								mapWarpper.push({value:map[key], key:key});
							}
							//recordWrapper.push({parent: mapWarpper});

							var children = response.recordsWrapper[i].childRecordWrapper;
							var listOfChild = [];
							if(children != null && children.length > 0){
								for (var j=0; j< children.length; j++){
									var childMap = children[j].recordsFieldWrapperByRowNumber;
									var listOfChildMapWrapper = [];
									for(var childKey in childMap){
										listOfChildMapWrapper.push({value:childMap[childKey], key: childKey});
									}
									listOfChild.push(listOfChildMapWrapper);
								}
							}
	 
							//recordWrapper.push({children: listOfChild});						
							recordWrapper.push({parent:mapWarpper, children:listOfChild});
						}
					}
				
					component.set('v.dataListWrapper',recordWrapper);
                    
					component.set('v.spinner',false);
                    break;
                case "ERROR":
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Error",
                        "title": "Error",
                        "message": "Unable to load Data"
                    });
                    resultsToast.fire();
					component.set('v.spinner',false);
                    break;
                default:
            }
        });
        $A.enqueueAction(action);

	},
	getRecords: function (component, event, helper) {
		var recordId = component.get('v.recordId');
		var startDate = component.get('v.startDate');
		var endDate = component.get('v.endDate');
		var selectedRT = component.get('v.sellectedRecordType');

		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
		  "url": "/apex/CaseNotePrint?id="+recordId+"&selectedRT="+selectedRT+"&date1="+startDate+"&date2="+endDate,
		  "isredirect": "true"
		});
		urlEvent.fire();

	},
	filterWithDates: function(component, event, helper){

		var recordId = component.get('v.recordId');
        var startDate = component.get('v.startDate');
        var endDate = component.get('v.endDate');
        
        var action = component.get('c.getFilterData');
        action.setParams({
            'recordId': component.get('v.recordId'),
            'filter' : true,
            'startDate': startDate,
            'endDate': endDate
        });
        action.setCallback(this, function (a) {
            switch (a.getState()) {
                case "SUCCESS":
					
                    var response = a.getReturnValue();

                    component.set('v.recordTypesList', response.recordTypesList);
					component.set('v.minDate',response.minDate);
					component.set('v.maxDate',response.maxDate);
					component.set('v.printPageSettings', response.cleanPrintPageFieldSettings);

					var recordWrapper = [];	
					if(response.recordsWrapper != null){
						for(var i=0; i<response.recordsWrapper.length; i++){
							var recordName = response.recordsWrapper[i].recordName;
							var map = response.recordsWrapper[i].recordsFieldWrapperByRowNumber;
							var mapWarpper=[];
							for(var key in map){
								mapWarpper.push({value:map[key], key:key});
							}

							var children = response.recordsWrapper[i].childRecordWrapper;
							var listOfChild = [];
							if(children != null && children.length > 0){
								for (var j=0; j< children.length; j++){
									var childMap = children[j].recordsFieldWrapperByRowNumber;
									var listOfChildMapWrapper = [];
									for(var childKey in childMap){
										listOfChildMapWrapper.push({value:childMap[childKey], key: childKey});
									}
									listOfChild.push(listOfChildMapWrapper);
								}
							}
	 						
							recordWrapper.push({parent:mapWarpper, children:listOfChild});
						}
					}
					
					component.set('v.dataListWrapper',recordWrapper);
					
					component.set('v.spinner',false);
                    break;
                case "ERROR":
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Error",
                        "title": "Error",
                        "message": "Unable to load Data"
                    });
                    resultsToast.fire();
					component.set('v.spinner',false);
                    break;
                default:
            }
        });

        $A.enqueueAction(action);
    }
})