({
    getPlaceholderText: function (component) {
        console.log('in helper doInit');
        var FROM = component.get("v.FROM");
        console.log('objectName: ' + FROM);

        var action = component.get("c.getObjectNameServer");

        action.setParams({
            objectName: FROM
        });

        action.setCallback(this, function (a) {
            //get the response state
            var state = a.getState();

            //handle the response state
            switch (state) {
                case "SUCCESS":
                    var result = a.getReturnValue();
                    if (!$A.util.isEmpty(result) && !$A.util.isUndefined(result))
                        console.log('placeholder: ' + result);
                    component.set("v.placeholder", result);

                    break;
                case "ERROR":
                    console.log(a);
                    var errors = a.getError();
                    alert('Error in calling server side action for getObjectNameServer.' +
                        errors[0].message);
                    break;
                default:
                    console.log('Unhandled problem in calling server side action for getObjectNameServer.');
            }
        });

        //adds the server-side action to the queue
        $A.enqueueAction(action);

    },

    getIcon: function (component) {
        console.log('in helper getIcon');
        var FROM = component.get("v.FROM");

        var action = component.get("c.getIconName");

        action.setParams({
            objectName: FROM
        });

        action.setCallback(this, function (a) {
            //get the response state
            var state = a.getState();

            //handle the response state
            switch (state) {
                case "SUCCESS":
                    var result = a.getReturnValue();
                    if (!$A.util.isEmpty(result) && !$A.util.isUndefined(result))
                        console.log('lookupIcon: ' + result);
                    component.set("v.lookupIcon", result);
                    break;
                case "ERROR":
                    console.log(a);
                    var errors = a.getError();
                    alert('Error in calling server side action for getObjectNameServer.' +
                        errors[0].message);
                    break;
                default:
                    console.log('Unhandled problem in calling server side action for getObjectNameServer.');
            }
        });

        //adds the server-side action to the queue
        $A.enqueueAction(action);
    },
    itemSelected: function (component, event, helper) {
        var target = event.target;
        var SelIndex = helper.getIndexFrmParent(target, helper, "data-selectedIndex");
        if (SelIndex) {
            var serverResult = component.get("v.server_result");
            var selItem = serverResult[SelIndex];
            console.log(selItem);
            component.set("v.selItem", selItem);
            component.set("v.last_ServerResult", serverResult);
            component.set("v.server_result", null);
        }
    },
    serverCall: function (component, event, helper) {
        var target = event.target;
        var searchText = target.value;
        var last_SearchText = component.get("v.last_SearchText");
        //Escape button pressed
        if (event.keyCode == 27 || !searchText.trim()) {
            helper.clearSelection(component, event, helper);
        } else if (searchText.trim() != last_SearchText) {
            //Save server call, if last text not changed
            //Search only when space character entered

            var FROM = component.get("v.FROM");
            var SELECT = component.get("v.SELECT");
            var field_API_search = component.get("v.field_API_search");
            var limit = component.get("v.limit");

            var action = component.get('c.searchDB');
            action.setStorable();

            action.setParams({
                objectName: FROM,
                selectString: SELECT,
                fld_API_Search: field_API_search,
                searchText: searchText,
                lim: limit
            });

            action.setCallback(this, function (a) {
                this.handleResponse(a, component, helper);
            });

            component.set("v.last_SearchText", searchText.trim());
            console.log('Server call made');
            $A.enqueueAction(action);
        } else if (searchText && last_SearchText && searchText.trim() == last_SearchText.trim()) {
            component.set("v.server_result", component.get("v.last_ServerResult"));
            console.log('Server call saved');
        }
    },
    handleResponse: function (res, component, helper) {
        if (res.getState() === 'SUCCESS') {
            var retObj = res.getReturnValue();
            if (retObj.length <= 0) {
                var noResult = JSON.parse('[{"name":"No Results Found"}]');
                component.set("v.server_result", noResult);
                component.set("v.last_ServerResult", noResult);
            } else {
                component.set("v.server_result", retObj);
                component.set("v.last_ServerResult", retObj);
            }
        } else if (res.getState() === 'ERROR') {
            var errors = res.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                    alert(errors[0].message);
                }
            }
        }
    },
    getIndexFrmParent: function (target, helper, attributeToFind) {
        //User can click on any child element, so traverse till intended parent found
        var SelIndex = target.getAttribute(attributeToFind);
        while (!SelIndex) {
            target = target.parentNode;
            SelIndex = helper.getIndexFrmParent(target, helper, attributeToFind);
        }
        return SelIndex;
    },
    clearSelection: function (component, event, helper) {
        component.set("v.selItem", null);
        component.set("v.server_result", null);
    }
})