({
	cloneRecord: function (component, helper) {
		component.set('v.toggle',true);

		var action = component.get('c.cloneMethod');
        action.setParams({
            'recordId': component.get('v.recordId')
        });
        action.setCallback(this, function (response) {
            switch (response.getState()) {
                case "SUCCESS":
                    var newRecordId = response.getReturnValue();                  
					helper.redirect(component, newRecordId);
                    break;
                case "ERROR":
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Error",
                        "title": "Error",
                        "message": "Unable to clone record"
                    });
                    resultsToast.fire();
                    console.log('Error in calling cloneRecord:\n' + response.getError()[0].message);
                    break;
                default:
                    console.log('Unhandled problem in calling cloneRecord.');
            }
        });
        $A.enqueueAction(action);
	},

	redirect : function(component, newRecordId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({"recordId": newRecordId});
        navEvt.fire();
	},
})