({
    //get Contact List from apex controller
    doInit: function (component, event, helper) {
        helper.createObjectData(component, event, helper);
        helper.getContactWrappers(component);
        helper.setWidthToComponent(component);
    },
    handleCallContactList: function (component, event, helper) {
        var params = event.getParam('objectRecord');
        component.set("v.contactList", params);
    },
    createContactsAndAtt: function (component, event, helper) {
        component.set("v.successMsg", '');

        var currentRecId = component.get("v.recordId");
        var newConRec = component.get('v.newConRec');
        var convar = component.get('v.contactList');
        var contactOrRef = component.get("v.contactOrRef");

      /*  if (contactOrRef == "Referral") {
            var action = component.get("c.newRefRecInsert");
        } else {
            var action = component.get("c.newConRecInsert");
        }*/
        var action = component.get("c.newRecordToInsert");

        action.setParams({
            "newContacts": newConRec,
            "eventId": currentRecId,
            "selectedContacts": convar
        });

        action.setCallback(this, function (a) {

            var state = a.getState();

            //check if result is successful
            if (state === "SUCCESS") {

                component.set("v.newConRec", []);
                helper.createObjectData(component, event, helper);
                component.set("v.selectedLookUpRecords", []);
                component.set("v.successMsg", 'Event Participation Records created Successfully!');

            } else if (state === "ERROR") {
                console.log(a);
                var errors = a.getError();
                alert('Error in calling server side action. ' +
                    errors[0].message);
            }
        });
        $A.enqueueAction(action);
    },

    // function for create new object Row in Contact List 
    addNewRow: function (component, event, helper) {
        helper.createObjectData(component, event, helper);
    },

    // function for delete the row
    removeDeletedRow: function (component, event, helper) {

        var index = event.getParam("indexVar");
        var AllRowsList = component.get("v.newConRec");
        AllRowsList.splice(index, 1);
        component.set("v.newConRec", AllRowsList);
    },

});