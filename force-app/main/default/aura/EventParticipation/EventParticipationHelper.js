({
	 getContactWrappers : function(component) {

        var action = component.get("c.getContactWrappers");
        console.log("hello from getContactWrappers");
        //Setting the Callback
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();

            //check if result is successfull
            if(state == "SUCCESS"){
                var result = a.getReturnValue();
                if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result))
                    component.set("v.ContactWrappers",result);
            } else if(state == "ERROR"){
                console.log(a);
                var errors = a.getError();
                alert('Error in calling server side action for ContactWrapper.' +
                errors[0].message);
            }
        });

        //adds the server-side action to the queue
        $A.enqueueAction(action);
    },
    getclassname : function(component, event, sectionId){
        var action = component.get("c.nameofclass");
        var currentRecId = component.get("v.recordId");
        action.setParams({
            "clsId": currentRecId
        });
        console.log("hello from nameofclass");
        //Setting the Callback
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            var result = a.getReturnValue();
            console.log('Name Result..... ' + result);
            //check if result is successfull
            if(state == "SUCCESS"){
                component.set("v.Name",result);
            } else if(state == "ERROR"){
                console.log(a);
                var errors = a.getError();
                alert('Error in calling server side action for ContactWrapper.' +
                      errors[0].message);
            }
        });
		$A.enqueueAction(action);
	},
    toggleSection : function(component, event, sectionId) {
        var acc = component.find(sectionId);
        for(var cmp in acc) {
            $A.util.toggleClass(acc[cmp], 'slds-show');
            $A.util.toggleClass(acc[cmp], 'slds-hide');
       }
    },
    createObjectData: function(component, event,helper) {
        // get the contactList from component and add(push) New Object to List  
        var RowItemList = component.get("v.newConRec");
        RowItemList.push({
            'sobjectType': 'Contact',
            'FirstName': '',
            'LastName': '',
            'Phone': '',
            'Email':'',
            'Company':''
        });
        // set the updated list to attribute (contactList) again    
        component.set("v.newConRec", RowItemList);
    },
    // helper function for check if first Name is not null/blank on save  
    validateRequired: function(component, event) {
        var isValid = true;
        var allNewConRows = component.get("v.newConRec");
        for (var indexVar = 0; indexVar < allNewConRows.length; indexVar++) {
            if (allNewConRows[indexVar].LastName == '') {
                isValid = false;
                alert('Last Name Can\'t be Blank on Row Number ' + (indexVar + 1));
            }
        }
        return isValid;
    },
    setWidthToComponent: function(component){
        var getAtribute = component.get("v.divWidth");
        console.log(getAtribute);
       
        var action = component.get('c.widthOfComponent');       
        action.setCallback(this, function(a){
			switch(a.getState()){
				case "SUCCESS":
                    var response = a.getReturnValue();
                    if(response != null && response > 1000){
                        var newWidth = response;
                        component.set('v.divWidth', newWidth);
                    }										
                break;
            }
                        
         }); 
        $A.enqueueAction(action);
                        
    },
})