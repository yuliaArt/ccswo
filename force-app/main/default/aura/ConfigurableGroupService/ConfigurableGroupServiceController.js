({
	 doInit: function (component, event, helper) {
        helper.getServiceWrapper(component,helper);

        helper.getFirstRow(component,helper);
        helper.getSecondRow(component,helper);
        helper.getThirdRow(component,helper);
     },
    callprogram: function (component, event, helper) {
        helper.getContactWrappers(component, event, helper);
         
    },
    toggleBoth: function (component, event, helper) {
        helper.toggleSection(component, event, "serviceFields");
        helper.toggleSection(component, event, "selectContacts");
    },
    toggleServiceFields: function (component, event, helper) {
        helper.toggleSection(component, event, "serviceFields");
    },
    toggleSelectContacts: function (component, event, helper) {
        helper.toggleSection(component, event, "selectContacts");
    },
    clearSelected: function (component, event, helper) {
        helper.clearSelected(component);
    },
    rerender: function (component, event) {
        if(event.which === 13) {
            event.preventDefault();
            component.set("v.searchTerm", component.get("v.searchString"));
            component.set("v.contactwrp", component.get("v.contactwrp"));
        }
    },
    onRecordSubmit: function(component, event, helper) {

        event.preventDefault();
        helper.insertServices(component, event, helper);

    }
 })