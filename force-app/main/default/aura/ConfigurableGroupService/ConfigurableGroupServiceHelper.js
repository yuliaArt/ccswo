({
	getContactWrappers : function(component) {

        component.set("v.spinner", true);
        var emptyList = [];
        component.set("v.contactwrp", emptyList);

        var action = component.get("c.getProgramEnrollments");
        var selected = component.get("v.selectedProgram");
        var groupfamiliesId = component.get("v.group.Id");
        action.setParams({ 
           "programSelectedValue": selected,
           "groupId":groupfamiliesId
        });

		action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();

            //check if result is successfull
            if(state == "SUCCESS"){

                var result = a.getReturnValue();
                if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result))
                    
                    component.set("v.contactwrp", result);

                    if (result == null || result.length == 0) {
                        component.set('v.message', 'No values for selected program.');
                    } else {
                        component.set('v.message', '');
                    }

            } else if(state == "ERROR"){

                var errors = a.getError();
                alert('Error in calling server side action for ContactWrapper.' +
                errors[0].message);
            }
            component.set("v.spinner", false);
        });

        //adds the server-side action to the queue
        $A.enqueueAction(action);
    },
    getFirstRow: function(component) {

        var firstRow = component.get("v.firstFieldsApi");
        var action = component.get("c.convertStrToList");
        action.setParams({
            'str': firstRow
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var fieldsMap = [];

                var result = response.getReturnValue();
                for(var key in result){
                    fieldsMap.push({ value: result[key] });
                }
                component.set("v.firstFieldsList", result);

            } else {
                console.log('error');
            }
        });
        $A.enqueueAction(action);

    },
    getSecondRow: function(component) {

        var secondRow = component.get("v.secondFieldsApi");
        var action = component.get("c.convertStrToList");
        action.setParams({
            'str': secondRow
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var fieldsMap = [];

                var result = response.getReturnValue();
                for(var key in result){
                    fieldsMap.push({ value: result[key] });
                }
                component.set("v.secondFieldsList", result);

            } else {
                console.log('error');
            }
        });
        $A.enqueueAction(action);

    },
    getThirdRow: function(component) {

        var thirdRow = component.get("v.thirdFieldsApi");
        var action = component.get("c.convertStrToList");
        action.setParams({
            'str': thirdRow
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var fieldsMap = [];

                var result = response.getReturnValue();
                for(var key in result){
                    fieldsMap.push({ value: result[key] });
                }
                component.set("v.thirdFieldsList", result);

            } else {
                console.log('error');
            }
        });
        $A.enqueueAction(action);

    },
    clearSelected : function(component) {
        var ContactWrappers = component.get('v.contactwrp');

        for(var i in ContactWrappers) {
            ContactWrappers[i].isSelected = false;
        }

        component.set('v.contactwrp', ContactWrappers);
    },

    getServiceWrapper : function(component) {
        var action = component.get("c.newServiceWrapper");

        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();

            //check if result is successfull
            if(state == "SUCCESS"){
                var result = a.getReturnValue();
                if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result))
                    component.set("v.serviceWrapper",result);
            } else if(state == "ERROR"){
                alert('Error in calling server side action for ServiceWrapper');
            }
        });
          $A.enqueueAction(action);

    },
    toggleSection : function(component, event, sectionId) {
        var acc = component.find(sectionId);
        for(var cmp in acc) {
            $A.util.toggleClass(acc[cmp], 'slds-hide');
            $A.util.toggleClass(acc[cmp], 'slds-show');
       }
    },
    insertServices : function(component, event) {

        var selectedRT = component.get("v.recordTypeName");
        var eventFields = event.getParam("fields");
        var contactWrapper = component.get("v.contactwrp");
		console.log('eventFields '+JSON.stringify(eventFields));
        if (eventFields['Topics__c'] == 'Other' && eventFields['Other_Topic__c'] == null) {
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({
                "type": "Error",
                "title": "Error",
                "message": "Please, fill in the Other Topic field."
            });
            resultsToast.fire();
            
        } else {
            
            var action = component.get("c.insertRecords");
            action.setParams({
                'contactWrappers' : JSON.stringify(contactWrapper),
                'eventFields' :  JSON.stringify(eventFields),
                'groupId' : component.get("v.group.Id"),
                'rcdTypeId' : selectedRT
            });
			component.set("v.spinner", true);
            action.setCallback(this, function (a) {
                //get the response state
                var state = a.getState();
                if (state == "SUCCESS") {
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Success",
                        "title": "Success",
                        "message": "Services were created!"
                    });
                    resultsToast.fire();
                    component.set("v.spinner", false);
                    $A.get('event.force:refreshView').fire();

                } else if (state == "ERROR") {
                    var errors = a.getError();
                    console.log('Error in calling server side action' + JSON.stringify(errors));
                    component.set("v.message", JSON.stringify(errors));
                }
            });
            $A.enqueueAction(action);
        }
        
    }
})