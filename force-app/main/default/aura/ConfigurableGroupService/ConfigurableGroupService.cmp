<!--
Author: Shashank Parashar
Date: July 30, 2018
Description: Lightning Component to create Service records
    Revised:
    Author: Ivanna Kuzemchak
    Date: December 14, 2018
    Description: Lightning Component was modified to be more flexible. It accepts fields api and record type from UI in order 
    to create service records.

This code is the property of Provisio Partners and copy or reuse is prohibited.
Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
support@provisiopartners.org
-->

<aura:component controller="ConfigurableGroupServiceComponentCtrl" 
                implements="force:appHostable,flexipage:availableForAllPageTypes,flexipage:availableForRecordHome,force:hasRecordId,forceCommunity:availableForAllPageTypes,force:lightningQuickAction"
                access="global">
    <aura:handler name="init" value="{!this}" action="{!c.doInit}"/>
    <aura:attribute name="programRecords" type="list[]"/>
    <aura:attribute name="serviceWrapper" type="ServiceWrapper"/>
    <aura:attribute name="contactwrp" type="ContactWrapper[]"/>
    <aura:attribute name="selectedProgram" type="string"/>
    <aura:attribute name="serviceType" type="string"/>
    <aura:attribute name="selectedTopic" type="string"/>
    <aura:attribute name="selectedCareyReceived" type="string"/>
    <aura:attribute name="selectedCareyCompleted" type="string"/>
    <aura:attribute name="group" type="Group__c"/>
    <aura:attribute name="searchTerm" type="String" default=""/>
    <aura:attribute name="spinner" type="Boolean" default="false" />
    <aura:attribute name="message" type="String" default="" />
    <aura:attribute name="searchString" type="String" default=""
                    description="this is the string stored in the search box, passed to the searchTerm when enter is hit."/>
    <aura:attribute name="firstFieldsApi" type="String" />
    <aura:attribute name="secondFieldsApi" type="String" />
    <aura:attribute name="thirdFieldsApi" type="String" />
    <aura:attribute name="firstFieldsList" type="String[]" />
    <aura:attribute name="secondFieldsList" type="String[]" />
    <aura:attribute name="thirdFieldsList" type="String[]" />
    <aura:attribute name="recordTypeName" type="String" default="None"/>

    <!-- loading spinner start... style=Brand Medium (blue dots)-->
    <aura:if isTrue="{!v.spinner}">
        <div aura:id="spinnerId" class="slds-spinner_container">
            <div class="slds-spinner--brand  slds-spinner slds-spinner--large slds-is-relative" role="alert">
                <span class="slds-assistive-text">Loading</span>
                <div class="slds-spinner__dot-a"></div>
                <div class="slds-spinner__dot-b"></div>
            </div>
        </div>
    </aura:if>
    <!--Loading spinner end-->
    
    <!-- start page -->
    <div class="slds-box slds-p-around_medium" style="background-color:white">

        <lightning:recordEditForm aura:id="editForm" objectApiName="Service__c" onsubmit="{!c.onRecordSubmit}">
            <div class="slds-page-header" style="cursor: pointer;" onclick="{!c.toggleServiceFields}">
                <section class="slds-clearfix">
                    <div class="slds-float--left">
                        <lightning:icon class="slds-hide" aura:id="serviceFields" iconName="utility:add" size="x-small"/>
                        <lightning:icon class="slds-show" aura:id="serviceFields" iconName="utility:dash" size="x-small"/>
                    </div>
                    <div class="slds-m-left--large">Service Fields</div>
                </section>
            </div>
            <div class="slds-show" aura:id="serviceFields">
                <lightning:layout>
                	<lightning:layoutItem size="3" padding="around-small">
                		<lightning:inputField fieldName="Date_of_Service__c"/>
                	</lightning:layoutItem>
                    <aura:iteration items="{!v.firstFieldsList}" var="fEachField">
                        <lightning:layoutItem size="3" padding="around-small">
                            <lightning:inputField fieldName="{!fEachField}"/>
                        </lightning:layoutItem>
                    </aura:iteration>
                </lightning:layout>
                <lightning:layout>
                    <aura:iteration items="{!v.secondFieldsList}" var="sEachField">
                        <lightning:layoutItem size="3" padding="around-small">
                            <lightning:inputField fieldName="{!sEachField}"/>
                        </lightning:layoutItem>
                    </aura:iteration>
                </lightning:layout>
                <lightning:layout>
                    <aura:iteration items="{!v.thirdFieldsList}" var="tEachField">
                        <lightning:layoutItem size="3" padding="around-small">
                            <lightning:inputField fieldName="{!tEachField}"/>
                        </lightning:layoutItem>
                    </aura:iteration>
                </lightning:layout>
            </div>

            <div class="slds-page-header" style="cursor: pointer;" onclick="{!c.toggleSelectContacts}">
                <section class="slds-clearfix">
                    <div class="slds-float--left">
                        <lightning:icon class="slds-show" aura:id="selectContacts" iconName="utility:add" size="x-small"/>
                        <lightning:icon class="slds-hide" aura:id="selectContacts" iconName="utility:dash" size="x-small"/>
                    </div>
                    <div class="slds-m-left--large">Select Contacts</div>
                </section>
            </div>
            <div class="slds-p-around_medium slds-hide" aura:id="selectContacts">
            
                <div class="slds-grid slds-gutters slds-p-around-show slds-wrap">
                    <div class="slds-col slds-size_1-of-4">
                        <c:GetPicklist objectName="Program_Enrollment__c" fieldName="Program__c" label="Select Program:"
                                       variant="standard" value="{!v.selectedProgram}"/>
                        
                    </div>
                    <div class="slds-col slds-size_1-of-4" >Group Families
                        <c:Lookup FROM="Group__c" selItem="{!v.group}"/>                 
                    </div>
                    <div class="slds-col slds-size_1-of-4 slds-align-bottom">
                        <lightning:button variant="brand" label="Get Enrollments" onclick="{!c.callprogram}"/>
                    </div>
                    
                </div>
                
                <div class="slds-grid slds-gutters slds-p-around-show slds-wrap">
                    <div class="slds-col slds-size_2-of-4">
                        <span onkeypress="{!c.rerender}">
                            <lightning:input name="clientFilter" label="Search Clients:" value="{!v.searchString}"
                                             placeholder="Hit Enter to search"/>
                        </span>
                    </div>
                    <div class="slds-col slds-size_1-of-4 slds-align-bottom" style="width:300px">
                        <lightning:button variant="brand" label="Clear Selected" onclick="{!c.clearSelected}"/>
                    </div>
                </div>
                
                <div class="slds-p-around_medium slds-align_absolute-center">
                    <lightning:button variant="success" label="Save Services" type="submit"/>
                </div>
                
                <aura:if isTrue="{!empty(v.contactwrp)}">
                    <div class="slds-p-around_medium slds-align_absolute-center">
                    	<aura:if isTrue="{!v.message != '' }">
                    		<p class="slds-color__text_gray-8">{! v.message }&nbsp;</p>
                    	</aura:if>
                        <p class="slds-color__text_gray-8">Please select a program in order to view enrollments.</p>
                    </div>
                </aura:if>
                <aura:if isTrue="{! !empty(v.contactwrp)}">
                    <table class="slds-table slds-table--bordered slds-table--cell-buffer slds-table--fixed-layout">
                        <thead>
                            <th>Program Enrollment</th>
                            <th>Contact Name</th>
                            <th>Select</th>
                        </thead>
                        <tbody>
                            <aura:iteration var="cw" items="{!v.contactwrp}">
                                <c:AuraIfStringContains container="{!cw.contactName}" substring="{!v.searchTerm}">
                                    <tr>
                                        <td>
                                            <ui:outputText value="{!cw.programName}"/>
                                        </td>
                                        <td>
                                            <ui:outputText value="{!cw.contactName}"/>
                                        </td>
                                        <td>
                                            <ui:inputCheckbox value="{!cw.isSelected}"/>
                                        </td>
                                    </tr>
                                </c:AuraIfStringContains>
                            </aura:iteration>
                        </tbody>
                    </table>
                </aura:if>
            </div>
            
        </lightning:recordEditForm>
       
    </div>
    
</aura:component>