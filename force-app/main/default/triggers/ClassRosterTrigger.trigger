//**
// Author: Ivanna Kuzemchak
// Date: November 21, 2018
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

trigger ClassRosterTrigger on Class_Roster__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    // Checking if trigger is not disabled in Custom Settings
    List<Trigger_Settings__c> triggerSet = [Select Id, Disable_ClassRosterTrigger__c from Trigger_Settings__c Limit 1];
    if (triggerSet[0].Disable_ClassRosterTrigger__c != true){
        ClassRosterTriggerHandler.entry(new TriggerParams(trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, 
            trigger.isUndelete, trigger.new, trigger.old, trigger.newMap, trigger.oldMap));
    }
}