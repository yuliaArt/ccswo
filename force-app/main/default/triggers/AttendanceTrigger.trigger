//**
// Author: Yaroslav Mazury
// Date: March 3, 2019
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

trigger AttendanceTrigger on Attendance__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    // Checking if trigger is not disabled in Custom Settings
	List<Trigger_Settings__c> triggerSet = [Select Id, Disable_AttendanceTrigger__c from Trigger_Settings__c Limit 1];
	if (triggerSet[0].Disable_AttendanceTrigger__c != true){
		AttendanceTriggerHandler.entry(new TriggerParams(trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, 
			trigger.isUndelete, trigger.new, trigger.old, trigger.newMap, trigger.oldMap));
	}

}