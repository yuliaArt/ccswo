@isTest 
private class BatchScheduleExceptionLogDeleteOldTest {

	@testSetup 
	public static void testSetup() {
		TestFactory.createDataForBatchExceptionDeleteOld();
		Exception_Log__c exceptionLog = [SELECT Id FROM Exception_Log__c];
		Datetime twoMonthsAgo = Datetime.now().addMonths(-3);
		Test.setCreatedDate(exceptionLog.Id, twoMonthsAgo);
	}

	@isTest static void executeBatchScheduleAttendanceCreateTest() {
		Test.startTest();
        String CRON_EXP = '0 0 0 31 12 ? 2050';
        String jobId = System.schedule('BatchScheduleExceptionLogDeleteOld', CRON_EXP, new BatchScheduleExceptionLogDeleteOld());
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
		System.assertEquals(0, ct.TimesTriggered);
		System.assertEquals('2050-12-31 00:00:00', String.valueOf(ct.NextFireTime));
		Test.stopTest();
	}
}