//**
// Author: Mykhailo Senyuk
// Date: May 08, 2019
// Description: This is a Trigger Handler that process related Session records.
// If Class record is created, this class creates Session records from start date to end date.
// If Class record is updates, this class creates Session records or deletes unnecessery records.
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**
public class ClassTriggerHandler {

	public static void entry(TriggerParams triggerParams) {

		List<Class__c> triggerNew = (List<Class__c>) triggerParams.triggerNew;
		List<Class__c> triggerOld = (List<Class__c>) triggerParams.triggerOld;
		Map<Id, Class__c> oldMap = (Map<Id, Class__c>) triggerParams.oldMap;
		Map<Id, Class__c> newMap = (Map<Id, Class__c>) triggerParams.newMap;
		
		if (triggerParams.isAfter) {
			if (triggerParams.isInsert) {
				createNewSessionRecord(newMap, null, false);
			}
			if (triggerParams.isUpdate) {
				updateSessionRecord(oldMap, newMap);
			}
		}

	}

	public static void createNewSessionRecord(Map<Id, Class__c> newClassById, Map<Id, Map<Date, Session__c>> oldSessionsByDateByClassId, Boolean isDelete) {
		List<Session__c> sessions = new List<Session__c> ();
		Map<Id, Map<Date, Session__c>> sessionsToDeleteByDateByClassId = new Map<Id, Map<Date, Session__c>> ();
		
		if(oldSessionsByDateByClassId != null) {
			sessionsToDeleteByDateByClassId.putAll(oldSessionsByDateByClassId);
		}
		
		for (Id classId : newClassById.keySet()) {
			if (newClassById.get(classId).Start_Date__c != null && newClassById.get(classId).End_Date__c != null) {
				for (Date classDate = newClassById.get(classId).Start_Date__c; classDate <= newClassById.get(classId).End_Date__c; classDate = classDate.addDays(1)) {
					for (Integer day : ClassRosterTriggerHandler.listOfClassDays(newClassById.get(classId))) {
						if ( dayOfWeek(classDate) == day ) {
							
							if( !isDelete && oldSessionsByDateByClassId != null && oldSessionsByDateByClassId.containsKey(classId) ){ //** when update class create Sessions
								if( !oldSessionsByDateByClassId.get(classId).containsKey(classDate) ){
									Session__c session = new Session__c();
									session.Class__c = classId;
									session.Date__c = classDate;
									sessions.add(session);
								}
							} else if(!isDelete) { //** when create class create Sessions
								Session__c session = new Session__c();
								session.Class__c = classId;
								session.Date__c = classDate;
								sessions.add(session);
							} else if(isDelete && sessionsToDeleteByDateByClassId.containsKey(classId)){ //** when update class delete Sessions
								sessionsToDeleteByDateByClassId.get(classId).remove(classDate);
							}
						}
					}
				}
			}
		}  
		if(isDelete){
			List<Session__c> sessionsToDelete = new List<Session__c>();
			
			for(Map<Date, Session__c> sessionDate: sessionsToDeleteByDateByClassId.values()){
				sessionsToDelete.addAll(sessionDate.values());
			}
			if ( !sessionsToDelete.isEmpty() ) {
				Database.delete(sessionsToDelete, false);
			}
		}
		if (!sessions.isEmpty()) {
			insert sessions;
		}

	}

	public static void updateSessionRecord(Map<Id, Class__c> oldClassById, Map<Id, Class__c> newClassById){
		Map<Id, Map<Date, Session__c>> oldSessionsByDateByClassId = new Map<Id, Map<Date, Session__c>> ();
		
		List<Session__c> oldSessions = [SELECT id, Date__c, Class__c FROM Session__c WHERE Class__c in:oldClassById.keySet()];
		
		if (!oldSessions.isEmpty()) {
			for (Session__c session : oldSessions) {
				if (oldSessionsByDateByClassId.containsKey(session.Class__c)) {
					Map<Date, Session__c> sessionByDate = oldSessionsByDateByClassId.get(session.Class__c);
					sessionByDate.put(session.Date__c, session);
					oldSessionsByDateByClassId.put(session.Class__c, sessionByDate);
				} else {
					oldSessionsByDateByClassId.put(session.Class__c, new Map<Date, Session__c> { session.Date__c => session });
				}
			}
		}

		for (Id classId: oldClassById.keySet()) {
			if(oldClassById.get(classId).Start_Date__c != newClassById.get(classId).Start_Date__c || oldClassById.get(classId).End_Date__c != newClassById.get(classId).End_Date__c || oldClassById.get(classId).Monday__c != newClassById.get(classId).Monday__c || oldClassById.get(classId).Tuesday__c != newClassById.get(classId).Tuesday__c || oldClassById.get(classId).Wednesday__c != newClassById.get(classId).Wednesday__c || oldClassById.get(classId).Thursday__c != newClassById.get(classId).Thursday__c || oldClassById.get(classId).Friday__c != newClassById.get(classId).Friday__c || oldClassById.get(classId).Saturday__c != newClassById.get(classId).Saturday__c || oldClassById.get(classId).Sunday__c != newClassById.get(classId).Sunday__c){
				if ( oldClassById.get(classId).Start_Date__c > newClassById.get(classId).Start_Date__c || oldClassById.get(classId).End_Date__c < newClassById.get(classId).End_Date__c  || oldClassById.get(classId).Monday__c != newClassById.get(classId).Monday__c || oldClassById.get(classId).Tuesday__c != newClassById.get(classId).Tuesday__c || oldClassById.get(classId).Wednesday__c != newClassById.get(classId).Wednesday__c || oldClassById.get(classId).Thursday__c != newClassById.get(classId).Thursday__c || oldClassById.get(classId).Friday__c != newClassById.get(classId).Friday__c  || oldClassById.get(classId).Saturday__c != newClassById.get(classId).Saturday__c || oldClassById.get(classId).Sunday__c != newClassById.get(classId).Sunday__c ){
					createNewSessionRecord(newClassById, oldSessionsByDateByClassId, false); //update
				} 
				if ( oldClassById.get(classId).Start_Date__c < newClassById.get(classId).Start_Date__c || oldClassById.get(classId).End_Date__c > newClassById.get(classId).End_Date__c || oldClassById.get(classId).Monday__c != newClassById.get(classId).Monday__c || oldClassById.get(classId).Tuesday__c != newClassById.get(classId).Tuesday__c || oldClassById.get(classId).Wednesday__c != newClassById.get(classId).Wednesday__c || oldClassById.get(classId).Thursday__c != newClassById.get(classId).Thursday__c || oldClassById.get(classId).Friday__c != newClassById.get(classId).Friday__c  || oldClassById.get(classId).Saturday__c != newClassById.get(classId).Saturday__c || oldClassById.get(classId).Sunday__c != newClassById.get(classId).Sunday__c ) {
					createNewSessionRecord(newClassById, oldSessionsByDateByClassId, true); //delete
				}
				if ( newClassById.get(classId).Start_Date__c != null && newClassById.get(classId).End_Date__c != null && newClassById.get(classId).Start_Date__c != newClassById.get(classId).End_Date__c && (oldClassById.get(classId).Start_Date__c == null || oldClassById.get(classId).End_Date__c == null) ) {
					createNewSessionRecord(newClassById, null, false); //create
				}
			}
		}

	}

	public static Integer dayOfWeek(Date d) {
		Datetime dt = (Datetime) d;
		String dayOfWeekStr = dt.addDays(1).format('EE');
		Map<String, Integer> dayOfWeekByName = new Map<String, Integer> {
			'Mon' => 1,
			'Tue' => 2,
			'Wed' => 3,
			'Thu' => 4,
			'Fri' => 5,
			'Sat' => 6,  
			'Sun' => 0 };
		return dayOfWeekByName.get(dayOfWeekStr);
	}
}