@isTest 
private class ObjectUtilsTest {

	@testsetup
    public static void tesetup(){
        TestFactory.createDataForCloneRecordController();
		
    }
	
	@isTest
	public static void testRecordTypes(){
   		Id recordTypeId = ObjectUtils.getRecordTypeId('Account','Master');
		System.assertEquals('012000000000000AAA', recordTypeId);
		
		ObjectUtils.getAllObjectFields('Account');
		ObjectUtils.getAllFields('Contact');
        ObjectUtils.getAllFieldsFromObject('Account');
		ObjectUtils.getAllFieldsLabelFromObject('Account');
		ObjectUtils.getFieldLabelFromObjectByFieldName('Account','Name');

		ObjectUtils.HasSObjectField('Account','Name');
		
		Map<String, Schema.RecordTypeInfo> allRecordTypesFromObject = ObjectUtils.getAllRecordTypesFromObject('Account');
		ObjectUtils.getRecordTypeId(allRecordTypesFromObject, 'Master');
		ObjectUtils.hasSObjectRecordType('Account','Master');
		ObjectUtils.hasSObjectRecordType(allRecordTypesFromObject,'Master');
		ObjectUtils.getAllRecordTypesFromObject('Account');
		 
		Messaging.EmailFileAttachment testAttachment = ObjectUtils.setAttachment('test','test.csv');
		ObjectUtils.sendEmails('test', new List<id>{UserInfo.getUserId()}, 'Test Subject','Test Name',testAttachment);
		
		List<Account> accountsTest = TestFactory.createSObjectList(new Account(), 2 , 'TestFactory.AccountDefaults', true);
		ObjectUtils.randomize(accountsTest);

    }

	@isTest
	public static void testGetReletedObjects(){
		Service_Plan__c testServicePlan = [SELECT id FROM Service_Plan__c LIMIT 1];
		ObjectUtils.getReletedObjects(testServicePlan.Id);
	}

	@isTest static void findPublicGroupUsersPositive(){
		
		Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
		User testUser = (User) TestFactory.createSObject(new User(ProfileId = p.Id), 'TestFactory.UserDefaults', true);
		Group testGroup = (Group) TestFactory.createSObject(new Group(Name = 'Address group'), true);
		GroupMember groupMember = (GroupMember) TestFactory.createSObject(new GroupMember(UserOrGroupId = testUser.Id, GroupId = testGroup.Id), true);

		ObjectUtils.getUsersIdFromPublicGroup('Address_group');

	}

}