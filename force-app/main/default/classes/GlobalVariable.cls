public class GlobalVariable  {
	public static final String CLASS_ROSTER_STATUS = 'Current';
	public static final String CLASS_ROSTER_STATUS_WAITLIST = 'Waitlist';
	public static final String CLASS_STATUS = 'In Progress'; 
	public static final String ATTENDANCE_STATUS = 'Enrolled';
    public static final String ATTENDANCE_STATUS_PRESENT = 'Present';
	public static final String InterReq_Interpreter_Scheduled = 'Interpreter Scheduled';
	public static final String InterReq_Completed = 'Completed';
	public static final String InterReq_In_Progress= 'In Progress'; 
	public static final String AuditChart_In_Progress= 'In Progress'; 

	public static final String EXCEPTION_RECEIVERS_GROUP = 'Exception_Receivers';


}