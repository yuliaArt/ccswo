@isTest 
private class ContactWrapperTest {
	
	@TestSetup
	private static void testSetup(){
		Contact con = (Contact) TestFactory.createSObject(new Contact(), 'TestFactory.ContactDefaults', true);
		List<Program_Enrollment__c> pe = TestFactory.createSObjectList(new Program_Enrollment__c(Contact__c = con.Id), 1, 'TestFactory.Program_EnrollmentDefaults', true);
	}

	@isTest
	private static void testName() {
		Contact contact = [SELECT Id, Name FROM Contact LIMIT 1];
		List<Program_Enrollment__c> programEnrollmens = [SELECT Id, Name FROM Program_Enrollment__c];

		new ContactWrapper(true, 'First Last', contact.Id, 'docNumb');
		new ContactWrapper(true, 'First Last', 'programName', contact.Id, programEnrollmens, programEnrollmens[0].Id);
		new ContactWrapper(true, 'First Last', 'Test Name', contact.Id, programEnrollmens, 1, programEnrollmens[0].Id, null);
		new ContactWrapper(true, 'First Last', 'Test Name', contact.Id, programEnrollmens, 1, programEnrollmens[0].Id, Date.today());
		new ContactWrapper(true, 'First Last', contact.Id);

	}
}