/* Provisio Partners
** Author: Shashank Parashar
** Date: 7/17/2018
** Description: Heart Land Unaccompanied tab which is used for Class to Display data when click on action Case Notes from Contact page layout
** Revision: <<Date, initials, description>>
**/

public with sharing class CaseNotesController {
    public String selectedRT {get;set;}
    //List of the Service Record Type
    public List<SelectOption> recordTypeList {get;set;}
    public List<Service__c> allSNotes { get; set; }
    public List<Service__c> allSNoteswithRT { get; set; }
    // List will pass serivce data to VF Page
    public List<Service__c> SNotes { get; set; }
    public Program_Enrollment__c ind { get; set; }
    public Contact indC { get; set; }
    public Id IdRecord {get;set;}
    public String date1 { get; set; }
    public String date2 { get; set; }

    //Standard constructor
    public CaseNotesController(ApexPages.StandardController Controller) {
        recordTypeList = new List<SelectOption>();
        List<RecordType> rtList = [SELECT Id,Name FROM RecordType WHERE SObjectType='Service__c'];
        recordTypeList.add(new SelectOption('--None--', '--None--'));
        for(RecordType eachservRT : rtList)
        {
            recordTypeList.add(new SelectOption(eachservRT.Id, eachservRT.Name));
        }
        // String ind_id = ApexPages.currentPage().getParameters().get('id');
        this.ind = (Program_Enrollment__c) Controller.getRecord();
        IdRecord = ind.Id;
        //System.debug('value in Get Record '+ind);
        //Query to get Service object record with Record Type = Case Note and Service date order by DESC
        allSNotes = querySNotes(ind.Id);
        //Moving records from allSNotes to SNotes and passing List<SNotes> to VF Page
        SNotes = new List<Service__c>(allSNotes);
        // System.debug('value in allSnotes '+allSnotes);
        // System.debug('value in SNotes '+SNotes); 

        ind = [
                SELECT Id, Name, Department__c, Program__c
                FROM Program_Enrollment__c
                WHERE Id = :ind.Id
                LIMIT 1
        ];
        //System.debug('value in Get Record '+ind);
    }
    //Custom constructor
    public CaseNotesController() {
        //ind_id  will get current page record 
        Id indId = (Id) ApexPages.currentPage().getParameters().get('id');
        IdRecord = indId;
        //Query to get Service object record with Record Type = Case Note and Service date order by DESC
        allSNotes = querySNotes(indId);
        //Moving records from allSNotes to SNotes and passing List<SNotes> to PDF VF Page             
        SNotes = new List<Service__c>(allSNotes);
        
        String id = String.valueOf(indId);
        String keyCode  = id.subString(0,3);

        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        for(Schema.SObjectType objectInstance : gd.values()){
            if(objectInstance.getDescribe().getKeyPrefix() == keyCode){
                if(objectInstance.getDescribe().getName() == 'Contact'){
                    indC = [
                        SELECT Id, Name
                        FROM Contact
                        WHERE Id = :indId
                        LIMIT 1
                    ];

                }else{
                    ind = [
                        SELECT Id, Name, Department__c, Program__c
                        FROM Program_Enrollment__c
                        WHERE Id = :indId
                        LIMIT 1
                    ]; 
                }
            }
        }
    }

    public List<Service__c> querySNotes(Id indId ) {

        String id = String.valueOf(indId);
        String keyCode  = id.subString(0,3);

        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        List<Service__c> services = new List<Service__c>();
        for(Schema.SObjectType objectInstance : gd.values()){
            if(objectInstance.getDescribe().getKeyPrefix() == keyCode){
                if(objectInstance.getDescribe().getName() == 'Contact'){
                    List<Service__c> servicesRelatedToContact = [
                        SELECT Id,
                                Name,
                                Note__c,
                                Date_of_Service__c,
                                Service_Provided__c,
                                Duration__c,
                                Program_Enrollment__c,
                                CreatedBy.Name
                        FROM Service__c
                        WHERE Client__c = :indId 
                        ORDER BY Date_of_Service__c DESC
                    ];

                    services.addAll(servicesRelatedToContact);

                    List<Service__c> filterList = CaseNotePreviewController.getServicesRelatedToPrograms(indId);

                    services.addAll(filterList);

                }else{
                    List<Service__c> servicesRelatedToContact = [
                        SELECT Id,
                                Name,
                                Service_Provided__c,
                                Duration__c,
                                Note__c,
                                Date_of_Service__c,
                                Program_Enrollment__c,
                                CreatedBy.Name
                        FROM Service__c
                        WHERE Program_Enrollment__c = :indId 
                        ORDER BY Date_of_Service__c DESC
                    ];
                    services.addAll(servicesRelatedToContact);
                }
            }
        }
        return services;
    }
    public List<Service__c> getSNotes() {
        return SNotes;
    }

    public void updateSNotes() {
        //List to display is cleared to filter records so that it will show only record which are satifying criteria
        SNotes.clear();        
        for (Service__c bil : allSNotes) {
            if (selectedRT != null) {
                //Service records to display where above if  condition is satisfied
                SNotes.add(bil);
            }
        }
    }
    
    Public Void getServiceRTvise(){  

        SNotes.clear();   
        Id indId = (Id) ApexPages.currentPage().getParameters().get('id');
        
        List<Service__c> queryServiceList = new List<Service__c>();

        String id = String.valueOf(indId);
        String keyCode  = id.subString(0,3);
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        for(Schema.SObjectType objectInstance : gd.values()){
            if(objectInstance.getDescribe().getKeyPrefix() == keyCode){
                if(objectInstance.getDescribe().getName() == 'Contact'){   
                    List<Service__c> servicesRelatedToContact =  [SELECT Id,
                                Name,
                                Service_Provided__c,
                                Duration__c,
                                Note__c,
                                Date_of_Service__c,
                                Program_Enrollment__c,
                                CreatedBy.Name
                        FROM Service__c
                        WHERE 
                        Client__c = :IdRecord
                        ORDER BY Date_of_Service__c DESC];

                        queryServiceList.addAll(servicesRelatedToContact);

                        List<Service__c> filterList = CaseNotePreviewController.getServicesRelatedToPrograms(indId);
                        queryServiceList.addAll(filterList);
                        
                    }else{
                        List<Service__c> servicesRelatedToContact =  [SELECT Id,
                                Name,
                                Note__c,
                                Date_of_Service__c,
                                Service_Provided__c,
                                Duration__c,
                                Program_Enrollment__c,
                                CreatedBy.Name
                        FROM Service__c
                        WHERE 
                        Program_Enrollment__c = :IdRecord
                        ORDER BY Date_of_Service__c DESC];

                        queryServiceList.addAll(servicesRelatedToContact);
                    }
                }
            }

            Date datevar1, datevar2;
            if(queryServiceList.size() == 0 && (date1 != 'undefined' || date2 != 'undefined' )){
                        
                           if ((date1 == 'undefined') || (date1 == 'undefined')) {
                                // D1 is set to 1990, 1, 1
                                datevar1 = Date.newInstance(1990, 1, 1);
                            } else {
                                try {
                                    datevar1 = Date.parse(date1);
                                } catch (TypeException e) {
                                    return;
                                }
                            }
                            if ((date2 == 'undefined') || (date2 == 'undefined')) {
                                // D2 is set to 2990, 1, 1
                                datevar2 = Date.newInstance(2990, 1, 1);
                            } else {
                                try {
                                    datevar2 = Date.parse(date2);
                                } catch (TypeException e) {
                                    return;
                                }
                            }
                            //List to display is cleared to filter records so that it will show only record which are satifying criteria
                            SNotes.clear();
                            for (Service__c bil : allSNotes) {
                                if (bil.Date_of_Service__c >= datevar1 && (bil.Date_of_Service__c <= datevar2)) {
                                    //Service records to display where above if  condition is satisfied
                                    SNotes.add(bil);
                                }
                            }
                        
                }else if(date1 != 'undefined' && date2 != 'undefined' && queryServiceList.size() > 0){
                        for(Service__c sd : queryServiceList){ 
                           datetime d1 = Date.valueOf(date1);
                           datetime d2 = Date.valueOf(date2);
                           datetime serDate1= sd.Date_of_Service__c;
                            if(serDate1 >= d1 && serDate1 <= d2){
                               SNotes.add(sd);   
                            }
                         }
                  }else if(queryServiceList.size() > 0 && (date1 != 'undefined' )){
                    for(Service__c sd : queryServiceList){ 
                        datetime d1 = Date.valueOf(date1);
                        datetime serDate1= sd.Date_of_Service__c;
                        if(serDate1 >= d1){
                           SNotes.add(sd);   
                        }
                     }
                }else if(queryServiceList.size() > 0 && (date2 != 'undefined' )){     
                    System.debug('underfined ');
                    for(Service__c sd : queryServiceList){ 
                        datetime d2 = Date.valueOf(date2);
                        datetime serDate1= sd.Date_of_Service__c;
                        if(serDate1 >= d2){
                           SNotes.add(sd);   
                        }
                     }
                     System.debug('SNotes '+SNotes);
                }else if(queryServiceList.size() > 0 &&  (date1 == 'undefined' || date2 == 'undefined' )){
                    SNotes.addAll(queryServiceList);
                }else{
                    SNotes = new List<Service__c>(allSNotes);
                }
                
                  
    }

    public void updateSNotesPDF() {
        System.debug('here ');
        // Get date1 and date2 from VF page entered by user on VF Page
        date1 = ApexPages.currentPage().getParameters().get('date1');
        date2 = ApexPages.currentPage().getParameters().get('date2');
        selectedRT  = ApexPages.currentPage().getParameters().get('selectedRT');
        System.debug('date1 '+date1);
         System.debug('date2 '+date2);
          System.debug('selectedRT '+selectedRT);
        //Call of method updateSNotes
        getServiceRTvise();
    }
}