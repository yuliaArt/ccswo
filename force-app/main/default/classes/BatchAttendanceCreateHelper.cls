//**
// Author: Ivanna Kuzemchak
// Date: November 26, 2018
// Description: This is a Batch Class Helper that creates Attendance records for a week for active classes.
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

public class BatchAttendanceCreateHelper {


	public static void insertAttendance(List<Class__c> scope) {
		try {
			Date today = Date.today();
			Date startOfWeek = Date.today().toStartOfWeek();
			List<Attendance__c> attendanceToInsert = new List<Attendance__c> ();
			Map<String, Id> recordTypesIdByProgram = new Map<String, Id> ();
			Map<Id, List<Integer>> numberOfDaysByClassId = new Map<Id, List<Integer>> ();
			Map<Id, String> programByClassId = new Map<Id, String> ();
			Set<Id> classId = new Set<Id> ();

			//** attendance Create Settings By Picklist Api Name
			Map<String, List<AttendanceCreateSetting__mdt>> attendanceCreateSettingsByPicklistApiName = ClassRosterTriggerHandler.getAttendanceCreateSettings();
			//** End attendance Create Settings By Picklist Api Name

			for (Class__c cls : scope) {
				numberOfDaysByClassId.put(cls.Id, ClassRosterTriggerHandler.listOfClassDays(cls));
				classId.add(cls.Id);

				//**Populate Map with Class Id and Program
				for (String program : attendanceCreateSettingsByPicklistApiName.keySet()) {
					if (ObjectUtils.HasSObjectField('Class__c', program)) {
						programByClassId.put(cls.Id, (String) cls.get(program));
					}
				} 
			}

			List<Session__c> sessions = [SELECT id, Date__c, Class__c FROM Session__c WHERE Class__c in :classId];
			Map<Id, Map<Date, Session__c>> sessionsByDateByClassId = new Map<Id, Map<Date, Session__c>> ();
			if (!sessions.isEmpty()) {
				for (Session__c session : sessions) {
					if (sessionsByDateByClassId.containsKey(session.Class__c)) {
						Map<Date, Session__c> sessionByDate = sessionsByDateByClassId.get(session.Class__c);
						sessionByDate.put(session.Date__c, session);
						sessionsByDateByClassId.put(session.Class__c, sessionByDate);
					} else {
						sessionsByDateByClassId.put(session.Class__c, new Map<Date, Session__c> { session.Date__c => session });
					}
				}
			}


			List<Class_Roster__c> classRosters = [SELECT Id, Class__c FROM Class_Roster__c WHERE Class__c in :classId AND Start_Date__c <= :today AND Status__c = :GlobalVariable.CLASS_ROSTER_STATUS AND Class__r.Auto_Create_Attendance__c = True];

			//**if object has field RecordTypeId
			if (ObjectUtils.HasSObjectField('Attendance__c', 'RecordTypeId')) {
				recordTypesIdByProgram = ClassRosterTriggerHandler.getRecordTypesIdByProgram(attendanceCreateSettingsByPicklistApiName);
			} //** End if object has field RecordTypeId

			if (!scope.isEmpty()) {
				for (Class_Roster__c classRoster : classRosters) {
					if (!numberOfDaysByClassId.get(classRoster.Class__c).isEmpty()) {
						for (Integer day : numberOfDaysByClassId.get(classRoster.Class__c)) {
							Date dateOfClass = startOfWeek.addDays(day);
							Id recordTypeId = recordTypesIdByProgram.get(programByClassId.get(classRoster.Class__c));
							Id sessionId = sessionsByDateByClassId.get(classRoster.Class__c).get(dateOfClass).Id;

							attendanceToInsert.add(ClassRosterTriggerHandler.createAttendance(classRoster, dateOfClass, recordTypeId, sessionId));
						}
					}
				}
			}
			System.debug('attendanceToInsert '+attendanceToInsert);
			if (!attendanceToInsert.isEmpty()) {
				insert attendanceToInsert;
			}
			if(Test.isRunningTest()) {
                throw new ListException();
            }
		}
		catch(Exception ex) {
			System.debug('Exception ' + ex.getMessage());
			Exception_Log__c exceptionLog = new Exception_Log__c();
			exceptionLog.Exception_Type__c = ex.getTypeName();
			exceptionLog.Line_Number__c = ex.getLineNumber();
			exceptionLog.Message__c = ex.getMessage();
			exceptionLog.Stack_Trace__c = ex.getStackTraceString();
			insert exceptionLog;	
		}
	}



}