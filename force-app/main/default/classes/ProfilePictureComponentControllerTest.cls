@isTest
public class ProfilePictureComponentControllerTest {
    
    @TestSetup
    public static void TestSetup(){
        TestFactory.createTestForPhotoComponent();
        
    }
    
    @isTest 
    public static void  getProfilePictureTest(){
        Contact con = [Select Id From Contact];
        Test.startTest();
        ProfilePictureComponentController.saveAttachment(con.Id, 'test Con', 'test string for the image', 'image/png');
        Attachment att = [Select Id, Name, LastModifiedDate, ContentType  FROM Attachment];
        System.assertEquals(ProfilePictureComponentController.getProfilePicture(con.Id), att);
        Test.stopTest();
        
    } 
    @isTest 
    public static void  saveAttacmentTest(){
        Contact con = [Select Id From Contact];
        Test.startTest();
        Id Att1 =  ProfilePictureComponentController.saveAttachment(con.Id, 'test Con', 'test string for the image', 'image/png');
        Test.stopTest();
        Attachment att = [Select Id  FROM Attachment];
        System.assertEquals(att.Id, Att1);
    }
}