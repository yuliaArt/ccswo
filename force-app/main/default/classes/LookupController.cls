//**
// Author: Jitendra Zaa
// Date: July 4, 2017
// Description: Controller for Lookup Lightning Component
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

public class LookupController {

    /**
     * Returns JSON of list of ResultWrapper to Lex Components
     * @objectName - Name of SObject
     * @selectString - Comma delimited list of fields to be returned in addition to Name and Id
     * @lim   - Total number of record to be returned
     * @fld_API_Search - API name of field to be searched
     * @searchText - text to be searched
     * */
    @AuraEnabled
    public static String getObjectNameServer(String objectName) {
        return Schema.getGlobalDescribe().get(objectName).getDescribe().getLabelPlural();
    }

    @AuraEnabled
    public static String getIconName(String objectName) {
        // getIconName function taken from salesforce.stackexchange.com user Kumar
        // https://salesforce.stackexchange.com/questions/116688/lightning-get-sobject-tab-icon
        String u;
        List<Schema.DescribeTabSetResult> tabSetDesc = Schema.describeTabs();
        List<Schema.DescribeTabResult> tabDesc = new List<Schema.DescribeTabResult>();
        List<Schema.DescribeIconResult> iconDesc = new List<Schema.DescribeIconResult>();

        for (Schema.DescribeTabSetResult tsr : tabSetDesc) {
            tabDesc.addAll(tsr.getTabs());
        }

        for (Schema.DescribeTabResult tr : tabDesc) {
            if (objectName == tr.getSobjectName()) {
                if (tr.isCustom() == true) {
                    iconDesc.addAll(tr.getIcons());
                } else {
                    u = 'standard:' + objectName.toLowerCase();
                }
            }
        }
        for (Schema.DescribeIconResult ir : iconDesc) {
            if (ir.getContentType() == 'image/svg+xml') {
                u = 'custom:' + ir.getUrl().substringBetween('custom/', '.svg').substringBefore('_');
                break;
            }
        }
        return u;
    }

    @AuraEnabled
    public static List<SObject> searchDB(String objectName, String selectString,
            String fld_API_Search, String searchText, Integer lim) {




        searchText = '\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';

        String query = 'SELECT Id, ' + fld_API_Search;
        if(selectString.length() != 0) {
            query += ', ' + selectString;
        }

        query +=' FROM ' + objectName +
                ' WHERE ' + fld_API_Search + ' LIKE ' + searchText +
                ' LIMIT ' + lim;

        //throwAuraException('query: ' + query);

        List<sObject> sobjList = Database.query(query);

        return sobjList;
    }
}