@isTest
public class BatchExceptionLogSendEmailTest  {

	@testSetup static void initialize() {
		//TestFactory.createUaserAndGroupTestDAta();
        TestFactory.createDataForBatchExceptionLogSendEmail();
    }

	@isTest
    static void startBatchExceptionlogSendEmailTest() {
		Group testGroup = new Group();	
		testGroup.Name = 'Exception Receivers';
		insert testGroup;

		System.debug('testGroup '+[select id, name, developerName FROM Group WHere DeveloperName = 'Exception_Receivers']);

		Group testGroupException = [select id, name, developerName FROM Group WHere DeveloperName = 'Exception_Receivers' LIMIT 1];

		GroupMember gM = new GroupMember();
		gM.GroupId = testGroupException.Id;
		gM.UserOrGroupId = UserInfo.getUserId();
		insert gM;

		System.debug('group Member '+gM);
		User testUser = [SELECT id, name FROM User LIMIT 1];

        Test.startTest();
		BatchExceptionLogSendEmail obj = new BatchExceptionLogSendEmail();
		DataBase.executeBatch(obj); 
        Test.stopTest();
    }
}