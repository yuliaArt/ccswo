public class GetFieldTypeController  {
	@AuraEnabled
	public static String getFieldType(String objectName, String fieldName){
		return String.valueOf(Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().get(fieldName).getDescribe().getType());
	}
}