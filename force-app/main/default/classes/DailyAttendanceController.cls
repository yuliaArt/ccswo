public class DailyAttendanceController {

	//Method for fetching the records based on Date and RecordId(ClassId)
	@AuraEnabled
	public static List<Attendance__c> serverGetAttendanceRecords(Id classId, String dateString) {
		List<DailyAttendanceSetting__mdt> dailyAttendanceSettings = dailyAttendanceSettings();
		Date attendanceDate = Date.valueOf(dateString);

		String dailyAttendanceFields = '';
		for (Integer i = 0; i < dailyAttendanceSettings.size(); i++){
			dailyAttendanceFields += dailyAttendanceSettings.get(i).Field_API_Name__c + ', ';
		}

		String query = 'SELECT Id, ' + dailyAttendanceFields + 'Class_Roster__r.Class__c, Class_Roster__r.Contact__r.Name  FROM Attendance__c WHERE Date__c =: attendanceDate AND Class_Roster__r.Class__c = :classId';
	
		//System.debug('query ' + query);
		List<Attendance__c> attList = Database.query(query);
		//System.debug('attList ' + attList);
		return attList;
	}

	//Method for Updating the records
	@AuraEnabled
	public static void serverUpdateAttendanceRecord(List<Attendance__c> attendanceUpdate) {
		update attendanceUpdate;
	}

	//Method for geting daily Attendance setting from Custom matadata type
	@AuraEnabled
	public static List<DailyAttendanceSetting__mdt> dailyAttendanceSettings(){
		List<DailyAttendanceSetting__mdt> dailyAttendanceSettings = [SELECT id, MasterLabel, Field_API_Name__c, Object_API_Name__c, Order__c, IsHeader__c FROM DailyAttendanceSetting__mdt WHERE IsVisible__c = TRUE ORDER BY Order__c];
		
		return dailyAttendanceSettings;
	}
	
}