@isTest
public class TestFactory {

	public static SObject createSObject(SObject sObj) {
		// Check what type of object we are creating and add any defaults that are needed.
		String objectName = String.valueOf(sObj.getSObjectType());
		// Construct the default values class. Salesforce doesn't allow '__' in class names
		String defaultClassName = 'TestFactory.' + objectName.replaceAll('__(c|C)$|__', '') + 'Defaults';
		// If there is a class that exists for the default values, then use them
		if (Type.forName(defaultClassName) != null) {
			sObj = createSObject(sObj, defaultClassName);
		}
		return sObj;
	}

	public static SObject createSObject(SObject sObj, Boolean doInsert) {
		SObject retObject = createSObject(sObj);
		if (doInsert) {
			insert retObject;
		}
		return retObject;
	}

	public static SObject createSObject(SObject sObj, String defaultClassName) {
		// Create an instance of the defaults class so we can get the Map of field defaults
		Type t = Type.forName(defaultClassName);
		if (t == null) {
			Throw new TestFactoryException('Invalid defaults class.');
		}
		FieldDefaults defaults = (FieldDefaults) t.newInstance();
		addFieldDefaults(sObj, defaults.getFieldDefaults());
		return sObj;
	}

	public static SObject createSObject(SObject sObj, String defaultClassName, Boolean doInsert) {
		SObject retObject = createSObject(sObj, defaultClassName);
		if (doInsert) {
			insert retObject;
		}
		return retObject;
	}

	public static SObject[] createSObjectList(Sobject sObj, Integer numberOfObjects) {
		return createSObjectList(sObj, numberOfObjects, (String) null);
	}

	public static SObject[] createSObjectList(SObject sObj, Integer numberOfObjects, Boolean doInsert) {
		SObject[] retList = createSObjectList(sObj, numberOfObjects, (String) null);
		if (doInsert) {
			insert retList;
		}
		return retList;
	}

	public static SObject[] createSObjectList(SObject sObj, Integer numberOfObjects, String defaultClassName, Boolean doInsert) {
		SObject[] retList = createSObjectList(sObj, numberOfObjects, defaultClassName);
		if (doInsert) {
			insert retList;
		}
		return retList;
	}

	public static SObject[] createSObjectList(Sobject sObj, Integer numberOfObjects, String defaultClassName) {
		SObject[] sObjs = new SObject[] { };
		SObject newObj;
		Boolean nameIsAutoNumber;
		
		// Get one copy of the object
		if (defaultClassName == null) {
			newObj = createSObject(sObj);
		} else {
			newObj = createSObject(sObj, defaultClassName);
		}

		// Get the name field for the object
		String nameField = nameFieldMap.get(String.valueOf(sObj.getSObjectType()));
		if (nameField == null) {
			nameField = 'Name';
		}
		nameIsAutoNumber = sObj.getSobjectType().getDescribe().fields.getMap().get('name').getDescribe().isAutoNumber();

		// Clone the object the number of times requested. Increment the name field so each record is unique
		for (Integer i = 0; i<numberOfObjects; i++) {
			SObject clonedSObj = newObj.clone(false, true);
			if (!nameIsAutoNumber) {
				clonedSObj.put(nameField, (String) clonedSObj.get(nameField) + ' ' + i);
			}
			sObjs.add(clonedSObj);
		}
		return sObjs;
	}

	private static void addFieldDefaults(SObject sObj, Map<Schema.SObjectField, Object> defaults) {
		// Loop through the map of fields and if they weren't specifically assigned, fill them.
		Map<String, Object> populatedFields = sObj.getPopulatedFieldsAsMap();
		for (Schema.SObjectField field : defaults.keySet()) {
			if (!populatedFields.containsKey(String.valueOf(field))) {
				sObj.put(field, defaults.get(field));
			}
		}
	}

	// When we create a list of SObjects, we need to
	private static Map<String, String> nameFieldMap = new Map<String, String> {
		'Contact'                                                    => 'LastName',
		'Case'                                                       => 'Subject'
	};

	public class TestFactoryException extends Exception { }

	// Use the FieldDefaults interface to set up values you want to default in for all objects.
	public interface FieldDefaults {
		Map<Schema.SObjectField, Object> getFieldDefaults();
	}

	// To specify defaults for objects, use the naming convention [ObjectName]Defaults.
	// For custom objects, omit the __c from the Object Name

	public class AccountDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Account.Name                                        => 'Test Account'
			};
		}
	}

	public class ContactDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Contact.FirstName                                   => 'First',
				Contact.LastName                                    => 'Last'
			};
		}
	}

	public class OpportunityDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Opportunity.Name                                    => 'Test Opportunity',
				Opportunity.StageName                               => 'Closed Won',
				Opportunity.CloseDate                               => System.today()
			};
		}
	}

	public class CaseDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Case.Subject                                        => 'Test Case'
			};
		}
	}

	public class Trigger_SettingsDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Trigger_Settings__c.Disable_ClassRosterTrigger__c   => false,
				Trigger_Settings__c.Disable_ServiceTrigger__c       => false,
				Trigger_Settings__c.Disable_AttendanceTrigger__c    => false,
				Trigger_Settings__c.Disable_ClassTrigger__c         => false
			};
		}
	}

	public class ClassDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Class__c.Name                                       => 'Testing Class',
				Class__c.Monday__c                                  => true,
				Class__c.Tuesday__c                                 => true,
				Class__c.Wednesday__c								=> true,
				Class__c.Auto_Create_Attendance__c                  => true,
				Class__c.Start_Date__c                              => Date.today().toStartOfWeek(),
				Class__c.End_Date__c                                => Date.today().addDays(365),
				Class__c.Status__c                                  => 'In Progress'
			};
		}
	}

	public class Class_RosterDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Class_Roster__c.Status__c                           => GlobalVariable.CLASS_ROSTER_STATUS,//'Training',
				Class_Roster__c.Start_Date__c                       => Date.today().toStartOfWeek(),
				Class_Roster__c.End_Date__c                         => Date.today().toStartOfWeek().addDays(30)
			};
		}
	}

	public class AttendanceDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Attendance__c.Status__c                             => 'Absent'
			};
		}
	}

	public class Birdseye_EventDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Birdseye_Event__c.Name                              => 'Test Name'
			};
		}
	}

	public class GoalDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Goal__c.Name                                        => 'Test Name'
			};
		}
	}

	public class Action_StepDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Action_Step__c.Name                                 => 'Test Name'
			};
		}
	}

	public class Service_planDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Service_plan__c.Name                                => 'Test Name'
			};
		}
	}

	public class ServiceDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				//Service__c.Name                                     => 'Test Name',
				Service__c.Date_of_Service__c                       => Date.today()
			};
		}
	}

	public class Program_EnrollmentDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Program_Enrollment__c.Name                           => 'Test Name',
				Program_Enrollment__c.Start_Date__c                  => Date.today()
			};
		}
	}

	public class Exception_LogDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Exception_Log__c.Stack_Trace__c                      => 'Class.TestClassName.testMethod: line 10, column 1',
				Exception_Log__c.Message__c                          => 'Test failed',
				Exception_Log__c.Line_Number__c                      => 10,
				Exception_Log__c.Exception_Type__c                   => 'System.DmlException'
			};
		}
	}

	public class UserDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				User.Alias                                           => 'standt',
				User.Email                                           => 'standarduser@testorg.com',
				User.EmailEncodingKey                                => 'UTF-8',
				User.LastName                                        => 'Testing',
				User.LanguageLocaleKey                               => 'en_US',
				User.LocaleSidKey                                    => 'en_US',
				User.TimeZoneSidKey                                  => 'America/Los_Angeles',
				User.UserName                                        => 'standarduser@testbirdeye.com'
			};
		}
	}

	public class GroupDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Group.Name                                           => 'Test Group'
			};
		}
	}

	public class SessionDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Session__c.Date__c                                   => Date.today()
			};
		}
	}

	public class Income_DetailDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Income_Detail__c.Amount__c                           => 100,
				Income_Detail__c.Frequency__c                        => 'Weekly',
				Income_Detail__c.Include_in_Household_Income__c      => true
			};
		}
	}

	public class Poverty_LevelDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Poverty_Level__c.Year__c							 => String.valueOf(Date.today().year()),
				Poverty_Level__c.Base_Amount__c						 => 100,
				Poverty_Level__c.Incremental_Amount__c				 => 10
			};
		}
	}

	public class AMY_LevelDefaults implements FieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				AMI_Level__c.Name    								 => 'Test Name',
				AMI_Level__c.Amount__c								 => 100,
				AMI_Level__c.Percentage__c							 => 10,
				AMI_Level__c.Year__c								 => String.valueOf(Date.today().year()),
				AMI_Level__c.Number_In_Household__c					 => 1
			};
		}
	}

	public static void createUaserAndGroupTestDAta() {
		Profile p = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
		User testUser = (User) TestFactory.createSObject(new User(ProfileId = p.Id), 'TestFactory.UserDefaults', true);
		Group testGroup = (Group) TestFactory.createSObject(new Group(), 'TestFactory.GroupDefaults', true);

		GroupMember member = new GroupMember();
		member.UserOrGroupId = testUser.id;
		member.GroupId = testGroup.Id;
		insert member;


	}

	public static void createDataForBatchExceptionLogSendEmail() {
		Trigger_Settings__c disableTrigger = (Trigger_Settings__c) TestFactory.createSObject(new Trigger_Settings__c(), 'TestFactory.Trigger_SettingsDefaults', true);
		Exception_Log__c exceptionLog = (Exception_Log__c) TestFactory.createSObject(new Exception_Log__c(), 'TestFactory.Exception_LogDefaults', true);
	}

	public static void createDataForBatchExceptionDeleteOld() {
		Trigger_Settings__c disableTrigger = (Trigger_Settings__c) TestFactory.createSObject(new Trigger_Settings__c(), 'TestFactory.Trigger_SettingsDefaults', true);
		Exception_Log__c exceptionLog = (Exception_Log__c) TestFactory.createSObject(new Exception_Log__c(), 'TestFactory.Exception_LogDefaults', true);
	}

	public static void createDataForBatchAttendance() {
		Trigger_Settings__c disableTrigger = (Trigger_Settings__c) TestFactory.createSObject(new Trigger_Settings__c(),'TestFactory.Trigger_SettingsDefaults', true);
        Contact con = (Contact)TestFactory.createSObject(new Contact(),'TestFactory.ContactDefaults', true);
        Class__c cls = (Class__c)TestFactory.createSObject(new Class__c(),'TestFactory.ClassDefaults', true);
        Class_Roster__c clsroster = (Class_Roster__c)TestFactory.createSObject(new Class_Roster__c(Class__c = cls.id,Contact__c = con.id),'TestFactory.Class_RosterDefaults', true);
	}


	public static void createDataForClassRosterTrigger() {
		Trigger_Settings__c disableTrigger = (Trigger_Settings__c) TestFactory.createSObject(new Trigger_Settings__c(), 'TestFactory.Trigger_SettingsDefaults', true);
		Contact con = (Contact) TestFactory.createSObject(new Contact(), 'TestFactory.ContactDefaults', true);
		Class__c cls = (Class__c) TestFactory.createSObject(new Class__c(Maximum_Enrollment__c = 1), 'TestFactory.ClassDefaults', true);
		Class_Roster__c clsroster = (Class_Roster__c) TestFactory.createSObject(new Class_Roster__c(Class__c = cls.id, Contact__c = con.id, End_Date__c = Date.today().addDays(1)), 'TestFactory.Class_RosterDefaults', true);
	}

	public static void createDataForAttendanceController() {
		Trigger_Settings__c disableTrigger = (Trigger_Settings__c) TestFactory.createSObject(new Trigger_Settings__c(), 'TestFactory.Trigger_SettingsDefaults', true);
		Contact con = (Contact) TestFactory.createSObject(new Contact(), 'TestFactory.ContactDefaults', true);
		Class__c cls = (Class__c) TestFactory.createSObject(new Class__c(), 'TestFactory.ClassDefaults', true);
		Class_Roster__c clsroster = (Class_Roster__c) TestFactory.createSObject(new Class_Roster__c(Class__c = cls.id, Contact__c = con.id), 'TestFactory.Class_RosterDefaults', true);    
	}

	public static void eventParticipantControllerTest() {
		Contact con = (Contact) TestFactory.createSObject(new Contact(), 'TestFactory.ContactDefaults', true);
		Birdseye_Event__c birdEvent = (Birdseye_Event__c) TestFactory.createSObject(new Birdseye_Event__c(), 'TestFactory.Birdseye_EventDefaults', true);
	}

	public static void createDataForCloneRecordController() {
		Service_plan__c serPlan = (Service_plan__c) TestFactory.createSObject(new Service_plan__c(), 'TestFactory.Service_planDefaults', true);
		Action_Step__c actiStep = (Action_Step__c) TestFactory.createSObject(new Action_Step__c(Service_plan__c = serPlan.id), 'TestFactory.Action_StepDefaults', true);
		Goal__c goal = (Goal__c) TestFactory.createSObject(new Goal__c(Service_plan__c = serPlan.id), 'TestFactory.GoalDefaults', true);
	}

	public static void createTestForPhotoComponent() {
		Contact con = (Contact) TestFactory.createSObject(new Contact(), 'TestFactory.ContactDefaults', true);
	}

	public static void testServiceTrigger() {
		Trigger_Settings__c disableTrigger = (Trigger_Settings__c) TestFactory.createSObject(new Trigger_Settings__c(), 'TestFactory.Trigger_SettingsDefaults', true);
		Contact con = (Contact) TestFactory.createSObject(new Contact(), 'TestFactory.ContactDefaults', true);
		Program_Enrollment__c pe = (Program_Enrollment__c) TestFactory.createSObject(new Program_Enrollment__c(Contact__c = con.Id), 'TestFactory.Program_EnrollmentDefaults', true);
	}

	public static void createDataForRecordTypePickList() {
		Trigger_Settings__c disableTrigger = (Trigger_Settings__c) TestFactory.createSObject(new Trigger_Settings__c(), 'TestFactory.Trigger_SettingsDefaults', true);
		Service__c service = (Service__c) TestFactory.createSObject(new Service__c(), 'TestFactory.ServiceDefaults', true);
	}

	public static void createDataForClassTrigger() {
		Trigger_Settings__c disableTrigger = (Trigger_Settings__c) TestFactory.createSObject(new Trigger_Settings__c(), 'TestFactory.Trigger_SettingsDefaults', true);
		Class__c cls = (Class__c) TestFactory.createSObject(new Class__c(), 'TestFactory.ClassDefaults', true);
	}

	public static void createDataForBatchAudit() {
		Trigger_Settings__c disableTrigger = (Trigger_Settings__c) TestFactory.createSObject(new Trigger_Settings__c(),'TestFactory.Trigger_SettingsDefaults', true);
        Contact con = (Contact)TestFactory.createSObject(new Contact(),'TestFactory.ContactDefaults', true);
        
		List<Program_Enrollment__c> programEnrollments = TestFactory.createSObjectList(new Program_Enrollment__c(Contact__c = con.id, Status__c = 'Enrolled'), 2, 'TestFactory.Program_EnrollmentDefaults', true);
	}

	public static void calculatePovertyLevelControllerTestData() {
		Trigger_Settings__c disableTrigger = (Trigger_Settings__c) TestFactory.createSObject(new Trigger_Settings__c(),'TestFactory.Trigger_SettingsDefaults', true);
		Account testAccount = (Account) TestFactory.createSObject(new Account(npsp__Number_of_Household_Members__c = 1, Self_Reported_Income__c = 1000), 'TestFactory.AccountDefaults', true);
		Income_Detail__c testIncomeDetail = (Income_Detail__c) TestFactory.createSObject(new Income_Detail__c(Account__c = testAccount.id), 'TestFactory.Income_DetailDefaults', true);
		Poverty_Level__c testPovertyLevel = (Poverty_Level__c) TestFactory.createSObject(new Poverty_Level__c(), 'TestFactory.Poverty_LevelDefaults', true);
		AMI_Level__c testAmiLevel = (AMI_Level__c) TestFactory.createSObject(new AMI_Level__c(), 'TestFactory.AMY_LevelDefaults', true);
	}

}