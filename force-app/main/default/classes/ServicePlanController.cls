//**
// Author: Tejashree Deshpande
// Description: Controller to display Goal, Care plan and action step details
// Revised: 
//      Author: Ivanna Kuzemchak
//      Date: December 13, 2018
//      Description: Class was modified because of difference in data model.
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

public with sharing class ServicePlanController {
    public Service_Plan__c care { get; set; }
    //List for Goal__c
    //public List<Goal__c> allGoalsRec { get; set; }
    public List<Goal__c> goalsRecords { get; set; }
    //List for Care_Plan_Action__c
    //public List<Action_Step__c> allActionStepRec { get; set; }
    public List<Action_Step__c> actionStepRec { get; set; }
    // Map to find related action steps
   // public Map<Id, List<Action_Step__c>> mapGoalWiseActions { get; set; }


    public ServicePlanController(ApexPages.StandardController controller) {
        //care plan id from current page
        Id careId = (Id) ApexPages.currentPage().getParameters().get('id');
        // showActionRec = false;
        care = queryCarePlan(careId);
    }

    //Constructor to display data on print VF page
    public ServicePlanController() {
        //care plan id from current page
        Id careId = (Id) ApexPages.currentPage().getParameters().get('id');

        care = queryCarePlan(careId);
        //Goals data in list
    }

    public Service_Plan__c queryCarePlan(Id careId) {

        // this function uses the SObjectAggregator class - see that class definition
        // for a detailed description of its inner workings.
        List<Service_Plan__c> carePlans = [
                SELECT Id,
                        Name,
                        Program_Enrollment__r.Name,
                        Program_Enrollment__r.Contact__r.Full_Name__c,
                        Target_Date__c,
                        Start_Date__c,
                        End_Date__c,
                        Notes__c,
                        CreatedBy.Name
                FROM Service_Plan__c
                WHERE Id = :careId
        ];

        List<Goal__c> goals = [
                SELECT Id,
                        Name,
                       // Goal_Number__c,
                        Barrier__c,
                        Plan_to_Meet_Goal__c,
                        Service_Plan__c,
                        Outcome__c,
                        Incentive__c
                FROM Goal__c
                WHERE Service_Plan__r.Id = :careId
        ];

        List<Action_Step__c> steps = [
                SELECT Id,
                        Name,
                        Goal__c,
                        //Step_Number__c,
                        Notes__c,
                        Due_Date__c,
                        Completion_Date__c,
                        Start_Date__c
                FROM Action_Step__c
                WHERE  Service_Plan__r.Id = :careId
        ];
		

        SObjectAggregator.Node stepsNode = new SObjectAggregator.Node();
        stepsNode.sObjects = steps;
        stepsNode.childsideFieldName = 'Goal__c';

        SObjectAggregator.Node goalsNode = new SObjectAggregator.Node();
        goalsNode.sObjects = goals;
        goalsNode.childNode = stepsNode;
        goalsNode.parentsideRelationshipName = 'Action_Steps__r';
        goalsNode.childsideFieldName = 'Service_Plan__c';

        SObjectAggregator.Node carePlansNode = new SObjectAggregator.Node();
        carePlansNode.sObjects = carePlans;
        carePlansNode.childNode = goalsNode;
        carePlansNode.parentsideRelationshipName = 'Goals__r';

        List<Service_Plan__c> carePlansWithChildren = (List<Service_Plan__c>) SObjectAggregator.wireUpSObjectHeirarchy(carePlansNode);
		System.debug('carePlansWithChildren '+carePlansWithChildren);
        return carePlansWithChildren[0];
    }

    public List<Goal__c> getGoals() {
        return goalsRecords;
    }
    public List<Action_Step__c> getActionStep() {
        return actionStepRec;
    }
}