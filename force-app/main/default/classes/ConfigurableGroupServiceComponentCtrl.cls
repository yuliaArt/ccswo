//**
// Author: Shashank Parashar
// Date: July 30, 2018
// Description: This class will going to create the services based on the selection of the program on UI.
// User will also choose the group Id for particular program.
// Revised:
//      Author: Ivanna Kuzemchak
//      Date: December 14, 2018
//      Description: Class was changed to be more flexible. It accepts fields api and record type from UI in order 
//      to create service records.
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

public with sharing class ConfigurableGroupServiceComponentCtrl {

    //Calling servicewrapper class.
    @AuraEnabled
    public static ServiceWrapper newServiceWrapper() {
        return new ServiceWrapper();
    }
    
    //Method to get the program list based on the selected program on the UI.
    //This method will return the List of contact wrapper.
    @AuraEnabled
    public static List<ContactWrapper> getProgramEnrollments(String programSelectedValue, Id groupId) {
        List<ContactWrapper> contactWrapperList = new List<ContactWrapper>();

            System.debug(
                'programSelectedValue '+programSelectedValue
            );
            System.debug(
                'groupId '+groupId
            );
        //Condition to check the group Id is null or not.
        if(groupId == Null){
            List<Program_Enrollment__c> prog = [Select Id , Name , Contact__c, Contact__r.Name, Program__c  from Program_Enrollment__c where Program__c =: programSelectedValue and Contact__c != Null /*And Active__c = true*/];
            System.debug(
                'prog '+prog
            );
            for(Program_Enrollment__c pr:prog){
                List<Program_Enrollment__c> prList = new List<Program_Enrollment__c>();
                //Filling contact wrapper list for return to the method.
                contactWrapperList.add(new ContactWrapper(false, pr.contact__r.Name, pr.name, pr.Contact__c, prList, 0, pr.id));
            }
            return contactWrapperList;
        }
        else {
            List<Group_Member__c> grpList = [Select Id, Contact__c, Group__c from Group_Member__c where Group__c=:groupId];
            Set<Id> groupIds = New Set<Id>();
            for(Group_Member__c eachGroupvar: grpList){
                groupIds.add(eachGroupvar.Contact__c);
            }
            List<Program_Enrollment__c> prog = [Select Id, Name, Contact__c, Contact__r.Name, Program__c from Program_Enrollment__c where Program__c =: programSelectedValue and Contact__c = :groupIds and Contact__c != Null /*And Active__c = true*/];
            for(Program_Enrollment__c pr:prog){
                List<Program_Enrollment__c> prList = new List<Program_Enrollment__c>();
                contactWrapperList.add(new ContactWrapper(false, pr.contact__r.Name, pr.name, pr.Contact__c, prList, 0, pr.id));
            }
            return contactWrapperList;
        }
    }

    @AuraEnabled
    public static user fetchUser(){
        // query current user information  
        User oUser = [select id,Name,FirstName,LastName,IsActive FROM User Where id =: userInfo.getUserId()];
        return oUser;
    }

    @AuraEnabled
    public static List<String> convertStrToList(String str) {
        List<String> listToReturn = new List<String>();
        if(String.isNotBlank(str)) {
            for(String eachStr : str.split(',')) {
                listToReturn.add(eachStr.deleteWhitespace());
            }
        }
        return listToReturn;
    }

    @AuraEnabled
    public static void insertRecords(String contactWrappers, String eventFields, Id groupId, String rcdTypeId) {

        List<ContactWrapper> contactWrapperList =
            (List<ContactWrapper>) System.JSON.deserialize(contactWrappers, List<ContactWrapper>.class);
        Map<String, String> mapString = (Map<String, String>) System.JSON.deserialize(eventFields, Map<String, String>.class);

        //Perform Operation with records
        List<Service__c> newServices = new List<Service__c>();

        //Map Fields api and Type
        Map<String, Schema.DisplayType> mapFields = new Map<String, Schema.DisplayType>();
        Map<String, Schema.SObjectField> serviseFields = Schema.SObjectType.Service__c.fields.getMap();
        for (String str : mapString.keySet()) {
            Schema.DisplayType flType = serviseFields.get(str.toLowerCase()).getDescribe().getType();
            mapFields.put(str, flType);
        }

        for (ContactWrapper contactWrap : contactWrapperList) {
            if (contactWrap.isSelected == true) {

                Service__c srv = new Service__c();
                // if ((String)mapString.get('Service_Type__c') != '') {
                //     srv.Name = (String)mapString.get('Service_Type__c') + ' - ' + (String)mapString.get('Date_of_Service__c');
                // } else {
                //     srv.Name = 'Group Service' + ' - ' + (String)mapString.get('Date_of_Service__c');
                // }
                srv.Client__c = contactWrap.contactId;
                srv.Program_Enrollment__c = contactWrap.selectedProgm;
				if(doesFieldExist('Service_Type', 'RecordtypeId')){
					//srv.RecordtypeId = rcdTypeId;
					srv.put('RecordtypeId', rcdTypeId);
				}
                for (String fieldApi : mapString.keySet()) {

                    if (String.valueof(mapFields.get(fieldApi)) == 'DATE') {
                        srv.put(fieldApi, Date.valueOf(mapString.get(fieldApi)));
                    } else if (String.valueof(mapFields.get(fieldApi)) == 'DOUBLE') {
                        srv.put(fieldApi, Decimal.valueOf(mapString.get(fieldApi)));
                    } else {
                        srv.put(fieldApi, mapString.get(fieldApi));
                    }                
                }
                
				System.debug('srv'+srv);
                newServices.add(srv);
            
            }
           
        }
        insert newServices;
    }
	public static boolean doesFieldExist(String objName, string fieldName){
        try {
            SObject so = Schema.getGlobalDescribe().get(objName).newSObject();
            return so.getSobjectType().getDescribe().fields.getMap().containsKey(fieldName);
        }
        catch(Exception ex) {
			System.debug('Exception ' + ex.getMessage());
		}
         
        return false;
    }
}