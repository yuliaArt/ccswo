public class BatchExceptionLogSendEmailHelper  {
	
	public static String buildExceptionEmail(List<Exception_Log__c> scope){
		List<Exception_Log__c> newExceptionLog = new List<Exception_Log__c>();
		String attachmentBody = 'Exception Type, Stack Trace, Message, Created Date\n';
		for(Exception_Log__c exceptionLog: scope){
			attachmentBody += '"' + exceptionLog.Exception_Type__c + '","' + exceptionLog.Stack_Trace__c  + '","' + exceptionLog.Message__c + '","' + exceptionLog.CreatedDate + '"\n';

			exceptionLog.IsSended__c = true;
			newExceptionLog.add(exceptionLog);
		}

		if (!newExceptionLog.isEmpty()){
			update newExceptionLog;
		}

		return attachmentBody;
	}

	public static void sendEmails(String attachmentBody){
		List<id> usersIdFromPublicGroup = ObjectUtils.getUsersIdFromPublicGroup(GlobalVariable.EXCEPTION_RECEIVERS_GROUP);
		if(!usersIdFromPublicGroup.isEmpty()){
			String mailAttachmentName = 'ExceptionLogs_' + System.now().format('MM-dd-yyyy_hh-mm-ss') + '.csv';
			Messaging.EmailFileAttachment mailAttachment = ObjectUtils.setAttachment(attachmentBody, mailAttachmentName);
			Messaging.SendEmailResult[] emailResults = ObjectUtils.sendEmails('All exceptions in file', usersIdFromPublicGroup, 'Exceptions for '+System.now().format('MM/dd/yyyy'), 'System Administrator', mailAttachment);
		}
	}

}