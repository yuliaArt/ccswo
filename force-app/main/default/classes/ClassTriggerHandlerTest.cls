@isTest 
private class ClassTriggerHandlerTest {

	@testSetup static void initialize() {	
        TestFactory.createDataForClassTrigger();
    }
	
	@isTest static void updateSessionRecordUpdate() {
        List<Class__c> clsList = [SELECT Id, Start_Date__c, End_Date__c FROM Class__c];
        for (Class__c cls : clsList) {
			cls.Start_Date__c = cls.Start_Date__c.addDays(-10);
            cls.End_Date__c = cls.End_Date__c.addDays(10);
        }
        update clsList;
    }

	@isTest static void updateSessionRecordDelete() {
        List<Class__c> clsList = [SELECT Id, Start_Date__c, End_Date__c FROM Class__c];
        for (Class__c cls : clsList) {
			cls.Start_Date__c = cls.Start_Date__c.addDays(10);
            cls.End_Date__c = cls.End_Date__c.addDays(-10);
        }
        update clsList;
    }

	@isTest static void updateSessionRecordCreate() {
        List<Class__c> clsList = [SELECT Id, Start_Date__c, End_Date__c FROM Class__c];
        for (Class__c cls : clsList) {
			cls.Start_Date__c = null;
            cls.End_Date__c = null;
        }
        update clsList;

		for (Class__c cls : clsList) {
			cls.Start_Date__c = Date.today().addDays(-10);
            cls.End_Date__c = Date.today().addDays(10);
        }
        update clsList;
    }
}