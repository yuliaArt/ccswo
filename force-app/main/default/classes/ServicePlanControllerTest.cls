/** Developer Name : Tejashree 
** Description : VisualForce page rendered as pdf for Print view 
**/
@isTest
public class ServicePlanControllerTest {
    
    //method
    static testmethod void validateExtensionController(){
        
        //define list for different objects
        list<Contact> contactList= new list<Contact>();
        list<Program_Enrollment__c> programEnrollmentList= new list<Program_Enrollment__c>();
        list<Service_Plan__c> careplanList= new list<Service_Plan__c>();
        list<Goal__c> goalList= new list<Goal__c>();
        list<Action_Step__c> actionStepList= new list<Action_Step__c>();
        //list<Class_Workshop__c  > classWorkshopList= new list<Class_Workshop__c>();
       
        //Insert Contact Records
        Contact ContactObj= new Contact();
        ContactObj.LastName= 'Test Yrc LastName';
        insert ContactObj;
        system.assertEquals('Test Yrc LastName', ContactObj.LastName);
        
        Intake__c intakeObj= new Intake__c();
        intakeObj.Contact__c=ContactObj.Id;
        insert intakeObj;
        
		


		//ContactObj.Intake__c= intakeObj.Id;
        //update ContactObj;

        //Trigger_Settings__c sett = new Trigger_Settings__c();
        //sett.Disable_ProgramEnrollmentTrigger__c = false;
        //insert sett;
        
        //Insert Program_Enrollment Records
        Program_Enrollment__c ProgramEnrollmentObj= new Program_Enrollment__c();
        ProgramEnrollmentObj.Contact__c= ContactObj.Id;
        programEnrollmentList.add(ProgramEnrollmentObj);
        insert programEnrollmentList;
        system.assertEquals(ContactObj.Id, ProgramEnrollmentObj.Contact__c);
        
        //Insert Care_Plan Records
        Service_Plan__c careplanObj= new Service_Plan__c();
        careplanObj.Program_Enrollment__c=ProgramEnrollmentObj.id;
        //careplanObj.Completion_Date__c=date.today();
        careplanList.add(careplanObj);
        insert careplanList;
        system.assertEquals(ProgramEnrollmentObj.id, careplanObj.Program_Enrollment__c);
        
        //Insert Goal Records
        Goal__c GoalObj= new Goal__c();
        GoalObj.Name= 'Yrc Test Goal';
        GoalObj.Service_Plan__c= careplanObj.Id;
        goalList.add(GoalObj);
        insert goalList;
        system.assertEquals(careplanObj.Id, GoalObj.Service_Plan__c);
        
        //Insert Action_Step Records
        Action_Step__c ActionStepObj= new Action_Step__c();
        ActionStepObj.Name= 'Yrc Test ActionStep';
        //ActionStepObj.Goal__c= GoalObj.id;
        ActionStepObj.Service_Plan__c = careplanObj.Id;
        actionStepList.add(ActionStepObj);
        insert actionStepList;
        //system.assertEquals(GoalObj.id, ActionStepObj.Goal__c);
        
        Test.setCurrentPage(Page.ServicePlan);
        ApexPages.currentPage().getParameters().put('id',careplanObj.Id);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(careplanObj);
        ServicePlanController CarePlanControllerobj  = new ServicePlanController(stdLead);
        CarePlanControllerobj.getGoals();
        CarePlanControllerobj.getActionStep();
        
    }
    //method2
    static testmethod void validateExtensionController1(){
        
        list<Contact> contactList= new list<Contact>();
        list<Program_Enrollment__c> programEnrollmentList= new list<Program_Enrollment__c>();
        list<Service_Plan__c> careplanList= new list<Service_Plan__c>();
        list<Goal__c> goalList= new list<Goal__c>();
        list<Action_Step__c> actionStepList= new list<Action_Step__c>();        
        
        //Insert Contact Records
        Contact ContactObj= new Contact();
        ContactObj.LastName= 'Test Yrc LastName';
        insert ContactObj;
        system.assertEquals('Test Yrc LastName', ContactObj.LastName);
        
        //Intake__c intakeObj= new Intake__c();
        //intakeObj.Contact__c=ContactObj.Id;
        //insert intakeObj;
        
        //ContactObj.Intake__c= intakeObj.Id;
        //update ContactObj;

        //Trigger_Settings__c sett = new Trigger_Settings__c();
        //sett.Disable_ProgramEnrollmentTrigger__c = false;
        //insert sett;
        
        //Insert Program_Enrollment Records
        Program_Enrollment__c ProgramEnrollmentObj= new Program_Enrollment__c();
        ProgramEnrollmentObj.Contact__c= ContactObj.Id;
        programEnrollmentList.add(ProgramEnrollmentObj);
        insert programEnrollmentList;
        system.assertEquals(ContactObj.Id, ProgramEnrollmentObj.Contact__c);
        
        //Insert Care_Plan Records
        Service_Plan__c careplanObj= new Service_Plan__c();
        careplanObj.Program_Enrollment__c=ProgramEnrollmentObj.id;
        //careplanObj.Completion_Date__c=date.today();
        careplanList.add(careplanObj);
        insert careplanList;
        system.assertEquals(ProgramEnrollmentObj.id, careplanObj.Program_Enrollment__c);
        
        //Insert Goal Records
        Goal__c GoalObj= new Goal__c();
        GoalObj.Name= 'Yrc Test Goal';
        GoalObj.Service_Plan__c= careplanObj.Id;
        goalList.add(GoalObj);
        insert goalList;
        system.assertEquals(careplanObj.Id, GoalObj.Service_Plan__c);
        
        //Insert Action_Step Records
        Action_Step__c ActionStepObj= new Action_Step__c();
        ActionStepObj.Name= 'Yrc Test ActionStep';
        //ActionStepObj.Goal__c= GoalObj.id;
        ActionStepObj.Service_Plan__c = careplanObj.Id;
        actionStepList.add(ActionStepObj);
        insert actionStepList;
        //system.assertEquals(GoalObj.id, ActionStepObj.Goal__c);
        
        Test.setCurrentPage(Page.ServicePlan);
        ApexPages.currentPage().getParameters().put('id',careplanObj.Id);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(careplanObj);
        ServicePlanController CarePlanControllerobj  = new ServicePlanController();
        CarePlanControllerobj.getGoals();
        CarePlanControllerobj.getActionStep();
        
    }
}