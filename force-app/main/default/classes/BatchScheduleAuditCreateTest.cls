@IsTest
public class BatchScheduleAuditCreateTest  {

	@TestSetup 
	public static void testSetup() {
		TestFactory.createDataForBatchAudit();
	}

	@IsTest static void executeBatchScheduleAttendanceCreateTest() {

		Test.startTest();
        String CRON_EXP = '0 0 0 31 12 ? 2050';
        String jobId = System.schedule('BatchScheduleAuditCreateTest', CRON_EXP, new BatchScheduleAuditCreate());
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
		System.assertEquals(0, ct.TimesTriggered);
		System.assertEquals('2050-12-31 00:00:00', String.valueOf(ct.NextFireTime));
		Test.stopTest();

	}

}