@isTest 
private class BatchExceptionLogDeleteOldTest {

	@testSetup 
	private static void initialize() {
		TestFactory.createDataForBatchExceptionDeleteOld();
		Exception_Log__c exceptionLog = [SELECT Id FROM Exception_Log__c];
		Datetime twoMonthsAgo = Datetime.now().addMonths(-3);
		Test.setCreatedDate(exceptionLog.Id, twoMonthsAgo);
    }

	@isTest
	private static void testName() {
		Test.startTest();
		BatchExceptionLogDeleteOld obj = new BatchExceptionLogDeleteOld();
		DataBase.executeBatch(obj); 
        Test.stopTest();
	}
}