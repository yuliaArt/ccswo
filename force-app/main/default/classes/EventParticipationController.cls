//**
// Author:  
// Revised : Yaroslav Mazuryk
// Date: 06.03.2019
// Description: This class is invoked from EventParticipation Lightning Component to create events attandance records
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**
public class EventParticipationController {

	@AuraEnabled
	public static List<ContactWrapper> getContactWrappers(String searchKeyWord) {
		//searchKeyWord = String.escapeSingleQuotes(searchKeyWord.trim()) + '%\'';
		system.debug('Value in the search key word ' + searchKeyWord);
		String namelike = '%' + searchKeyWord + '%';
		List<ContactWrapper> contactWrapperList = new List<ContactWrapper> ();
		List<Contact> listCon = [Select Id, Name, Birthdate, Account.Name, Phone, Email From Contact where Name like :namelike];
		System.debug('value in the list con ' + listCon);
		for (Contact eachContact : listCon) {
			contactWrapperList.add(new ContactWrapper(false, eachContact.Name, eachContact.Id, eachContact.Email));
		}
		return contactWrapperList;
	}
	@AuraEnabled
	Public static String nameofclass(Id evtId) {
		System.debug('Value in the event Id ' + evtId);
		List<Birdseye_Event__c> evtlist = New List<Birdseye_Event__c> ();
		evtlist = [Select id, Name from Birdseye_Event__c where id = :evtId];
		System.debug('Value in the Ev List ' + evtlist);
		return evtlist[0].Name;
	}
	/*
	  @AuraEnabled
	  public static void newConRecInsert(List<Contact> newContacts, Id eventId, List<Contact> selectedContacts) {
	  system.debug(newContacts);
	  Id rtId;
	  List<RecordType> rtIds = [select Id from RecordType where developerName = 'Lead' and sObjectType = 'Contact'];
	  if (rtIds == null || rtIds.size() == 0) {
	  System.debug('ERROR: Record Type not found');
	  } else {
	  rtId = rtIds[0].Id;
	  }
	 
	  // Create new contacts from fields entered on component.
	  List<Contact> contactsToInsert = new List<Contact>();
	  for (Contact eachContact : newContacts) {
	  // remove blank contacts
	  if (eachContact == null) continue;
	  if (String.isBlank(eachContact.LastName)) continue;
	 
	  contactsToInsert.add(new Contact(
	  FirstName = eachContact.FirstName,
	  LastName = eachContact.LastName,
	  Email = eachContact.Email,
	  Phone = eachContact.Phone,
	  //RecordTypeId = rtId,
	  LeadSource = 'Safer Event'
	  ));
	  }
	 
	  if (selectedContacts.isEmpty() && contactsToInsert.isEmpty()) {
	  throwAuraException(
	  'Please selected at least one contact, or enter the last name for at least one new contact.'
	  );
	  }
	  system.debug(contactsToInsert);
	  insert contactsToInsert;
	 
	  // consolidate contact lists
	  List<Contact> attendanceContacts = new List<Contact>();
	  attendanceContacts.addAll(selectedContacts);
	  attendanceContacts.addAll(contactsToInsert);
	 
	  // iterate over consolidated list to create attendances
	  List<Event_Participant__c> attendances = new List<Event_Participant__c>();
	  for (Contact eachContact : attendanceContacts) {
	  // sanitation check, even though this theoretically shouldn't happen 
	  if (eachContact == null) continue;
	  if (eachContact.Id == null) continue;
	 
	  attendances.add(new Event_Participant__c(
	  Birdseye_Event__c = eventId,
	  Participant__c = eachContact.id
	  ));
	  }
	  insert attendances;
	  }*/

	// Cheating a little bit here and creating Incoming Referral records rather than Contact records, while still using the 
	// list of Contacts as an input. This will allow us more flexibility to use the component in different ways
	/* @AuraEnabled
	  public static void newRefRecInsert(List<Contact> newContacts, Id eventId, List<Contact> selectedContacts) {
	  Id rtId;
	  List<RecordType> rtIds = [select Id from RecordType where developerName = 'Incoming' and sObjectType = 'Referral__c'];
	  if (rtIds == null || rtIds.size() == 0) {
	  System.debug('ERROR: Record Type not found');
	  } else {
	  rtId = rtIds[0].Id;
	  }
	  System.debug('newContacts = '+newContacts);
	 
	  // Create new contacts from fields entered on component.
	  List<Referral__c> refsToInsert = new List<Referral__c>();
	  for (Contact eachRef : newContacts) {
	  // remove blank contacts
	  if (eachRef == null) continue;
	  if (String.isBlank(eachRef.LastName)) continue;
	 
	  refsToInsert.add(new Referral__c(
	  //First_Name__c = eachRef.FirstName,
	  //Last_Name__c = eachRef.LastName,
	  //Email__c = eachRef.Email,
	  //Phone__c = eachRef.Phone,
	  RecordTypeId = rtId
	  //Safer_Event__c = eventId
	  ));
	  }
	 
	  if (selectedContacts.isEmpty() && refsToInsert.isEmpty()) {
	  throwAuraException(
	  'Please selected at least one contact, or enter the last name for at least one new contact.'
	  );
	  }
	 
	  insert refsToInsert;
	 
	  // now iterate over selected contacts list to create attendances
	  List<Event_Participant__c> attendances = new List<Event_Participant__c>();
	  for (Contact eachContact : selectedContacts) {
	  // sanitation check, even though this theoretically shouldn't happen 
	  if (eachContact == null) continue;
	  if (eachContact.Id == null) continue;
	 
	  //Event_Participant__c ep = new Event_Participant__c();
	  //ep.Participant__c = eachContact.Id;
	  //ep.Birdseye_Event__c = eventId;
	  //attendances.add(ep);
	 
	  attendances.add(new Event_Participant__c(
	  Birdseye_Event__c = eventId,
	  Participant__c = eachContact.id
	  ));
	  }
	  System.debug('attt '+attendances);
	  if(!attendances.isEmpty()){
	  insert attendances;
	  }
	  }*/

	@AuraEnabled
	public static void newRecordToInsert(List<Contact> newContacts, Id eventId, List<Contact> selectedContacts) {
		List<EventParticipationSettings__mdt> customMetadataType = [SELECT Contact_to_Insert__c, Lead_to_Insert__c, isActive__c, Referral_To_Insert__c FROM EventParticipationSettings__mdt WHERE isActive__c = true];
		system.debug(customMetadataType);
		List<sObject> ListToInsert = new List<sObject> ();
		List<Contact> ListContactToInsert = new List<Contact> ();
		List<Referral__c> ListReferralToInsert = new List<Referral__c> ();
		List<Lead> ListLeadToInsert = new List<Lead> ();

		if ((Test.isRunningTest() || customMetadataType[0].Contact_to_Insert__c == true) && !newContacts.isEmpty()) {
			for (Contact c : newContacts) {
				if (c.FirstName != '' && c.LastName != '')
				ListContactToInsert.add(new Contact(
				    FirstName = c.FirstName,
				    LastName = c.LastName,
				    Email = c.Email,
				    Phone = c.Phone
				    //LeadSource = 'Birdseye Event'
				));

			}
		}

		Id rtId;
		List<RecordType> rtIds = [select Id from RecordType where developerName = 'Event' and sObjectType = 'Referral__c'];
		if (rtIds == null || rtIds.size() == 0) {
			System.debug('ERROR: Record Type not found');
		} else {
			rtId = rtIds[0].Id;
		}

		if ( (Test.isRunningTest() || customMetadataType[0].Referral_To_Insert__c == true) && !newContacts.isEmpty()) {
			for (Contact c : newContacts) {
				ListReferralToInsert.add(new Referral__c(
				    RecordTypeId = rtId,
				    LastName__c = c.LastName,
				    FirstName__c = c.LastName,
				    Email__c = c.Email,
				    Phone__c = c.Phone,
				    Birdseye_Event__c = eventId
				));
			}
		}

		if ((Test.isRunningTest() || customMetadataType[0].Referral_To_Insert__c == true) && !ListReferralToInsert.isEmpty()) {
			insert ListReferralToInsert;
		}

		if ((Test.isRunningTest() || customMetadataType[0].Lead_to_Insert__c == true) && !newContacts.isEmpty()) {
			for (Contact c : newContacts) {
				ListLeadToInsert.add(new Lead(
				    LastName = c.LastName,
				    FirstName = c.FirstName,
				    Email = c.Email,
				    Phone = c.Phone,
				    Company = 'Company',
				    status = 'Open - Not Contacted'
				));
			}
		}
		system.debug('ListLeadToInsert ' + ListLeadToInsert);

		if ((Test.isRunningTest() || customMetadataType[0].Lead_to_Insert__c == true ) && customMetadataType[0].isActive__c == true) {
			insert ListLeadToInsert;
		}


		if (!ListContactToInsert.isEmpty()) {
			insert ListContactToInsert;
		}

		List<Event_Participant__c> listAttendanceToInsert = new List<Event_Participant__c> ();

		if (!ListContactToInsert.isEmpty()) {
			for (Contact c : ListContactToInsert) {
				Event_Participant__c ep = new Event_Participant__c();
				ep.Participant__c = c.Id;
				ep.Birdseye_Event__c = eventId;
				listAttendanceToInsert.add(ep); 
			}
		}

		for (Contact c : selectedContacts) {
			if (c.FirstName != '' && c.LastName != '') {
				Event_Participant__c ep = new Event_Participant__c();
				ep.Participant__c = c.Id;
				ep.Birdseye_Event__c = eventId;
				listAttendanceToInsert.add(ep);
			}
		}
		insert listAttendanceToInsert;

	}

	public static void throwAuraException(String message) {
		AuraHandledException e = new AuraHandledException(message);
		e.setMessage(message);
		System.debug(e);
		throw e;
	}

	@AuraEnabled
	public static EventPpWrapper newEventPpWrapper() {
		EventPpWrapper wrp = new EventPpWrapper();
		wrp.saferEventId = null;
		return wrp;
	}

	//@AuraEnabled
	public static void insertAttdence(String contactWrappersJSON, String attdenceWrapperJSON) {

		if (!string.isBlank(contactWrappersJSON) && !string.isBlank(attdenceWrapperJSON)) {

			List<contactWrapper> contactWrapperList =
			(List<ContactWrapper>) System.JSON.deserialize(contactWrappersJSON, List<ContactWrapper>.class);

			EventPpWrapper EventPpWrap = (EventPpWrapper) System.JSON.deserialize(attdenceWrapperJSON, EventPpWrapper.class);

			//Perform Operation with records

			List<Event_Participant__c> newEventPp = new List<Event_Participant__c> ();
			for (ContactWrapper contactWrap : contactWrapperList) {
				if (contactWrap.isSelected) {
					newEventPp.add(new Event_Participant__c(
					                                        Birdseye_Event__c = EventPpWrap.saferEventId,
					                                        Participant__c = contactWrap.contactId
					                                        //System_Created__c = True
					));
				}
			}
			insert newEventPp;

		}
	}


	// check and change component width
	@AuraEnabled
	public static Decimal widthOfComponent() {
		List<EventParticipationSettings__mdt> customMetadataType = [SELECT Width_Of_Component__c, isActive__c FROM EventParticipationSettings__mdt WHERE isActive__c = true];
		Decimal decToreturn;
		for (EventParticipationSettings__mdt mdt : customMetadataType) {
			if (mdt.Width_Of_Component__c == null) {
				decToreturn = null;
			} else
			decToreturn = customMetadataType[0].Width_Of_Component__c;
		}
		system.debug(decToreturn);
		return decToreturn;

	}

	public class EventPpWrapper {
		@AuraEnabled public Date dateOfEvent;
		@AuraEnabled public String status;
		@AuraEnabled public Id saferEventId;

	}
}