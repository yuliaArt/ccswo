//*
// Author: Yaroslav Mazuryk
// Date: March 3, 2019
// Description: this code set Last Attendance Date to Contact and Account
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//*

public class AttendanceTriggerHandler {

	public static void entry(TriggerParams triggerParams) {

		List<Attendance__c> triggerNew = (List<Attendance__c>) triggerParams.triggerNew;
		List<Attendance__c> triggerOld = (List<Attendance__c>) triggerParams.triggerOld;
		Map<Id, Attendance__c> oldMap = (Map<Id, Attendance__c>) triggerParams.oldMap;
		Map<Id, Attendance__c> newMap = (Map<Id, Attendance__c>) triggerParams.newMap;

		if (triggerParams.isBefore) {
			if (triggerParams.isInsert) {

			}
		}
		if (triggerParams.isAfter) {
			if (triggerParams.isInsert) {

			}
			if (triggerParams.isUpdate) {
				updateDateLastAttendaneOnContactObject(triggerNew);
			}
		}

	}

	public static void updateDateLastAttendaneOnContactObject(List<Attendance__c> triggerNew) {
		// id Class Roster To search 
		List<id> classRosterIds = new List<id> ();
		// che if attendance record has status Present, created formula field and check if this true    
		for (Attendance__c a : triggerNew) {
			if (a.Present__c == true) {
				classRosterIds.add(a.Class_Roster__c);
			}
		}
		//select Class Roster List
		List<Class_Roster__c> clrList = [SELECT id, Class__c, Contact__c FROM Class_Roster__c WHERE Id IN :classRosterIds];
		//create Map and Save Date by Class Roster ID
		Map<Id, Date> mapDateByIdClassRoster = new Map<Id, Date> ();
		// Select Max Date on Attendance object where status present
		List<AggregateResult> aggregationResultList = [SELECT MAX(Date__c), Class_Roster__c FROM Attendance__c WHERE Class_Roster__c IN :classRosterIds AND Present__c = true Group by Class_Roster__c];
		// add to map date by class roster id
		for (AggregateResult a : aggregationResultList) {
			mapDateByIdClassRoster.put((ID) a.get('Class_Roster__c'), (Date) a.get('expr0'));
		}
		// create map To hold class roster id by contact id        
		Map<id, Id> classRosterIDByContactID = new Map<id, id> ();
		// list ID for search
		List<ID> contatListIDs = new List<ID> ();
		// put on map class roster id by contact 
		if (!clrList.isEmpty()) {
			for (Class_Roster__c crl : clrList) {
				classRosterIDByContactID.put(crl.Contact__c, crl.id);
				contatListIDs.add(crl.Contact__c);
			}
		}
		//select contact list to update
		List<Contact> contactListToUpdateDate = [SELECT id, AccountId, Last_Attendance_Date__c FROM Contact WHERE ID IN :contatListIDs];
		//create account ids
		List<id> accountIds = new List<Id> ();
		// create myp Account id by contact to get class roster ID and finaly get last max Date
		Map<Id, id> mapContactIdByAccount = new Map<id, id> ();
		// save max date on contact object
		if (!contactListToUpdateDate.IsEmpty()) {
			for (Contact c : contactListToUpdateDate) {
				id crlId = classRosterIDByContactID.get(c.id);
				c.Last_Attendance_Date__c = mapDateByIdClassRoster.get(crlId);
				mapContactIdByAccount.put(c.AccountId, c.Id);
				accountIds.add(c.AccountId);
			}
		}
		//update current contact list
		if (!contactListToUpdateDate.isEmpty()) {
			update contactListToUpdateDate;
		}
		//select releted account and update
		List<Account> accountListToUpdate = [SELECT id, Last_Attendance_Date__c FROM Account WHERE id IN :accountIds];
		if (!accountListToUpdate.isEmpty()) {
			for (Account a : accountListToUpdate) {
				Id conID = mapContactIdByAccount.get(a.id);
				Id clrID = classRosterIDByContactID.get(conID);
				a.Last_Attendance_Date__c = mapDateByIdClassRoster.get(clrID);
			}
			// update account lass Attendance date       
			if (!accountListToUpdate.isEmpty()) {
				update accountListToUpdate;
			}
		}
	}

}