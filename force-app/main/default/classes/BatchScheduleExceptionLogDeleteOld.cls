global class BatchScheduleExceptionLogDeleteOld implements Schedulable {

	global void execute(SchedulableContext sc) {
		proccessSchedule();
	}

	private void proccessSchedule() {
		BatchExceptionLogDeleteOld batchExceptionLog = new BatchExceptionLogDeleteOld();

		String query = 'SELECT Id, Name FROM Exception_Log__c WHERE CreatedDate < LAST_N_MONTHS:2';
		List<Exception_Log__c> ExceptionLogList = Database.query(query);
		
		//Check if exist any records
		if (!ExceptionLogList.isEmpty()) {
			//Check if there are less than 5 batches active, otherwise wait for 5 minutes and try again
			Integer numberOfBatch = [SELECT count() FROM AsyncApexJob WHERE JobType = 'BatchApex' AND Status IN('Processing', 'Preparing', 'Queued')];
			if (numberOfBatch < 5) {
				Database.executeBatch(batchExceptionLog, 200);
			} 
			if (numberOfBatch >= 5 || Test.isRunningTest()) {				
				BatchScheduleExceptionLogDeleteOld delayBachExceptionLog = new BatchScheduleExceptionLogDeleteOld();
				Datetime dt = Datetime.now().addMinutes(5);
				String timeForScheduler = dt.format('s m H d M \'?\' yyyy');
				Id schedId = System.Schedule('Delete Old Exception Log Records Batch Schedule' + timeForScheduler, timeForScheduler, delayBachExceptionLog );
			}
			
		}
	}

	/* At 23:00:00pm, on the last day of the month, every month
		BatchScheduleExceptionLogDeleteOld updater = new BatchScheduleExceptionLogDeleteOld();
		String sch = '0 0 23 L * ? *';
		system.schedule('Delete Old Exception Logs', sch, updater);
	*/
}