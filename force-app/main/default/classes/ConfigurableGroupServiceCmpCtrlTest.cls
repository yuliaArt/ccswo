@isTest
public class ConfigurableGroupServiceCmpCtrlTest  {
    
    public static Service__c service1;
    public static ContactWrapper contactWrap1;
    public static ContactWrapper contactWrap2;
    public static ServiceWrapper serviceWrap1;
    public static ServiceWrapper serviceWrap2;
    
    @isTest
    public static void testInsertServices() {
        Trigger_Settings__c sett = new Trigger_Settings__c();
        sett.Disable_ServiceTrigger__c = false;
        insert sett;
        
        //Creating Group
        Group__c groupRecord = new Group__c();
        groupRecord.Name='Group Test';
        insert groupRecord;
        
        service1 = new Service__c();
        //Creating Contacts
        Contact con1 = new Contact(FirstName = 'Thom', LastName = 'Behrens');
        Contact con2 = new Contact(FirstName = 'Joshua', LastName = 'Dempsey');
        
        //insert Contact records 
        insert new List<Contact>{ con1, con2 };            
            System.assert(con1!=null &&groupRecord!=null);
        System.assert(con2!=null && groupRecord!=null); 
        Group_Member__c groupAssRecord2=new Group_Member__c();
        groupAssRecord2.Contact__c=con2.id;
        groupAssRecord2.Group__c    = groupRecord.Id;
        groupAssRecord2.Start_Date__c = Date.today();
        groupAssRecord2.End_Date__c = Date.today().addDays(10);
        insert groupAssRecord2;
        List<Program_Enrollment__c> peList= new List<Program_Enrollment__c>();
        //Creating ProgramEnrollment Records
        //Id aceRT = Schema.SObjectType.Program_Enrollment__c.getRecordTypeInfosByDeveloperName().get('ACE').getRecordTypeId();
        //Program_Enrollment__c pe1 = new Program_Enrollment__c(Contact__c = con1.Id,  Program__c='ACE', RecordTypeId = aceRT);
        //Program_Enrollment__c pe2 = new Program_Enrollment__c(Contact__c = con2.Id, Program__c='ACE', RecordTypeId = aceRT);
        Program_Enrollment__c pe1 = new Program_Enrollment__c(Contact__c = con1.Id,  Program__c='nnn');
        Program_Enrollment__c pe2 = new Program_Enrollment__c(Contact__c = con2.Id, Program__c='nnn');
        
        peList.add(pe1);
        peList.add(pe2);
        insert peList;
        
        //Creating Service Record
        service1.Date_of_Service__c = date.today().addDays(10);
        service1.Program_Enrollment__c=pe1.id;
        insert service1;
        
        contactWrap1 = new ContactWrapper(TRUE,con1.Name, pe2.Program__c, con1.Id, peList,0,pe1.id );
        contactWrap2 = new ContactWrapper(TRUE,con1.Name, pe2.Program__c, con1.Id, peList,0,pe1.id );
        contactWrap1.isSelected = true;
        
        serviceWrap1 = new ServiceWrapper();
        serviceWrap1.Counselor='Test Counseler';
        
        serviceWrap2= new ServiceWrapper();
        serviceWrap2.Counselor='';
        
        
        List<ContactWrapper> contactWrappers = new List<ContactWrapper>{contactWrap1, contactWrap2};
        Map<String, String> eventsMap = new Map<String, String>();
        eventsMap.put('Date_of_Service__c', String.valueof(Date.today()));
        eventsMap.put('Service_Type__c', 'Values will Vary');
        String eventsJSON = JSON.serialize(eventsMap);
        
        List<ServiceWrapper> serviceWrappers = new List<ServiceWrapper>{serviceWrap1};
        String contactWrappersJSON = JSON.serialize(contactWrappers);
        
        String serviceWrapperJSON = JSON.serialize(serviceWrap1);
        String serviceWrapperJSON2= JSON.serialize(serviceWrap2);
        
        //Id impactRT = Schema.SObjectType.Service__c.getRecordTypeInfosByDeveloperName().get('Impact').getRecordTypeId();
        
        test.startTest();
        ConfigurableGroupServiceComponentCtrl controller = new ConfigurableGroupServiceComponentCtrl();
        ConfigurableGroupServiceComponentCtrl.getProgramEnrollments(pe1.Program__c, null);
        ConfigurableGroupServiceComponentCtrl.getProgramEnrollments(pe1.Program__c, groupRecord.Id);
        ConfigurableGroupServiceComponentCtrl.newServiceWrapper();
        //ConfigurableGroupServiceComponentCtrl.insertRecords(contactWrappersJSON, eventsJSON, groupRecord.Id, impactRT);
        ConfigurableGroupServiceComponentCtrl.insertRecords(contactWrappersJSON, eventsJSON, groupRecord.Id, groupRecord.Id);
        ConfigurableGroupServiceComponentCtrl.fetchUser();
        test.stopTest();
        
    }
    
    @isTest
    public static void convertStrToListTest() {
        
        String fieldsApi = 'Duration__c, Service_Type__c, Topics__c';
        List<String> fieldsList = ConfigurableGroupServiceComponentCtrl.convertStrToList(fieldsApi);
        System.assertEquals(fieldsList.size(), 3);
        
        
    }
}