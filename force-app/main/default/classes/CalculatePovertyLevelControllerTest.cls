@IsTest
public class CalculatePovertyLevelControllerTest  {
	
	@testSetup static void initialize() {
        TestFactory.calculatePovertyLevelControllerTestData();
    }
    
    @isTest
    static void povertyLevelCalculationTest() {
		Account testAccount = [SELECT Id FROM Account LIMIT 1 ];
        Test.startTest();
        CalculatePovertyLevelController.povertyLevelCalculation(testAccount.Id);
        Test.stopTest();
    }

	@isTest
    static void povertyLevelCalculationNegativeWithoutPovertyLevelTest() {
		Account testAccount = [SELECT Id, npsp__Number_of_Household_Members__c FROM Account LIMIT 1 ];
		
		Poverty_Level__c testPovertyLevel = [SELECT Id, Year__c FROM Poverty_Level__c LIMIT 1];
		testPovertyLevel.Year__c = '1000';
		update testPovertyLevel;
        
		Test.startTest();
        CalculatePovertyLevelController.povertyLevelCalculation(testAccount.Id);
        Test.stopTest();
    }

	@isTest
    static void povertyLevelCalculationNegativeWithoutAccountTest() {
		Account testAccount = [SELECT Id, npsp__Number_of_Household_Members__c FROM Account LIMIT 1 ];
		testAccount.npsp__Number_of_Household_Members__c = 0;
		update testAccount;
        
		Test.startTest();
        CalculatePovertyLevelController.povertyLevelCalculation(testAccount.Id);
        Test.stopTest();
    }
}