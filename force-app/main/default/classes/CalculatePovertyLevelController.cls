public class CalculatePovertyLevelController  {

	@AuraEnabled
	public static String povertyLevelCalculation(Id recordId) {
		String errorMessage = 'SUCCESS';
		String currentYear = String.valueOf(Date.today().year());
		List<Account> accounts = [SELECT id, Name, Total_Household_Income__c, Self_Reported_Income__c, Self_Reported_Number_in_HH__c, Self_Reported_FPL__c, Self_Reported_AMI__c, Detailed_FPL__c, Detailed_AMI__c, npsp__Number_of_Household_Members__c FROM Account WHERE Total_Household_Income__c != 0 AND npsp__Number_of_Household_Members__c != 0 AND Id =: recordId];

		if (!accounts.isEmpty()) {
			List<Poverty_Level__c> povertyLevel = [Select id, Name, Base_Amount__c, Incremental_Amount__c, Year__c FROM Poverty_Level__c WHERE Year__c =: currentYear AND Base_Amount__c != 0 AND Incremental_Amount__c != 0 ORDER BY CreatedDate DESC LIMIT 1];
			
			if (!povertyLevel.isEmpty()) {
				Account accountIncome = new Account();
				accountIncome.Id = accounts.get(0).Id;
				
				accountIncome.Detailed_FPL__c = (accounts.get(0).Total_Household_Income__c * 100) / (povertyLevel.get(0).Base_Amount__c + ((accounts.get(0).npsp__Number_of_Household_Members__c - 1) * povertyLevel.get(0).Incremental_Amount__c));

				accountIncome.Self_Reported_FPL__c = (accounts.get(0).Self_Reported_Income__c * 100) / (povertyLevel.get(0).Base_Amount__c + ((accounts.get(0).npsp__Number_of_Household_Members__c - 1) * povertyLevel.get(0).Incremental_Amount__c));

				List<AMI_Level__c> amiLevels = [SELECT Percentage__c, Amount__c FROM AMI_Level__c WHERE Number_In_Household__c = :accounts.get(0).npsp__Number_of_Household_Members__c AND Year__c = :povertyLevel.get(0).Year__c AND Amount__c <= :accounts.get(0).Self_Reported_Income__c];

				for (AMI_Level__c amiLevel : amiLevels) {
					if (amiLevel.Amount__c <= accounts.get(0).Self_Reported_Income__c) {
						accountIncome.Self_Reported_AMI__c = amiLevel.Percentage__c;
					}
					if (amiLevel.Amount__c <= accounts.get(0).Self_Reported_Income__c) {
						accountIncome.Detailed_AMI__c = amiLevel.Percentage__c;
					}
				}

				update accountIncome;
			} else {
				errorMessage = 'Error! The Base and Increment for year ' + Date.today().year() + ' are not found.';
				System.debug(errorMessage);
			}
		} else {
			errorMessage = 'Error! Total Household Income and Number of Household Members cannot be 0.';
			System.debug(errorMessage);
		}
		return errorMessage;
	}

}