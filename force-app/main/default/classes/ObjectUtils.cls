//**
// Author: Senyuk Mykhailo
// Date: Feburary 1, 2019
// Description: .
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//** 
public class ObjectUtils {

	public static Id getRecordTypeId(String nameOfSObject, String recordTypeName) {
		return Schema.getGlobalDescribe().get(nameOfSObject).getDescribe().getRecordTypeInfosByDeveloperName().get(recordTypeName).getRecordTypeId();
	}

	public static Id getRecordTypeId(Map<String, Schema.RecordTypeInfo> allRecordTypesFromObject, String recordTypeName) {
		return allRecordTypesFromObject.get(recordTypeName).getRecordTypeId();
	}

	public static Map<String, Schema.SObjectField> getAllObjectFields(String nameOfSObject) {
		Map<String, Schema.SObjectField> sObjectFields = Schema.getGlobalDescribe().get(nameOfSObject).getDescribe().fields.getMap();
		return sObjectFields;
	}

	public static String getAllFields(String nameOfSObject) {
		Map<String, Schema.SObjectField> sObjectFields = Schema.getGlobalDescribe().get(nameOfSObject).getDescribe().fields.getMap();
		List<String> fields = new List<String> ();

		for (String field : sObjectFields.keySet()) {
			if (sObjectFields.get(field).getDescribe().isAccessible()) {
				fields.add(field);
			}
		}
		String query = 'SELECT ' + String.join(fields, ', ');
		return query;
	}

	public static String getAllFieldsFromObject(String nameOfSObject) {

		Map<String, Schema.SObjectField> sObjectFields = Schema.getGlobalDescribe().get(nameOfSObject).getDescribe().fields.getMap();
		List<String> fields = new List<String> ();

		for (String field : sObjectFields.keySet()) {
			if (sObjectFields.get(field).getDescribe().isAccessible()) {
				fields.add(field);
			}
		}
		String query = 'SELECT ' + String.join(fields, ', ') + ' FROM ' + nameOfSObject;
		return query;
	}

	public static Map<String, String> getAllFieldsLabelFromObject(String nameOfSObject) {
		Map<String, Schema.SObjectField> sObjectFields = Schema.getGlobalDescribe().get(nameOfSObject).getDescribe().fields.getMap();
		Map<String, String> labelsByFieldApiName = new Map<String, String> ();

		for (String field : sObjectFields.keySet()) {
			if (sObjectFields.get(field).getDescribe().isAccessible()) {
				labelsByFieldApiName.put(field, sObjectFields.get(field).getDescribe().getLabel());
			}
		}
		return labelsByFieldApiName;
	}

	public static String getFieldLabelFromObjectByFieldName(String nameOfSObject, String fieldName) {
		return Schema.getGlobalDescribe().get(nameOfSObject).getDescribe().fields.getMap().get(fieldName).getDescribe().getLabel();
	}

	public static Boolean HasSObjectField(String nameOfSObject, String fieldName) {
		return Schema.getGlobalDescribe().get(nameOfSObject).getDescribe().fields.getMap().keySet().contains(fieldName.toLowerCase());
	}

	public static Map<String, Schema.RecordTypeInfo> getAllRecordTypesFromObject(String nameOfSObject) {
		Map<String, Schema.RecordTypeInfo> allRecordTypesFromObject = Schema.getGlobalDescribe().get(nameOfSObject).getDescribe().getRecordTypeInfosByDeveloperName();
		return allRecordTypesFromObject;
	}

	public static Boolean hasSObjectRecordType(String nameOfSObject, String recordTypeName) {
		Map<String, Schema.RecordTypeInfo> recordTypesByRecordTypeDevName = getAllRecordTypesFromObject(nameOfSObject);
		return recordTypesByRecordTypeDevName.containsKey(recordTypeName);
	}

	public static Boolean hasSObjectRecordType(Map<String, Schema.RecordTypeInfo> allRecordTypesFromObject, String recordTypeName) {
		return allRecordTypesFromObject.containsKey(recordTypeName);
	}

	public static Map<String, String> getReletedObjects(Id objectId) {
		String ObjectName = objectId.getSObjectType().getDescribe().getName();
		Map<string, string> relatedObjectsMap = new map<string, string> ();
		list<Schema.Childrelationship> relatedObjectsList = Schema.getGlobalDescribe().get(ObjectName).getdescribe().getChildRelationships();

		for (Schema.Childrelationship relatedObject : relatedObjectsList) {
			if (relatedObject.getChildSObject().getDescribe().isUpdateable()
			    && !relatedObject.getChildSObject().getDescribe().isCustomSetting()
			    && relatedObject.getChildSObject().getDescribe().isCreateable()) {
				relatedObjectsMap.put(relatedObject.getChildSObject().getDescribe().getName(), relatedObject.getChildSObject().getDescribe().getLabel());
			}
		}
		return relatedObjectsMap;
	}

	public static List<Id> getUsersIdFromPublicGroup(String groupName) {

		List<Id> userIds = new List<Id> ();
		String userType = Schema.SObjectType.User.getKeyPrefix();

		List<GroupMember> groupMembers = [SELECT UserOrGroupId FROM GroupMember WHERE group.DeveloperName = :groupName]; //'Exception_Receivers'

		for (GroupMember groupMember : groupMembers) {
			if (((String) groupMember.UserOrGroupId).startsWith(userType)) {
				userIds.add(groupMember.UserOrGroupId);
			}
		}
		List<User> usersFromPublicGroup = [SELECT Id, Email FROM User WHERE Id in :userIds];

		List<Id> listOfUsersId = new List<Id> ();
		for (User user : usersFromPublicGroup) {
			listOfUsersId.add(user.id);
		}
		return listOfUsersId;
	}


	public static Messaging.SendEmailResult[] sendEmails(String emailHtmlBody, List<Id> usersToAddress, String emailSebject, String emailSenderName, Messaging.EmailFileAttachment csvAttachment) {

		List<Messaging.SingleEmailMessage> allEmails = new List<Messaging.SingleEmailMessage> ();

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setHtmlBody(emailHtmlBody);
		if(!usersToAddress.isEmpty()){
			mail.setTargetObjectId(usersToAddress.get(0));
		}
		mail.setSenderDisplayName(emailSenderName);
		mail.setSubject(emailSebject);
		mail.setToAddresses(usersToAddress);
		mail.SaveAsActivity = false;
		mail.setFileAttachments(new Messaging.EmailFileAttachment[] { csvAttachment });

		allEmails.add(mail);

		Messaging.SendEmailResult[] emailResults = Messaging.sendEmail(allEmails);

		return emailResults;
	}

	public static Messaging.EmailFileAttachment setAttachment(String attachmentBody, String attachmentName) {
		Messaging.EmailFileAttachment csvAttachment = new Messaging.EmailFileAttachment();
		Blob csvBlob = Blob.valueOf(attachmentBody);
		csvAttachment.setFileName(attachmentName);
		csvAttachment.setBody(csvBlob);

		return csvAttachment;
	}

	public static List<SObject> randomize(List<SObject> lst) {
		Integer currentIndex = lst.size();
		SObject temporaryValue;
		Integer randomIndex;
		// While there remain elements to shuffle...
		while (0 != currentIndex) {
			// Pick a remaining element...
			randomIndex = integer.valueOf(Math.floor(Math.random() * currentIndex));
			currentIndex -= 1;
			// And swap it with the current element.
			temporaryValue = lst[currentIndex];
			lst[currentIndex] = lst[randomIndex];
			lst[randomIndex] = temporaryValue;
		}
		return lst;
	}
}