/* Provisio Partners
** Author: Dnyaneshwar Waghmare
** Date: 24/8/2018
** Description: Deep Clone functionality Test Class
**/
@isTest 
public class CloneRecordWithRelatedRecordsCntrTest {
    
    static testmethod void validateExtensionController(){
  		
        //defining lists
        list<Contact> ContactList= new list<Contact>();
        list<Program_Enrollment__c> ProgramEnrollmentList= new list<Program_Enrollment__c>();
        list<Service_Plan__c> careplanList= new list<Service_Plan__c>();
        list<Goal__c> GoalList= new list<Goal__c>();
        list<Action_Step__c> ActionStepList= new list<Action_Step__c>();


        //Trigger_Settings__c sett = new Trigger_Settings__c();
        //sett.Disable_ProgramEnrollmentTrigger__c = false;
        //sett.Disable_ContactTrigger__c = false;
        //insert sett;
        
        //Insert Contact Records
        Contact ContactObj= new Contact();
        ContactObj.LastName= 'Test Yrc LastName';
        //ContactObj.Gender__c = 'Male';
        ContactList.add(ContactObj);
        insert ContactList;
        system.assertEquals('Test Yrc LastName', ContactObj.LastName);

        
        //Insert Program_Enrollment Records
        Program_Enrollment__c ProgramEnrollmentObj= new Program_Enrollment__c();
        ProgramEnrollmentObj.Contact__c= ContactObj.Id;
        ProgramEnrollmentList.add(ProgramEnrollmentObj);
        insert ProgramEnrollmentList;
        system.assertEquals(ContactObj.Id, ProgramEnrollmentObj.Contact__c);
        
        //Insert Care_Plan Records
		Service_Plan__c careplanObj= new Service_Plan__c();
        careplanObj.Program_Enrollment__c=ProgramEnrollmentObj.id;
        careplanList.add(careplanObj);
        insert careplanList;
        system.assertEquals(ProgramEnrollmentObj.id, careplanObj.Program_Enrollment__c);
        
        //Insert Goal Records
		Goal__c GoalObj= new Goal__c();
        GoalObj.Name= 'Yrc Test Goal';
        GoalObj.Service_Plan__c= careplanObj.Id;
        GoalList.add(GoalObj);
        insert GoalList;
        system.assertEquals(careplanObj.Id, GoalObj.Service_Plan__c);

        
        //Insert Action_Step Records
		Action_Step__c ActionStepObj= new Action_Step__c();
        ActionStepObj.Name= 'Yrc Test ActionStep';
        ActionStepObj.Goal__c= GoalObj.id;
        ActionStepObj.Service_Plan__c = careplanObj.Id;
        ActionStepList.add(ActionStepObj);
        insert ActionStepList;
        system.assertEquals(GoalObj.id, ActionStepObj.Goal__c);
        
        //Test.setCurrentPage(Page.CloneButtonVF);
        //ApexPages.currentPage().getParameters().put('Program_Enrollment__c',ProgramEnrollmentObj.Id);
        //ApexPages.StandardController stdLead = new ApexPages.StandardController(careplanObj);
        //CloneButtonVFController CloneButtonVFControllerobj  = new CloneButtonVFController(stdLead);
        //CloneButtonVFControllerobj.cloneCarePlan();
    }    
}