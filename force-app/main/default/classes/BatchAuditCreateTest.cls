@isTest
public class BatchAuditCreateTest {

	@testSetup static void initialize() {
		TestFactory.createDataForBatchAudit();
	}

	@isTest
	static void startBatchAuditCreateTest() {
		List<AuditSetting__mdt> auditSettings = [SELECT Id, ObjectName__r.QualifiedAPIName, FieldName__r.QualifiedAPIName, FieldValue__c, PercentToShow__c, IsVisible__c, FieldForSelectQuarter__r.QualifiedAPIName FROM AuditSetting__mdt WHERE IsVisible__c = TRUE];

		
		SObject sob = Schema.getGlobalDescribe().get(auditSettings.get(0).ObjectName__r.QualifiedAPIName).newSObject();

		List<String> values = new List<String> ();
		for (AuditSetting__mdt auditSetting : auditSettings) {
			values.add('(' + auditSetting.FieldName__r.QualifiedAPIName + ' = \'' + auditSetting.FieldValue__c + '\' AND ' + auditSetting.FieldForSelectQuarter__r.QualifiedAPIName + ' = THIS_QUARTER)');
		}

		String query = ObjectUtils.getAllFieldsFromObject(auditSettings.get(0).ObjectName__r.QualifiedAPIName) + ' WHERE ' + String.join(values, ' OR ');

		Test.startTest();
		BatchAuditCreate obj = new BatchAuditCreate(query);
		DataBase.executeBatch(obj);
		Test.stopTest();
	}

}