@isTest
public class BatchAttendanceCreateTest {
    
    @testSetup static void initialize() {
        TestFactory.createDataForBatchAttendance();
    }
    
    @isTest
    static void startBatchAttandanceCreateTest() {
        Test.startTest();
        BatchAttendanceCreate obj = new BatchAttendanceCreate();
        DataBase.executeBatch(obj); 
        Test.stopTest();
    }

}