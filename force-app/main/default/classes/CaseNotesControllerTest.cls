/* Provisio Partners
** Author: Dnyaneshwar Waghmare
** Date: 08/20/2018
** Description: Test class for CaseNotesController.     
**/
@IsTest
public class CaseNotesControllerTest {
	
	@testSetup static void initialize() {
		Trigger_Settings__c disableTrigger = (Trigger_Settings__c) TestFactory.createSObject(new Trigger_Settings__c(),'TestFactory.Trigger_SettingsDefaults', true);
	}

    public static  testmethod void casenotesMethod(){
        
        //insert test contact record
        Contact conObj= new Contact();
        conObj.FirstName='YRC Test FirstName';
        conObj.LastName='YRC Test LastName';
        conObj.Race__c='Asian' ;
        insert conObj;
       
        //insert class records.
        Class__c classobj  = new Class__c();
        classobj.Auto_Create_Attendance__c= FALSE;
        insert classobj;
        
        //create program enrollment object.
        Program_Enrollment__c programobj= new Program_Enrollment__c();
        programobj.Contact__c=conobj.id;
        insert programobj;
        
        //insert test service record
        List<Service__c> serviceList = new List<Service__c>();
        Service__c ServiceObj= new Service__c();
        ServiceObj.Date_of_Service__c = date.today();
        ServiceObj.Program_Enrollment__c=programobj.id;
        ServiceObj.Duration__c = 4.00;
        ServiceObj.Service_Provided__c='Safety';
        serviceList.add(ServiceObj);
        Database.insert(serviceList);
          
        //start test method
        Test.startTest();
        //calling class and method
        ApexPages.StandardController sctrl = new ApexPages.StandardController(programobj);
        CaseNotesController caseNotesObj= new CaseNotesController(sctrl);
        caseNotesObj.getSNotes();
        caseNotesObj.updateSNotesPDF();
        caseNotesObj.updateSNotes();
        Test.stopTest();
        
    }
    
    //method 2
    public static  testmethod void casenotesMethod1(){
        
        //insert test contact record
        Contact conObj= new Contact();
        conObj.FirstName='YRC Test FirstName';
        conObj.LastName='YRC Test LastName'; 
        conObj.Race__c='Asian' ;
        insert conObj;
        
        Class__c classobj  = new Class__c();
        classobj.Auto_Create_Attendance__c= FALSE;
        insert classobj;
        
        //create program enrollment object.
        Program_Enrollment__c programobj= new Program_Enrollment__c();
        programobj.Contact__c=conobj.id;
        insert programobj;
        
        //insert test service record
        Service__c ServiceObj= new Service__c();
        ServiceObj.Date_of_Service__c=Date.today().adddays(2);
        ServiceObj.Program_Enrollment__c=programobj.id;
        ServiceObj.Duration__c = 4.00;
        ServiceObj.Service_Provided__c='Safety';
        Insert ServiceObj;
        system.assert(ServiceObj.Id != Null);        
        System.debug('Value in the service of date ' + ServiceObj.Date_of_Service__c);
        
        id contactid;
        system.assertEquals(programobj.id, ServiceObj.Program_Enrollment__c);
        
        //start test method
        Test.startTest();
        
        PageReference myVfPage = Page.CaseNote;
        Test.setCurrentPageReference(myVfPage); 
        ApexPages.currentPage().getParameters().put('id',programobj.Id);
        String id = ApexPages.currentPage().getParameters().get('id');
        ApexPages.currentPage().getParameters().put('date1','09/01/2018');
        ApexPages.currentPage().getParameters().put('date2','09/10/2018');
        //calling class and method
        ApexPages.StandardController sctrl = new ApexPages.StandardController(programobj);
        CaseNotesController caseNotesObj= new CaseNotesController();
        caseNotesObj.getSNotes();
        caseNotesObj.getServiceRTvise();
        caseNotesObj.updateSNotesPDF();
        caseNotesObj.updateSNotes();
       
        Test.stopTest();  
    }

    //method 3
    public static  testmethod void casenotesMethod3(){

        
        //insert test contact record
        Contact conObj= new Contact();
        conObj.FirstName='YRC Test FirstName';
        conObj.LastName='YRC Test LastName';
        conObj.Race__c='Asian' ;
        insert conObj;
        
        Class__c classobj  = new Class__c();
        classobj.Auto_Create_Attendance__c= FALSE;
        insert classobj;
        
        //create program enrollment object.
        Program_Enrollment__c programobj= new Program_Enrollment__c();
        programobj.Contact__c=conobj.id;
        insert programobj;
        
        //insert test service record
        Service__c ServiceObj= new Service__c();
        ServiceObj.Date_of_Service__c=Date.today().adddays(2);
        ServiceObj.Program_Enrollment__c=programobj.id;
        ServiceObj.Duration__c = 4.00;
        ServiceObj.Service_Provided__c='Safety';
        Insert ServiceObj;
        system.assert(ServiceObj.Id != Null);        

        id contactid;
        system.assertEquals(programobj.id, ServiceObj.Program_Enrollment__c);
        
        //start test method
        Test.startTest();
        
        PageReference myVfPage = Page.CaseNote;
        Test.setCurrentPageReference(myVfPage); 
        ApexPages.currentPage().getParameters().put('id',programobj.Id);
        String id = ApexPages.currentPage().getParameters().get('id');
        ApexPages.currentPage().getParameters().put('date1','');
        ApexPages.currentPage().getParameters().put('date2','');
        ApexPages.StandardController sctrl = new ApexPages.StandardController(programobj);
        CaseNotesController caseNotesObj= new CaseNotesController();
        caseNotesObj.getSNotes();
        caseNotesObj.getServiceRTvise();
        caseNotesObj.updateSNotes();
        caseNotesObj.updateSNotesPDF();
        Test.stopTest();  
    }
    
    
    //method 4
    public static  testmethod void casenotesMethod4(){
        
        //insert test contact record
        Contact conObj= new Contact();
        conObj.FirstName='YRC Test FirstName';
        conObj.LastName='YRC Test LastName';
        conObj.Race__c='Asian' ;
        insert conObj;
        
        Class__c classobj  = new Class__c();
        classobj.Auto_Create_Attendance__c= FALSE;
        insert classobj;
        
        //create program enrollment object.
        Program_Enrollment__c programobj= new Program_Enrollment__c();
        programobj.Contact__c=conobj.id;
        insert programobj;
        
        //insert test service record
        Service__c ServiceObj= new Service__c();
        ServiceObj.Date_of_Service__c=Date.today().adddays(2);
        ServiceObj.Program_Enrollment__c=programobj.id;
        ServiceObj.Duration__c = 4.00;
        ServiceObj.Service_Provided__c='Safety';
        Insert ServiceObj;
        system.assert(ServiceObj.Id != Null);        
        
        id contactid;
        system.assertEquals(programobj.id, ServiceObj.Program_Enrollment__c);
        
        //start test method
        Test.startTest();
        
        PageReference myVfPage = Page.CaseNote;
        Test.setCurrentPageReference(myVfPage); 
        ApexPages.currentPage().getParameters().put('id',programobj.Id);
        String id = ApexPages.currentPage().getParameters().get('id');
        ApexPages.currentPage().getParameters().put('date1','');
        ApexPages.currentPage().getParameters().put('date2','');
        
        //calling class and method
        ApexPages.StandardController sctrl = new ApexPages.StandardController(programobj);
        CaseNotesController caseNotesObj= new CaseNotesController();
        caseNotesObj.getSNotes();
        caseNotesObj.getServiceRTvise();
        caseNotesObj.updateSNotes();
        caseNotesObj.updateSNotesPDF();
        Test.stopTest();  
    }

    //method 5
    public static  testmethod void casenotesMethod5(){
        
        //insert test contact record
        Contact conObj= new Contact();
        conObj.FirstName='YRC Test FirstName';
        conObj.LastName='YRC Test LastName';
        conObj.Race__c='Asian' ;
        insert conObj;
        
        Class__c classobj  = new Class__c();
        classobj.Auto_Create_Attendance__c= FALSE;
        insert classobj;
        
        //create program enrollment object.
        Program_Enrollment__c programobj= new Program_Enrollment__c();
        programobj.Contact__c=conobj.id;
        insert programobj;
        
        //start test method
        Test.startTest();
        
        PageReference myVfPage = Page.CaseNote;
        Test.setCurrentPageReference(myVfPage); 
        ApexPages.currentPage().getParameters().put('id',programobj.Id);
        String id = ApexPages.currentPage().getParameters().get('id');
        ApexPages.currentPage().getParameters().put('date1','');
        ApexPages.currentPage().getParameters().put('date2','');
        //calling class and method
        ApexPages.StandardController sctrl = new ApexPages.StandardController(programobj);
        CaseNotesController caseNotesObj= new CaseNotesController();
        caseNotesObj.getSNotes();
        caseNotesObj.getServiceRTvise();
        caseNotesObj.updateSNotes();
        caseNotesObj.updateSNotesPDF();
        Test.stopTest();  
    }

}