public class CaseNotePreviewController {

	//public static List<PrintPageFieldSettingWrapper> printPageFieldSettingsWrapper; 
	//public static List<PrintPageFieldSettings__mdt> cleanPrintPageFieldSettings;
	public static String currentObjectName;

	public static List<Schema.SObjectType> getReferenceObjectByFieldName(String nameOfSObject, String fieldName) {
		return Schema.getGlobalDescribe().get(nameOfSObject).getDescribe().fields.getMap().get(fieldName).getDescribe().getReferenceTo();
	}

	@AuraEnabled
	public static StartDataWrapper getStartData(Id recordId, Boolean filter, Datetime startDate, Datetime endDate ) {

		String objectName = recordId.getSObjectType().getDescribe().getName();
		currentObjectName = objectName;

		List<PrintPageFieldSettings__mdt> printPageFieldSettings = printPageFieldSettings(objectName);
		String mainObjectName = objectName;
		String recordName = objectName;
		String parentObjectName = objectName;

		if (!printPageFieldSettings.isEmpty()) {
			if (!printPageFieldSettings.get(0).PrintPageSetting__r.UseCurrentObject__c) {
				mainObjectName = printPageFieldSettings.get(0).PrintPageSetting__r.ObjectApiName__c;
			}
			if (printPageFieldSettings.get(0).PrintPageSetting__r.RecordApiName__c != null && printPageFieldSettings.get(0).PrintPageSetting__r.RecordApiName__c != '') {
				recordName = printPageFieldSettings.get(0).PrintPageSetting__r.RecordApiName__c;
			}
		}
		

		List<RecordType> recordTypesList = new List<RecordType> ();
		Map<String, Schema.RecordTypeInfo> allRecordTypesFromObject = ObjectUtils.getAllRecordTypesFromObject('Service__c'); //Service__c

		for (Schema.RecordTypeInfo recordType : allRecordTypesFromObject.values()) {
			if (!recordType.isMaster() && recordType.isActive() && recordType.isAvailable()) {
				recordTypesList.add(new RecordType(Id = recordType.getRecordTypeId(), Name = recordType.getName()));
			}
		}
		StartDataWrapper startDataWrapper = new StartDataWrapper();

		if (!printPageFieldSettings.isEmpty()) {
			List<PrintPageFieldSettings__mdt> cleanPrintPageFieldSettings = getCleanPrintPageSettings(printPageFieldSettings, mainObjectName);

			List<RecordWrapper> recordsWrapper = getRecords(recordId, cleanPrintPageFieldSettings, mainObjectName, recordName, filter, startDate, endDate);

			startDataWrapper.mainObjectName = mainObjectName;
			startDataWrapper.recordName = recordName;
			startDataWrapper.currentObjectName = objectName;
			startDataWrapper.cleanPrintPageFieldSettings = cleanPrintPageFieldSettings;
			startDataWrapper.recordsWrapper = recordsWrapper;
			startDataWrapper.recordTypesList = recordTypesList;
		}
		
		return startDataWrapper;

	}
	public static Boolean isContactObject(Id id){

		String recordId = String.valueOf(id);
        String keyCode  = recordId.subString(0,3);

        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        for(Schema.SObjectType objectInstance : gd.values()){
            if(objectInstance.getDescribe().getKeyPrefix() == keyCode){
                if(objectInstance.getDescribe().getName() == 'Contact'){
					return true;
				}
			}
		}

		return false;
	}

	public static List<Service__c> getServicesRelatedToPrograms(Id recordId){

		List<Program_Enrollment__c> relatedPrograms = [SELECT Id, Name, Contact__c from Program_Enrollment__c
														where Contact__c  =:recordId];

		List<Service__c> services = [SELECT Id, Name, Date_of_Service__c, Service_Provided__c, 
									Duration__c, Program_Enrollment__c, Note__c,CreatedById, CreatedBy.Name FROM Service__c
									where Program_Enrollment__c  IN:relatedPrograms];

		List<Service__c> listOfServicesRelatedToContact = [SELECT Id, Name, Date_of_Service__c, 
														Service_Provided__c, Duration__c, Client__c, Note__c,
														CreatedById, CreatedBy.Name FROM Service__c WHERE Client__c =: recordId];

		List<Id> existingRelatedListOfServices = new List<Id>();													
		for(Service__c record: listOfServicesRelatedToContact){
			existingRelatedListOfServices.add(record.Id);
		}

		List<Service__c> filteredList = new List<Service__c>();	

		for(Service__c record: services){
			if(!existingRelatedListOfServices.contains(record.Id)){
				filteredList.add(record);
			}
		}											
		return filteredList;
	}

	@AuraEnabled
	public static StartDataWrapper getFilterData(Id recordId, Boolean filter, Datetime startDate, Datetime endDate ) {
		
		StartDataWrapper startDataWrapper = getStartData(recordId, true, startDate, endDate);

		return startDataWrapper;
	}


	public static List<RecordWrapper> getRecords(Id recordId, List<PrintPageFieldSettings__mdt> cleanPrintPageFieldSettings, String mainObjectName, String recordName, Boolean filter, Datetime startDate, Datetime endDate) {
		List<RecordWrapper> recordsWrapper = new List<RecordWrapper> ();

		Map<String, String> recordFields = new Map<String, String> ();
		for (PrintPageFieldSettings__mdt printField : cleanPrintPageFieldSettings) {
			if (printField.Field_API_Name__c != null && printField.Field_API_Name__c != '') {
				if (!recordFields.containsKey(printField.Field_API_Name__c)) {
					recordFields.put(printField.Field_API_Name__c, printField.Field_API_Name__c);
				}
			}
		}
		if (recordFields.isEmpty()) {
			recordFields.put('Id', 'Id');
		}
		Boolean hasRecordType = false;
		if (ObjectUtils.HasSObjectField(mainObjectName, 'RecordTypeId')) {
			recordFields.put('RecordTypeId', 'RecordTypeId');
			recordFields.put('RecordType.DeveloperName', 'RecordType.DeveloperName');
			hasRecordType = true;
		}
		String query = 'SELECT ' + String.join(recordFields.values(), ', ') + ' FROM ' + mainObjectName + ' WHERE ' + recordName + ' = \'' + recordId + '\'';

		List<Service__c> recordsListofRecords = Database.query(query);

		List<Service__c> servicesRelatedToProgramsFromContact = new List<Service__c>();

		List<SObject> recordsList =  new List<SObject>();
		
		if(isContactObject(recordId) == true){
			servicesRelatedToProgramsFromContact = getServicesRelatedToPrograms(recordId);
			recordsList.addAll(servicesRelatedToProgramsFromContact);
		}
		
		if(filter == true){
			recordsListofRecords.addAll(servicesRelatedToProgramsFromContact);
			recordsList = getFilteredRecords(recordsListofRecords, startDate, endDate);
		}else{
			recordsList.addAll(recordsListofRecords);
		}

		Integer recordCount = 0;
		for (SObject record : recordsList) {
			Integer nextRow = 0;
			Integer maxRow = 0;
			Integer row = 0;
			RecordWrapper recordWrapper = new RecordWrapper();
			recordWrapper.recordName = mainObjectName;

			Map<Integer, RowWrapper> recordsFieldWrapperByRowNumber = new Map<Integer, RowWrapper> ();
			for (PrintPageFieldSettings__mdt printPageField : cleanPrintPageFieldSettings) {
				if (printPageField.Record_Type_Developer_Name__c != null && printPageField.Record_Type_Developer_Name__c != '') {
					if (hasRecordType && record.getSObject('RecordType') != null && record.getSObject('RecordType').get('DeveloperName') == printPageField.Record_Type_Developer_Name__c) {
						recordsFieldWrapperByRowNumber = addRecordFieldToRow(recordsFieldWrapperByRowNumber, printPageField, row, nextRow, mainObjectName, record);

						if (maxRow<row) {
							maxRow = row;
						}
					}
				}
				else {
					recordsFieldWrapperByRowNumber = addRecordFieldToRow(recordsFieldWrapperByRowNumber, printPageField, row, nextRow, mainObjectName, record);

					if (maxRow<row) {
						maxRow = row;
					}
				}
			}
			nextRow = maxRow;
			recordWrapper.recordsFieldWrapperByRowNumber = recordsFieldWrapperByRowNumber;
			recordsWrapper.add(recordWrapper);
			recordCount++;
		}

		// for (RecordWrapper r : recordsWrapper) {
		// 	for (Integer key : r.recordsFieldWrapperByRowNumber.keySet()) {
				
		// 		for (RecordFieldWrapper field : r.recordsFieldWrapperByRowNumber.get(key).recordsFieldWrapper) {
		// 			System.debug('filed ' + field + '\n');
		// 		}
		// 	}
		// }

		return recordsWrapper;
	}
	public static List<SObject> getFilteredRecords(List<Service__c> services, Datetime startDate, Datetime endDate){
		
		Datetime min = startDate;
		Datetime max = endDate;

		List<SObject> recordsList = new List<SObject>();

		if(min == null && max == null){
			recordsList = services;
		}else{
			
			for(Service__c record : services){
				if(min != null && max != null){
					if(min <= record.Date_of_Service__c && record.Date_of_Service__c <= max){
						recordsList.add(record);
					}
				}else if(min == null){
					if(record.Date_of_Service__c <= max){
						recordsList.add(record);
					}
				}else if(max == null){
					if(min <= record.Date_of_Service__c){
						recordsList.add(record);
					}
				}
			}
		}
		return recordsList;
	}
	public static Map<Integer, RowWrapper> addRecordFieldToRow(Map<Integer, RowWrapper> recordsFieldWrapperByRowNumber, PrintPageFieldSettings__mdt printPageField, Integer row, Integer nextRow, String mainObjectName, SObject record) {
		//System.debug('printPageField '+printPageField);

		List<String> printFields = new List<String> ();
		if (printPageField.Field_API_Name__c != null && printPageField.Field_API_Name__c != '') {
			printFields = printPageField.Field_API_Name__c.split('\\.');
		}
		row = (Integer) printPageField.Row__c + nextRow;

		RecordFieldWrapper recordFieldWrapper = new RecordFieldWrapper();
		recordFieldWrapper.fieldApiName = printPageField.Field_API_Name__c;
		recordFieldWrapper.rowNumber = printPageField.Row__c;

		if (printPageField.Use_Filed_Label__c) {
			try {
				recordFieldWrapper.fieldLabel = ObjectUtils.getFieldLabelFromObjectByFieldName(mainObjectName.toLowerCase(), printPageField.Field_API_Name__c.toLowerCase());
			} catch(Exception ex) {
				recordFieldWrapper.fieldLabel = printPageField.Field_API_Name__c.replaceAll('__c', '').replaceAll('__r', '').replaceAll('_', ' ').replaceAll('\\.', ' ');
			}
		} else {
			if (printPageField.Filed_Label__c == '' || printPageField.Filed_Label__c == null) {
				recordFieldWrapper.fieldLabel = printPageField.Field_API_Name__c.replaceAll('__c', '').replaceAll('__r', '').replaceAll('_', ' ').replaceAll('\\.', ' ');
			} else {
				recordFieldWrapper.fieldLabel = printPageField.Filed_Label__c;
			}
		}

		//** get parent value
		if (printFields.size() == 6) {
			recordFieldWrapper.recordValue = record.getSObject(printFields.get(0).toLowerCase()).getSObject(printFields.get(1).toLowerCase()).getSObject(printFields.get(2).toLowerCase()).getSObject(printFields.get(3).toLowerCase()).getSObject(printFields.get(4).toLowerCase()).get(printFields.get(5).toLowerCase()) + '';
		} else if (printFields.size() == 5) {
			recordFieldWrapper.recordValue = record.getSObject(printFields.get(0).toLowerCase()).getSObject(printFields.get(1).toLowerCase()).getSObject(printFields.get(2).toLowerCase()).getSObject(printFields.get(3).toLowerCase()).get(printFields.get(4).toLowerCase()) + '';
		} else if (printFields.size() == 4) {
			recordFieldWrapper.recordValue = record.getSObject(printFields.get(0).toLowerCase()).getSObject(printFields.get(1).toLowerCase()).getSObject(printFields.get(2).toLowerCase()).get(printFields.get(3).toLowerCase()) + '';
		} else if (printFields.size() == 3) {
			recordFieldWrapper.recordValue = record.getSObject(printFields.get(0).toLowerCase()).getSObject(printFields.get(1).toLowerCase()).get(printFields.get(2).toLowerCase()) + '';
		} else if (printFields.size() == 2) {
			recordFieldWrapper.recordValue = record.getSObject(printFields.get(0).toLowerCase()).get(printFields.get(1).toLowerCase()) + '';
		} else if (printFields.size() == 1) {
			recordFieldWrapper.recordValue = record.get(printFields.get(0).toLowerCase()) + '';
		}

		if (recordsFieldWrapperByRowNumber.containsKey(row)) {
			RowWrapper rowWrapper = recordsFieldWrapperByRowNumber.get(row);
			List<RecordFieldWrapper> recordsFieldWrapper = rowWrapper.recordsFieldWrapper;

			recordsFieldWrapper.add(recordFieldWrapper);
			rowWrapper.recordsFieldWrapper = recordsFieldWrapper;
			recordsFieldWrapperByRowNumber.put(row, rowWrapper);
		} else {
			RowWrapper rowWrapper = new RowWrapper();
			rowWrapper.withoutHeader = printPageField.WithoutHeader__c;
			rowWrapper.withoutValue = printPageField.WithoutValue__c;
			rowWrapper.isHeaderInCell = printPageField.IsHeaderInCell__c;
			rowWrapper.recordsFieldWrapper = new List<RecordFieldWrapper> { recordFieldWrapper };
			recordsFieldWrapperByRowNumber.put(row, rowWrapper);
		}
		return recordsFieldWrapperByRowNumber;
		
	}

	public static List<PrintPageFieldSettings__mdt> getCleanPrintPageSettings(List<PrintPageFieldSettings__mdt> printPageFieldSettings, String mainObjectName) {
		List<PrintPageFieldSettings__mdt> cleanPrintPageFieldSettings = new List<PrintPageFieldSettings__mdt> ();

		List<Exception_Log__c> exceptionLogs = new List<Exception_Log__c> ();

		for (Integer i = 0; i<printPageFieldSettings.size(); i++) {
			List<String> printFieldName = new List<String> ();
			if (printPageFieldSettings.get(i).Field_API_Name__c != null) {
				printFieldName = printPageFieldSettings.get(i).Field_API_Name__c.split('\\.');
			}
			ExceptionLogWrapper sObjectHasField = isFieldExistOnSObject(printFieldName, mainObjectName);
			if (sObjectHasField.isNotError) {
				cleanPrintPageFieldSettings.add(printPageFieldSettings.get(i));
			} else {
				exceptionLogs.add(sObjectHasField.exceptionLog);
			}
		}

		try {
			if (!exceptionLogs.isEmpty()) {
				insert exceptionLogs;
			}
		} catch(Exception ex) {
			System.debug('Exception ' + ex.getMessage());
		}

		return cleanPrintPageFieldSettings;
	}

	public static ExceptionLogWrapper isFieldExistOnSObject(List<String> printFieldsName, String mainObjectName) {
		Boolean isFieldExist = true;
		ExceptionLogWrapper exceptionLogWrapper = new ExceptionLogWrapper();
		Map<String, Schema.SObjectField> sObjectFieldsByFieldName = ObjectUtils.getAllObjectFields(mainObjectName);

		if (printFieldsName.size() == 1) {
			if (!sObjectFieldsByFieldName.containsKey(printFieldsName.get(0))) {
				isFieldExist = false;
			}
		} else {
			try {
				List<Schema.SObjectType> newObjectsName = new List<Schema.SObjectType> ();
				for (Integer i = 0; i<printFieldsName.size(); i += 2) {
					if (i == 0) {
						try {
							newObjectsName = getReferenceObjectByFieldName(mainObjectName, printFieldsName.get(i).replaceAll('__(r|R)', '__c'));
						} catch(Exception ex) {
							newObjectsName = getReferenceObjectByFieldName(mainObjectName, printFieldsName.get(i) + 'Id');
						}
						try {
							newObjectsName = getReferenceObjectByFieldName(String.valueof(newObjectsName[0]), printFieldsName.get(i + 1).replaceAll('__(r|R)', '__c'));
						} catch(Exception ex) {
							newObjectsName = getReferenceObjectByFieldName(String.valueof(newObjectsName[0]), printFieldsName.get(i + 1) + 'Id');
						}
					} else {
						try {
							newObjectsName = getReferenceObjectByFieldName(String.valueof(newObjectsName[0]), printFieldsName.get(i).replaceAll('__(r|R)', '__c'));
						} catch(Exception ex) {
							newObjectsName = getReferenceObjectByFieldName(String.valueof(newObjectsName[0]), printFieldsName.get(i) + 'Id');
						}
					}
				}

			} catch(Exception e) {
				isFieldExist = false;

				Exception_Log__c exceptionLog = new Exception_Log__c();
				exceptionLog.Exception_Type__c = 'Field does not exist';
				exceptionLog.Message__c = 'No such column ' + printFieldsName + ' on entity ' + mainObjectName;
				exceptionLog.Stack_Trace__c = 'CustomMetadataType.PrintPageFieldSettings__mdt\n' + currentObjectName;

				exceptionLogWrapper.exceptionLog = exceptionLog;
			}
		}
		exceptionLogWrapper.isNotError = isFieldExist;

		return exceptionLogWrapper;
	}

	public static List<PrintPageFieldSettings__mdt> printPageFieldSettings(String objectName) {
		String query = ObjectUtils.getAllFields('PrintPageFieldSettings__mdt') + ', PrintPageSetting__r.objectApiName__c, PrintPageSetting__r.CurrentObjectApiName__c, PrintPageSetting__r.IsVisible__c, PrintPageSetting__r.UseCurrentObject__c, PrintPageSetting__r.RecordApiName__c FROM PrintPageFieldSettings__mdt WHERE IsVisible__c = TRUE AND PrintPageSetting__r.IsVisible__c = TRUE AND PrintPageSetting__r.CurrentObjectApiName__c = \'' + objectName + '\' ORDER BY Order__c';
		List<PrintPageFieldSettings__mdt> printPageFieldSettings = Database.query(query);

		System.debug('query 1');
		for (PrintPageFieldSettings__mdt p : printPageFieldSettings) {
			System.debug(p.Field_API_Name__c + ' // ' + p.PrintPageSetting__r.ObjectApiName__c);
		}
	
		return printPageFieldSettings;
	}

	public class RecordFieldWrapper {
		@AuraEnabled public String fieldLabel;
		@AuraEnabled public Object recordValue;
		@AuraEnabled public String fieldApiName;
		@AuraEnabled public String recordType;

		@AuraEnabled public Decimal rowNumber;
		//@AuraEnabled public RecordWrapper childRecordWrapper;
	}

	public class RecordWrapper {
		@AuraEnabled public String recordName;
		@AuraEnabled public Map<Integer, RowWrapper> recordsFieldWrapperByRowNumber;
		@AuraEnabled public String childRecodsName;
		@AuraEnabled public List<RecordWrapper> childRecordWrapper;
	}

	public class RowWrapper {
		@AuraEnabled public List<RecordFieldWrapper> recordsFieldWrapper;
		@AuraEnabled public Boolean withoutHeader;
		@AuraEnabled public Boolean withoutValue;
		@AuraEnabled public Boolean isHeaderInCell;
	}

	public class StartDataWrapper {
		@AuraEnabled public String minDate;
		@AuraEnabled public String maxDate;
		@AuraEnabled public List<RecordType> recordTypesList;

		@AuraEnabled public List<Object> filters;
		@AuraEnabled public List<PrintPageFieldSettings__mdt> cleanPrintPageFieldSettings;

		@AuraEnabled public String currentObjectName;
		@AuraEnabled public String mainObjectName;
		@AuraEnabled public String recordName;
		@AuraEnabled public List<RecordWrapper> recordsWrapper;
	}

	public class ExceptionLogWrapper {
		public Exception_Log__c exceptionLog;
		public Boolean isNotError;
	}
}