//**
// Author: Yuliia Artemenko
// Date: June 19, 2019
// Description: 
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//** 
global class BatchAuditCreate implements Database.Batchable<SObject> {
	
	private String developerName;
	private String query;

	global BatchAuditCreate(String developerName, String query){
		
		this.developerName = developerName;
		this.query = query;
	}
	
	global Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext context, List<SObject> scope) {

		Map<String, List<SObject>> existingPrograms = new Map<String, List<SObject>>();
		existingPrograms.put(developerName, scope);

		BatchAuditCreateHelper.insertAudit(existingPrograms);
	}
	
	global void finish(Database.BatchableContext context) {}
}