@IsTest
public class AttendanceTriggerHandlerTest {
  @Testsetup
    public static void setup(){
        TestFactory.createDataForClassRosterTrigger();          
    }
    
    @IsTest
    public static void updateDateLastAttendaneOnContactObject_test(){
        List<Attendance__c> aList = [SELECT id, Date__c,Status__c,Present__c FROM Attendance__c];
        
        for(Attendance__c a : aList){
            a.Date__c = Date.today();
            a.Status__c = 'Present';
        }
        update aList;
        
        List<Attendance__c> attendanceList = [SELECT id, Date__c,Status__c,Class_roster__c,Present__c FROM Attendance__c];
        
        List<Id> idsClassRoster = new List<Id>();
        
        For(Attendance__c a: attendanceList){
            idsClassRoster.add(a.Class_Roster__c);
            
        }
        
        List<ID> IdContacts = new List<Id>();
        
        List<Class_Roster__c> crlList = [SELECT id, Class__c,Contact__c FROM Class_Roster__c WHERE Id IN: idsClassRoster];
        
                
        For(Class_Roster__c cr: crlList){
            IdContacts.add(cr.Contact__c);
            
        }
        
        List<ID> accountIds = new List<Id>();
        List<Contact> contactListToUpdateDate = [SELECT id,AccountId,Last_Attendance_Date__c FROM Contact WHERE ID IN: IdContacts];
        
        For(Contact c: contactListToUpdateDate){
            accountIds.add(c.AccountId);
            
        }
        
        List<Account> acList = [SELECT id,Last_Attendance_Date__c FROM Account WHERE id IN: accountIds ];
                
         system.assertEquals(aList.get(0).Date__c, Date.today());
         system.assertEquals(acList.get(0).Last_Attendance_Date__c, Date.today());
         system.assertEquals(contactListToUpdateDate.get(0).Last_Attendance_Date__c, Date.today());
         system.assertEquals(aList.get(0).Status__c, 'Present');
        
        
        system.debug(aList);
    }

   /*  @isTest
   static void updateLastAttendanceDateContactAndAccount(){

       List<Attendance__c> attendances = [SELECT Status__c, Date__c, Class_roster__c, Present__c FROM Attendance__c WHERE Status__c = 'Present'];

       update attendances;

       Date today = Date.today();

       List<Contact> contacts = [SELECT Id FROM Contact WHERE Contact_Last_Attendance_Date__c =:today];
       List<Account> accounts = [SELECT Id FROM Account WHERE Account_Last_Attendance_Date__c =:today];

       System.assertEquals(attendances.size(), contacts.size());
       System.assertEquals(attendances.size(), accounts.size());

   }
*/    
}