/* Provisio Partners
** Author: Dnyaneshwar Waghmare
** Date: 08/20/2018
** Description: Test class for Daily Attendance Controller test.     
**/

@IsTest
public class DailyAttendanceControllerTest {

    @TestSetup
    public static void setup(){
        TEstFactory.createDataForAttendanceController();
    }

	@IsTest
    public static void attendancetestMethod(){
        
        String datestring = String.valueOf(Date.today().addDays(4));
        List<Class_Roster__c> crList = [SELECT ID FROM Class_Roster__c]; 
		List<Attendance__c> attendanceList = [SELECT Id FROM Attendance__c];         
        
        Test.startTest();  
        DailyAttendanceController DailyAttendanceControllerObj= new DailyAttendanceController();
        DailyAttendanceController.serverGetAttendanceRecords(crList[0].Id, datestring);
        DailyAttendanceController.serverUpdateAttendanceRecord(attendanceList);
        Test.stopTest();
    }
}