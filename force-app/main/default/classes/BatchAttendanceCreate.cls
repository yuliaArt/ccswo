//**
// Author: Ivanna Kuzemchak
// Date: November 26, 2018
// Description: This is a Batch Class that creates Attendance records for a week for active classes.
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

global class BatchAttendanceCreate implements Database.Batchable<sObject>{

	global Database.QueryLocator start(Database.BatchableContext BC) {
		String classQuery = ObjectUtils.getAllFieldsFromObject('Class__c') + ' WHERE status__c = \'' + GlobalVariable.CLASS_STATUS + '\' AND Auto_Create_Attendance__c = True';
		return Database.getQueryLocator(classQuery);
	}

	global void execute(Database.BatchableContext BC, List<Class__c> scope) {
		BatchAttendanceCreateHelper.insertAttendance(scope);
	}

	global void finish(Database.BatchableContext BC) {}

}