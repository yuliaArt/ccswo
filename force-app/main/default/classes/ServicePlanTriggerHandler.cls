//**
// Author: Yuliia Artemenko
// Date: June 10, 2019
// Description: this code updates 
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

public class ServicePlanTriggerHandler {
    public static void entry(TriggerParams triggerParams) {

		List<Service_Plan__c> triggerNew = (List<Service_Plan__c>) triggerParams.triggerNew;
		List<Service_Plan__c> triggerOld = (List<Service_Plan__c>) triggerParams.triggerOld;
		Map<Id, Service_Plan__c> oldMap = (Map<Id, Service_Plan__c>) triggerParams.oldMap;
		Map<Id, Service_Plan__c> newMap = (Map<Id, Service_Plan__c>) triggerParams.newMap;

		if (triggerParams.isBefore) {
			if (triggerParams.isInsert) {
		
			}
		}
		if (triggerParams.isAfter) {
			if (triggerParams.isInsert) {
                			
			}
			if (triggerParams.isUpdate) {
				//checkReviewServicePlans(triggerNew);
			}
		}
	}

    public static void checkReviewServicePlans(List<Service_Plan__c> triggerNew){

        Map<Id, List<Service_Plan__c>> mapOfServicePlans =  new Map<Id, List<Service_Plan__c>>();

        for(Service_Plan__c record: triggerNew){
            if(!mapOfServicePlans.containsKey(record.Program_Enrollment__c)){
                mapOfServicePlans.put(record.Program_Enrollment__c, new List<Service_Plan__c>());
            }
            mapOfServicePlans.get(record.Program_Enrollment__c).add(record);
        }

        List<Service_Plan__c> relatedListofServicePlans = [SELECT Id, Name, Program_Enrollment__c, Last_Reviewed_Date__c, CreatedDate 
															FROM Service_Plan__c where Program_Enrollment__c IN: mapOfServicePlans.keySet() 
															AND  Last_Reviewed_Date__c = null order by CreatedDate desc];
															System.debug('relatedListofServicePlans 1 '+relatedListofServicePlans);
															// System.debug('relatedListofServicePlans limit 1 '+relatedListofServicePlans);
		if(relatedListofServicePlans.size() != 1){

		}
	}
}