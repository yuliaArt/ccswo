@IsTest
public class ClassRosterTriggerHandlerTest {
    
    @testSetup static void initialize() {	
        TestFactory.createDataForClassRosterTrigger();
    }
    
	@IsTest static void checkMaximumEnrollmentTest(){
		Contact con = [SELECT Id FROM Contact LIMIT 1];
		Class__c cls = [SELECT Id FROM Class__c LIMIT 1];
		Class_Roster__c clsroster = (Class_Roster__c) TestFactory.createSObject(new Class_Roster__c(Class__c = cls.id, Contact__c = con.id), 'TestFactory.Class_RosterDefaults', true);
	}

	@IsTest static void checkStartDayOnClass(){
		Contact con = [SELECT Id FROM Contact LIMIT 1];
		Class__c cls = [SELECT Id FROM Class__c LIMIT 1];
		try{	        
			Class_Roster__c clsroster = (Class_Roster__c) TestFactory.createSObject(new Class_Roster__c(Class__c = cls.id, Contact__c = con.id, Start_Date__c = Date.today().toStartOfWeek().addDays(-10)), 'TestFactory.Class_RosterDefaults', true);
		}
		catch (Exception ex){
			Boolean expectedExceptionThrown =  ex.getMessage().contains('Please enter later Start Date') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}
	}
	
	@IsTest static void updateAttendanceRecords() {
		List<Class_Roster__c> clrList = [Select Id, End_Date__c, Start_Date__c, Class__r.Current_Enrollment__c from Class_Roster__c];
		for (Class_Roster__c cr : clrList) {
            cr.End_Date__c = Date.today().toStartOfWeek().addDays(10);
        }
        update clrList;
	}

	@IsTest static void updateAttendanceRecordsDelete() {
		List<Class_Roster__c> clrList = [Select Id, End_Date__c, Start_Date__c, Class__r.Current_Enrollment__c from Class_Roster__c];
		for (Class_Roster__c cr : clrList) {
            cr.End_Date__c = Date.today().toStartOfWeek().addDays(-5);
        }
        update clrList;
	}
}