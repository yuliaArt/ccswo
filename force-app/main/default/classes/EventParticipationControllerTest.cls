@isTest
public class EventParticipationControllerTest {

	 @testSetup static void testSetup() {
	 	TestFactory.eventParticipantControllerTest();

	 }

	 @isTest
	 public static void nameofclassTest() {
	 	Birdseye_Event__c evtlist = [Select Id from Birdseye_Event__c limit 1];
	 	Test.startTest();
	 	String eventName = EventParticipationController.nameofclass(evtlist.Id);
	 	System.assertEquals('Test Name', eventName);
	 	Test.stopTest();
	 }

	 @isTest
	 public static void getContactWrappersTest() {
	 	Test.startTest();
	 	EventParticipationController.getContactWrappers('First');
	 	Test.stopTest();
	 }

	 @isTest
	 public static void newConRecInsertTest() {
	 	List<Contact> cntToInsrt = new List<Contact> ();
	 	Contact firstCnt = new Contact();
	 	firstCnt.LastName = 'TestLastName';
	 	cntToInsrt.add(firstCnt);
	 	Contact secondCnt = new Contact();
	 	secondCnt.LastName = 'TestSecondLastName';
	 	cntToInsrt.add(secondCnt);

	 	Birdseye_Event__c evtlist = [Select Id from Birdseye_Event__c limit 1];

	 	List<Contact> cntToUpd = [Select Id, firstName, lastName from Contact Where lastName = 'Last'];

	 	system.debug('contact ' + cntToUpd);

	 	Test.startTest();
	 	EventParticipationController.newRecordToInsert(cntToInsrt, evtlist.Id, cntToUpd);
	 	Test.stopTest();
	 }

	 @isTest
	 public static void newConRecInsertExceptionTest() {

	 	List<Contact> cntToInsrt = new List<Contact> ();

	 	Birdseye_Event__c evtlist = [Select Id from Birdseye_Event__c limit 1];

	 	Test.startTest();
	 	try {
	 		EventParticipationController.newRecordToInsert(cntToInsrt, evtlist.Id, cntToInsrt);
	 	} catch(Exception ex) {
	 		Boolean expectedExceptionThrown = ex.getMessage().contains('Please selected at least one contact, or enter the last name for at least one new contact.') ? true : false;
	 		// System.assertEquals(expectedExceptionThrown, true);
	 	}
	 	Test.stopTest();
	 }

	 @isTest
	 public static void newRefRecInsertTest() {

	 	List<Contact> cntToInsrt = new List<Contact> ();

	 	Contact firstCnt = new Contact();
	 	firstCnt.LastName = 'TestLastName';
	 	cntToInsrt.add(firstCnt);
	 	Contact secondCnt = new Contact();
	 	secondCnt.LastName = 'TestSecondLastName';
	 	cntToInsrt.add(secondCnt);

	 	Birdseye_Event__c evtlist = [Select Id from Birdseye_Event__c limit 1];

	 	List<Contact> cntToUpd = [Select Id from Contact Where Name = 'TestName'];

	 	Test.startTest();
	 	EventParticipationController.newRecordToInsert(cntToInsrt, evtlist.Id, cntToUpd);
	 	Test.stopTest();
	 }

	 @isTest
	 public static void newEventPpWrapperTest() {
	 	Test.startTest();
	 	EventParticipationController.newEventPpWrapper();
	 	Test.stopTest();
	 }

	 @isTest
	 public static void newRefRecInsertExceptionTest() {

	 	List<Contact> cntToInsrt = new List<Contact> ();

	 	Birdseye_Event__c evtlist = [Select Id from Birdseye_Event__c limit 1];

	 	Test.startTest();
	 	try {
	 		EventParticipationController.newRecordToInsert(cntToInsrt, evtlist.Id, cntToInsrt);
	 	} catch(Exception ex) {
	 		Boolean expectedExceptionThrown = ex.getMessage().contains('Please selected at least one contact, or enter the last name for at least one new contact.') ? true : false;
	 		System.assertEquals(expectedExceptionThrown, true);
	 	}
	 	Test.stopTest();
	 }

	 //@isTest
	 //public static void insertAttdenceTest() {

	 	//Contact cnt = [select Id, LastName from Contact limit 1];

	 	//ContactWrapper wrp = new ContactWrapper(true, cnt.LastName, cnt.Id);
	 	//List<ContactWrapper> contactWrapperList = new List<ContactWrapper> ();
	 	//contactWrapperList.add(wrp);

	 	//String cntJSON = JSON.serialize(contactWrapperList);
	 	//System.debug('cntJSON: ' + cntJSON);


	 	//Birdseye_Event__c evtlist = [Select Id from Birdseye_Event__c limit 1];

	 	////EventPpWrapper evwrp = new EventPpWrapper();
	 	////List<EventPpWrapper> eventWrapperList = new List<EventPpWrapper>();
	 	////  eventWrapperList.add(evwrp);

	 	////Birdseye_Event__c evtlist = [Select Id from Birdseye_Event__c limit 1];

	 	//String eventJSON = JSON.serialize(evtlist);

	 	//Test.startTest();

	 	//EventParticipationController.insertAttdence(cntJSON, eventJSON);

	 	//Test.stopTest();
	 //}

	 @isTest
	 public static void widthOfComponent() {

	 	Test.startTest();
	 	EventParticipationController.widthOfComponent();
	 	Test.StopTest();
	 }




}