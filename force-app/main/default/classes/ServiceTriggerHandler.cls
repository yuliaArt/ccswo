//**
// Author: Yaroslav Mazuryk
// Date: March 3, 2019
// Description: this code set Last Attendance Date to Contact and Account
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

public class ServiceTriggerHandler {
    public static void entry(TriggerParams triggerParams) {

		List<Service__c> triggerNew = (List<Service__c>) triggerParams.triggerNew;
		List<Service__c> triggerOld = (List<Service__c>) triggerParams.triggerOld;
		Map<Id, Service__c> oldMap = (Map<Id, Service__c>) triggerParams.oldMap;
		Map<Id, Service__c> newMap = (Map<Id, Service__c>) triggerParams.newMap;

		if (triggerParams.isBefore) {
			if (triggerParams.isInsert) {
		
			}
		}
		if (triggerParams.isAfter) {
			if (triggerParams.isInsert) {
                setLastServiceDate(triggerNew);				
			}
			if (triggerParams.isUpdate) {

			}
		}
	}
    
    public static void setLastServiceDate(List<Service__c> triggerNew){
        List<ID> serviceIds = new List<ID>();
        
        for(Service__c c: triggerNew){
            serviceIds.add(c.id);
        }
        
        List<Id> prograEnrollmentIds = new List<Id>();
        Map<Id, Date> mapTosaveDate = new Map<Id, Date>();    
        
        List<Program_Enrollment__c> newListProgramEnrollments = [SELECT Id, Contact__c FROM Program_Enrollment__c WHERE Id IN: prograEnrollmentIds];
        Map<id, Date> mapToSaveDatePe = new Map<Id, Date>();
        
        List<ID> contactIDsToSearch = new List<ID>();
        
        for(Program_Enrollment__c pe : newListProgramEnrollments){
            contactIDsToSearch.add(pe.Contact__c);
            mapToSaveDatePe.put(pe.Contact__c, mapTosaveDate.get(pe.Id));
            
        }
        Map<ID, Date> mapToSaveAccountDate = new Map<Id, Date>();
        
        List<Contact> toUpdateContacts = [SELECT ID,Last_Service_Date__c,AccountId FROM Contact WHERE ID IN: contactIDsToSearch];
        List<ID> accountsIDs = new List<Id>();
        for(Contact c : toUpdateContacts){
            if(c.Last_Service_Date__c < mapToSaveDatePe.get(c.ID) || c.Last_Service_Date__c == null){
            	c.Last_Service_Date__c = mapToSaveDatePe.get(c.ID);
                accountsIDs.add(c.AccountId);
                mapToSaveAccountDate.put(c.AccountId, mapToSaveDatePe.get(c.ID));
        	}
        }
        update toUpdateContacts;
        
        List<Account> alistToUpdate = [SELECT ID,Last_Service_Date__c FROM Account WHERE ID IN: accountsIDs ];
        system.debug(alistToUpdate);
        for(Account a: alistToUpdate ){
            if(a.Last_Service_Date__c < mapToSaveAccountDate.get(a.ID) || a.Last_Service_Date__c == null){
                a.Last_Service_Date__c = mapToSaveAccountDate.get(a.ID);
                
            }  
        }
        update alistToUpdate;
        
        system.debug(newListProgramEnrollments);
       

}
    
   
}