//**
// Author: Ivanna Kuzemchak
// Date: November 21, 2018
// Description: This is a Trigger Handler that process related Attendance records.
// If Class Roster record is created, this class creates attendance records for the rest of the week.
// If Class Roster record is updated, this class creates attendance records for the rest of the week or
// deletes unnecessery records.
//		Changes:
//		Author: Mykhailo Senyuk
//		Date: February 15, 2019
//		Description: Code was modified due to a more flexible.	
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**
public class ClassRosterTriggerHandler {

	public static void entry(TriggerParams triggerParams) {

		List<Class_Roster__c> triggerNew = (List<Class_Roster__c>) triggerParams.triggerNew;
		List<Class_Roster__c> triggerOld = (List<Class_Roster__c>) triggerParams.triggerOld;
		Map<Id, Class_Roster__c> oldMap = (Map<Id, Class_Roster__c>) triggerParams.oldMap;
		Map<Id, Class_Roster__c> newMap = (Map<Id, Class_Roster__c>) triggerParams.newMap;

		if (triggerParams.isBefore) {
			if (triggerParams.isInsert) {
				checkStartDayOnClass(triggerNew);
				checkMaximumEnrollment(triggerNew);
			}
			if (triggerParams.isUpdate) {
				checkStartDayOnClass(triggerNew);
			}
		}
		if (triggerParams.isAfter) {
			if (triggerParams.isInsert) {
				createNewAttendanceRecords(triggerNew, false, oldMap);
			}
			if (triggerParams.isUpdate) {
				updateAttendanceRecords(newMap, oldMap);
			}
		}

	}

	public static void createNewAttendanceRecords(List<Class_Roster__c> newClassRosters, Boolean isUpdate, Map<Id, Class_Roster__c> oldClassRostreMap) {
		Date startOfWeek = Date.today().toStartOfWeek();
		List<Attendance__c> attendanceToInsert = new List<Attendance__c> ();
		Map<String, Id> recordTypesIdByProgram = new Map<String, Id> ();
		Map<Id, List<Integer>> numberOfDaysByClassId = new Map<Id, List<Integer>> ();
		Map<Id, String> programByClassId = new Map<Id, String> ();
		
		System.debug('test 123');
		Set<Id> classIds = new Set<Id> ();
		for (Class_Roster__c classRoster : newClassRosters) {
			if (classRoster.Status__c == GlobalVariable.CLASS_ROSTER_STATUS) {
				classIds.add(classRoster.Class__c);
			}
		}

		//** attendance Create Settings By Picklist Api Name
		Map<String, List<AttendanceCreateSetting__mdt>> attendanceCreateSettingsByPicklistApiName = getAttendanceCreateSettings();
		//** End attendance Create Settings By Picklist Api Name

		String classQuery = ObjectUtils.getAllFieldsFromObject('Class__c') + ' WHERE status__c = \'' + GlobalVariable.CLASS_STATUS + '\' AND Id in :classIds AND Auto_Create_Attendance__c = True';
		Map<Id, Class__c> classById = new Map<Id, Class__c> ((List<Class__c>) Database.query(classQuery));

		List<Session__c> sessions = [SELECT id, Date__c, Class__c FROM Session__c WHERE Class__c in :classById.keySet()];
		Map<Id, Map<Date, Session__c>> sessionsByDateByClassId = new Map<Id, Map<Date, Session__c>> ();
		if (!sessions.isEmpty()) {
			for (Session__c session : sessions) {
				if (sessionsByDateByClassId.containsKey(session.Class__c)) {
					Map<Date, Session__c> sessionByDate = sessionsByDateByClassId.get(session.Class__c);
					sessionByDate.put(session.Date__c, session);
					sessionsByDateByClassId.put(session.Class__c, sessionByDate);
				} else {
					sessionsByDateByClassId.put(session.Class__c, new Map<Date, Session__c> { session.Date__c => session });
				}
			}
		}

		//** If Object Class__c has any records
		if (!classById.isEmpty()) {
			for (Class__c cls : classById.values()) {
				numberOfDaysByClassId.put(cls.Id, listOfClassDays(cls));
				//**Populate Map with Class Id and Program
				for (String program : attendanceCreateSettingsByPicklistApiName.keySet()) {
					if (ObjectUtils.HasSObjectField('Class__c', program)) {
						programByClassId.put(cls.Id, (String) classById.get(cls.Id).get(program));
					}
				}
			}

			//**if object has field RecordTypeId
			if (ObjectUtils.HasSObjectField('Attendance__c', 'RecordTypeId')) {
				recordTypesIdByProgram = getRecordTypesIdByProgram(attendanceCreateSettingsByPicklistApiName);
			} //** End if object has field RecordTypeId

			//** Loop by triggerNew Class_Roster__c
			for (Class_Roster__c classRoster : newClassRosters) {
				//** IF Class__c has any class days and Class_Roster__c has specific Status__c
				if (!numberOfDaysByClassId.get(classRoster.Class__c).isEmpty() && classRoster.Status__c != null && classRoster.Status__c.equalsIgnoreCase(GlobalVariable.CLASS_ROSTER_STATUS)) {
					for (Integer day : numberOfDaysByClassId.get(classRoster.Class__c)) {
						Date dateOfClass = startOfWeek.addDays(day);
						if(dateOfClass >= classById.get(classRoster.Class__c).Start_Date__c && dateOfClass <= classById.get(classRoster.Class__c).End_Date__c) {
							Id recordTypeId = recordTypesIdByProgram.get(programByClassId.get(classRoster.Class__c));
							Id sessionId = null;
							//System.debug('dateOfClass '+dateOfClass);
							//System.debug('classRoster.Class__c ' + classRoster.Class__c );
							//System.debug('sessionsByDateByClassId get class '+sessionsByDateByClassId.get(classRoster.Class__c));
							//System.debug('sessionsByDateByClassId get class get date '+sessionsByDateByClassId.get(classRoster.Class__c).get(dateOfClass));
							if( sessionsByDateByClassId != null && sessionsByDateByClassId.containsKey(classRoster.Class__c) && sessionsByDateByClassId.get(classRoster.Class__c).containsKey(dateOfClass) ){
								sessionId = sessionsByDateByClassId.get(classRoster.Class__c).get(dateOfClass).Id;
							}

							if (isUpdate) {
								if (dateOfClass > oldClassRostreMap.get(classRoster.Id).End_Date__c && dateOfClass <= classRoster.End_Date__c) {
									attendanceToInsert.add(createAttendance(classRoster, dateOfClass, recordTypeId, sessionId));
								}
							} else {
								if (startOfWeek <= dateOfClass && dateOfClass <= classRoster.End_Date__c) {
									attendanceToInsert.add(createAttendance(classRoster, dateOfClass, recordTypeId, sessionId));
								}
							}
						}
					}
				} //** End if Class__c has any class days and Class_Roster__c has specific Status__c
			} //** End loop by triggerNew Class_Roster__c

		} //** End If Object Class__c has any records
		System.debug('attendanceToInsert ' + attendanceToInsert);
		if (!attendanceToInsert.isEmpty()) {
			insert attendanceToInsert;
		}

	} //** End function createNewAttendanceRecords

	public static void updateAttendanceRecords(Map<Id, Class_Roster__c> newClassRosterMap, Map<Id, Class_Roster__c> oldClassRostreMap) {
		List<Class_Roster__c> classRosterToUpdate = new List<Class_Roster__c> ();
		Map<Id, Class_Roster__c> classRosterToUpdateById = new Map<Id, Class_Roster__c> ();
		List<Class_Roster__c> classRosterToDelete = new List<Class_Roster__c> ();
		Set<Id> classRostersId = new Set<Id> ();

		for (Id classRosterId : newClassRosterMap.keySet()) {
			if (newClassRosterMap.get(classRosterId).End_Date__c != oldClassRostreMap.get(classRosterId).End_Date__c) {
				if (newClassRosterMap.get(classRosterId).End_Date__c<oldClassRostreMap.get(classRosterId).End_Date__c) {
					classRostersId.add(classRosterId);
					classRosterToDelete.add(newClassRosterMap.get(classRosterId));
				}
				System.debug('new '+newClassRosterMap.get(classRosterId).End_Date__c);
				System.debug('old '+oldClassRostreMap.get(classRosterId).End_Date__c);
				if (newClassRosterMap.get(classRosterId).End_Date__c> oldClassRostreMap.get(classRosterId).End_Date__c) {
					classRosterToUpdate.add(newClassRosterMap.get(classRosterId));
					classRosterToUpdateById.put(classRosterId, oldClassRostreMap.get(classRosterId));
				}
			}
		}

		//Delete unnecessary Attendance Records
		if (!classRosterToDelete.isEmpty()) {
			List<Attendance__c> attendanceToDelete = new List<Attendance__c> ();
			List<Attendance__c> attendancePreDelete = new List<Attendance__c> ();

			attendancePreDelete = [Select Id, Date__c from Attendance__c Where Class_Roster__c in :classRostersId];
			for (Attendance__c attendance : attendancePreDelete) {
				for (Class_Roster__c classRoster : classRosterToDelete) {
					if (attendance.Date__c > classRoster.End_Date__c) {
						attendanceToDelete.add(attendance);
					}
				}
			}
			if (!attendanceToDelete.isEmpty()) {
				delete attendanceToDelete;
			}
		}

		//Create missing Attendance Records
		if (!classRosterToUpdate.isEmpty()) {
			createNewAttendanceRecords(classRosterToUpdate, true, classRosterToUpdateById);
		}

	} //** End function updateAttendanceRecords

	public static void checkStartDayOnClass(List<Class_Roster__c> newClassRosters) {
		Set<Id> classId = new Set<Id> ();
		for (Class_Roster__c classRoster : newClassRosters) {
			classId.add(classRoster.Class__c);
		}
		Map<Id, Date> startDateByClassId = new Map<Id, Date> ();
		List<Class__c> classList = [SELECT Id, Start_Date__c FROM Class__c WHERE iD IN :classId];

		for (Class__c cls : classList) {
			startDateByClassId.put(cls.Id, cls.Start_Date__c);
		}

		for (Class_Roster__c classRoster : newClassRosters) {
			if (classRoster.Start_Date__c<startDateByClassId.get(classRoster.Class__c)) {
				classRoster.AddError('Your class will start on' + '  ' + startDateByClassId.get(classRoster.Class__c).format() + '  Please enter later Start Date');
				classRoster.Start_Date__c.AddError('Please enter later Start Date');
			}
		}

	} //** End function checkStartDayOnClass (validation)

	public static void checkMaximumEnrollment(List<Class_Roster__c> newClassRosters) {
		Set<Id> classId = new Set<Id> ();
		for (Class_Roster__c classRoster : newClassRosters) {
			classId.add(classRoster.Class__c);
		}
		Map<Id, Class__c> classById = new Map<Id, Class__c> ([SELECT Id, Maximum_Enrollment__c, Current_Enrollment__c FROM Class__c WHERE Id IN :classId]);

		for (Class_Roster__c classRoster : newClassRosters) {
			Integer maxEnrollment = (Integer) classById.get(classRoster.Class__c).Maximum_Enrollment__c;
			Integer currentEnrollment = (Integer) classById.get(classRoster.Class__c).Current_Enrollment__c + 1;
			
			if(maxEnrollment > 0 && currentEnrollment > 0){
				if(currentEnrollment - maxEnrollment > 0){
					classRoster.Waitlist_Number__c = currentEnrollment - maxEnrollment;
					classRoster.Status__c = GlobalVariable.CLASS_ROSTER_STATUS_WAITLIST;
				}
			}
		}
	} //** End function checkMaximumEnrollment 

	public static Attendance__c createAttendance(Class_Roster__c classRoster, Date dateOfClass, Id recordTypeId, Id sessionId) {
		Attendance__c attendance = new Attendance__c();
		attendance.Status__c = GlobalVariable.ATTENDANCE_STATUS;
		attendance.Class_Roster__c = classRoster.Id;
		attendance.Date__c = dateOfClass;
		attendance.System_Created__c = true;
		attendance.Session__c = sessionId;
		
		if (ObjectUtils.HasSObjectField('Attendance__c', 'RecordTypeId') && recordTypeId != null) {
			attendance.put('RecordTypeId', recordTypeId);
		}
		return attendance;
	}

	public static List<Integer> listOfClassDays(Class__c cls) {
		List<Integer> days = new List<Integer> ();
		if (cls.Sunday__c) days.add(0);
		if (cls.Monday__c) days.add(1);
		if (cls.Tuesday__c) days.add(2);
		if (cls.Wednesday__c) days.add(3);
		if (cls.Thursday__c) days.add(4);
		if (cls.Friday__c) days.add(5);
		if (cls.Saturday__c) days.add(6);

		return days;
	}

	public static Map<String, List<AttendanceCreateSetting__mdt>> getAttendanceCreateSettings() {
		List<AttendanceCreateSetting__mdt> attendanceCreateSettings = [SELECT Class_Picklist_API_Name__c, IsVisible__c, Picklist_Value__c, Record_Type_Developer_Name__c FROM AttendanceCreateSetting__mdt WHERE IsVisible__c = true];

		Map<String, List<AttendanceCreateSetting__mdt>> getAttendanceCreateSettingsByPicklistApiName = new Map<String, List<AttendanceCreateSetting__mdt>> ();
		for (AttendanceCreateSetting__mdt attendanceSetting : attendanceCreateSettings) {
			if (getAttendanceCreateSettingsByPicklistApiName.containsKey(attendanceSetting.Class_Picklist_API_Name__c)) {
				List<AttendanceCreateSetting__mdt> attendanceCreateSettingList = getAttendanceCreateSettingsByPicklistApiName.get(attendanceSetting.Class_Picklist_API_Name__c);
				attendanceCreateSettingList.add(attendanceSetting);
				getAttendanceCreateSettingsByPicklistApiName.put(attendanceSetting.Class_Picklist_API_Name__c, attendanceCreateSettingList);
			} else {
				getAttendanceCreateSettingsByPicklistApiName.put(attendanceSetting.Class_Picklist_API_Name__c, new List<AttendanceCreateSetting__mdt> { attendanceSetting });
			}
		}

		return getAttendanceCreateSettingsByPicklistApiName;
	}

	public static Map<String, Id> getRecordTypesIdByProgram(Map<String, List<AttendanceCreateSetting__mdt>> attendanceCreateSettingsByPicklistApiName) {
		Map<String, Id> allRecordTypesIdByProgram = new Map<String, Id> ();
		Map<String, Schema.RecordTypeInfo> allRecordTypesFromObject = ObjectUtils.getAllRecordTypesFromObject('Attendance__c');
		for (String program : attendanceCreateSettingsByPicklistApiName.keySet()) {
			for (AttendanceCreateSetting__mdt attendanceSetting : attendanceCreateSettingsByPicklistApiName.get(program)) {
				if (ObjectUtils.hasSObjectRecordType(allRecordTypesFromObject, attendanceSetting.Record_Type_Developer_Name__c)) {
					Id recordTypeId = ObjectUtils.getRecordTypeId(allRecordTypesFromObject, attendanceSetting.Record_Type_Developer_Name__c);
					allRecordTypesIdByProgram.put(attendanceSetting.Picklist_Value__c, recordTypeId);
				}
			}
		}

		return allRecordTypesIdByProgram;
	}

}