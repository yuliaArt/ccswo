//**
// Author: Ivanna Kuzemchak
// Date: November 26, 2018
// Description: This is a Schedule Class that invokes BatchAttendanceCreate Class to create Attendance records for active classes.
// It should be scheduled once a week on Sunday. 
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**


global class BatchScheduleAttendanceCreate implements Schedulable {
	global void execute(SchedulableContext sc) {
		proccessSchedule();
	}

	private void proccessSchedule() {
		BatchAttendanceCreate bachAttendance = new BatchAttendanceCreate();

		String classQuery = ObjectUtils.getAllFieldsFromObject('Class__c') + ' WHERE status__c = \'' + GlobalVariable.CLASS_STATUS + '\' AND Auto_Create_Attendance__c = True';
		List<Class__c> classList = Database.query(classQuery);

		//Check if exist any Class__c records
		if (!classList.isEmpty()) {
			//Check if there are less than 5 batches active, otherwise wait for 5 minutes and try again
			Integer numberOfBatch = [SELECT count() FROM AsyncApexJob WHERE JobType = 'BatchApex' AND Status IN('Processing', 'Preparing', 'Queued')];
			if (numberOfBatch < 5 ) {
				Database.executeBatch(bachAttendance, 200);
			} 
			if (numberOfBatch >= 5 || Test.isRunningTest()) {
				BatchScheduleAttendanceCreate delayBachAttendance = new BatchScheduleAttendanceCreate();
				Datetime dt = Datetime.now().addMinutes(5);
				String timeForScheduler = dt.format('s m H d M \'?\' yyyy');
				Id schedId = System.Schedule('Create Attendance Batch Schedule' + timeForScheduler, timeForScheduler, delayBachAttendance);
			}

		}
	}

	/* At 21:00:00pm, on every Sunday, every month
		BatchScheduleAttendanceCreate updater = new BatchScheduleAttendanceCreate();
		String sch = '0 0 21 ? * SUN *';
		system.schedule('Create Attendances one time per week', sch, updater);
	*/

}