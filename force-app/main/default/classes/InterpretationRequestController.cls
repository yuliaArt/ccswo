public class InterpretationRequestController {
    @AuraEnabled
	public static String getRecord(){
        //logged user
        User loggedUser = [SELECT Id, Name, ContactId FROM User WHERE Id =: UserInfo.getUserId()];

        DateTime now = System.now();

        //User loggedUser = [SELECT Id, Name, ContactId FROM User WHERE Id =: '0055C000001yjoa'];

        Interpretation_Request_Settings__mdt interpretationReqMdt = [SELECT id, MasterLabel, Value__c FROM Interpretation_Request_Settings__mdt 
                                                                            WHERE Value__c =: GlobalVariable.InterReq_Interpreter_Scheduled];
        //query list of int. request records related to logged user with status 'Interpreter Scheduled'
        List<Interpretation_Request__c> requestBasedOnUser = [SELECT Id, Name, Client_Name__c, Client__c, After_Hours__c, End_DateTime__c, 
                                        Interpreter__c, Language__c, Start_DateTime__c, Status__c, Planned_Start_Date_Time__c, 
                                        Planned_End_Date_Time__c, Hours__c FROM Interpretation_Request__c where 
                                        Interpreter__c =: loggedUser.Id  AND Status__c =: interpretationReqMdt.Value__c];

        //show record with time range: Start Time in the next 15 through End time
        List<Interpretation_Request__c> toUpdate = new List<Interpretation_Request__c>();
        if(!requestBasedOnUser.isEmpty()){
            for(Interpretation_Request__c item: requestBasedOnUser){
                if(item.Planned_Start_Date_Time__c < now && now <= item.Planned_End_Date_Time__c){
                    return item.Id;
                }
            }
        }
        
       return null;
    }

    @AuraEnabled
	public static String updateRecord(Id recordId, Boolean isStart){

        List<Interpretation_Request_Settings__mdt> interpretationReqMdt = [SELECT id, MasterLabel, Value__c FROM Interpretation_Request_Settings__mdt];


        Interpretation_Request__c requestBasedOnUser = [SELECT Id, Name, Client_Name__c, Client__c, After_Hours__c, End_DateTime__c, 
                                        Interpreter__c, Language__c, Start_DateTime__c, Status__c, Planned_Start_Date_Time__c, 
                                        Planned_End_Date_Time__c, Hours__c FROM Interpretation_Request__c where Id =: recordId];
        //update Start DateTime, En DateTime, Status fields based on current DateTime
        for(Interpretation_Request_Settings__mdt inReqMtd: interpretationReqMdt){
            if(isStart == true){
                if(inReqMtd.Value__c.contains(GlobalVariable.InterReq_In_Progress)){
                    requestBasedOnUser.Start_DateTime__c = System.now();
                    requestBasedOnUser.Status__c = GlobalVariable.InterReq_In_Progress;
                }
            }else{
                if(inReqMtd.Value__c.contains(GlobalVariable.InterReq_Completed)){
                    requestBasedOnUser.End_DateTime__c = System.now();
                    requestBasedOnUser.Status__c = GlobalVariable.InterReq_Completed;
                }
            }
        }

        update requestBasedOnUser;

        return requestBasedOnUser.Id;
    }
}