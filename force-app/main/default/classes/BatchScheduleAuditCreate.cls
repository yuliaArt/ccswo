//**
// Author: Yuliia Artemenko
// Date: June 19, 2019
// Description: 
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//** 
global class BatchScheduleAuditCreate implements Schedulable {

	public String startDate;
	public String endDate;
	
	global void execute(SchedulableContext sc) {
		proccessSchedule();
	}

	private void proccessSchedule() {

		List<AuditSetting__mdt> auditSettings = BatchAuditCreateHelper.getAuditSettings();

		List<Audit_Quarter_Setting__mdt>  auditQuarterSettings = BatchAuditCreateHelper.getAuditQuarterSettings();
		Map<Decimal, List<String>> currentQuarter = BatchAuditCreateHelper.getcurrentQuarter(auditQuarterSettings);

		for(List<String> dateString: currentQuarter.values()){
			startDate = dateString[0];
			endDate = dateString[1];
		}
	
		if(!auditSettings.isEmpty()){
			Map<String, String> values = new Map<String, String>();  

			for(AuditSetting__mdt auditSetting: auditSettings){

				String dateRange = auditSetting.FieldForSelectQuarter__r.QualifiedAPIName+' >= '+startDate+' AND '+auditSetting.FieldForSelectQuarter__r.QualifiedAPIName+' <= '+endDate;
				
				if(auditSetting.Status_Value__c == null){
					values.put(auditSetting.DeveloperName, '('+auditSetting.FieldName__r.QualifiedAPIName +' = \''+auditSetting.FieldValue__c+'\' AND '+dateRange+')');		
				}else{
					values.put(auditSetting.DeveloperName,'('+auditSetting.FieldName__r.QualifiedAPIName +' = \''+auditSetting.FieldValue__c+'\' AND '+auditSetting.FieldForSelectQuarter__r.QualifiedAPIName+' = \''+auditSetting.Status_Value__c+'\')');
				}
			}	
		

			Map<String, String> mapOfQuery = new Map<String, String>();
			for(String element: values.keySet()){
				mapOfQuery.put(element, ObjectUtils.getAllFieldsFromObject(auditSettings.get(0).ObjectName__r.QualifiedAPIName ) + ' WHERE '+values.get(element)+' AND Manager_is_the_Owner__c = false');
			}
			
			//Check if there are less than 5 batches active, otherwise wait for 5 minutes and try again
			Integer numberOfBatch = [SELECT count() FROM AsyncApexJob WHERE JobType = 'BatchApex' AND Status IN('Processing', 'Preparing', 'Queued')];
			if (numberOfBatch < 5 ) {
				for(String batch: mapOfQuery.keySet()){
					BatchAuditCreate batchAudit = new BatchAuditCreate(batch, mapOfQuery.get(batch));
					Database.executeBatch(batchAudit, 200);
				}
			} 
			if (numberOfBatch >= 5 || Test.isRunningTest()) {
				BatchScheduleAuditCreate delayBachAudit = new BatchScheduleAuditCreate();
				Datetime dt = Datetime.now().addMinutes(5);
				String timeForScheduler = dt.format('s m H d M \'?\' yyyy');
				Id schedId = System.Schedule('Create Audit Batch Schedule' + timeForScheduler, timeForScheduler, delayBachAudit);
			}
		}
	}

	/* At 23:00:00pm, on the last day of the month, in March, June, September and December
		BatchScheduleAuditCreate bt = new BatchScheduleAuditCreate();
		String sch = '0 0 23 L MAR,JUN,SEP,DEC ? *';
		system.schedule('Create Audit', sch, bt);
	*/

}