//**
// Author: Yuliia Artemenko
// Date: June 19, 2019
// Description: 
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//** 
public class BatchAuditCreateHelper  {
	
	public static void insertAudit(Map<String, List<SObject>> programs){

		List<AuditSetting__mdt> auditSettings = getAuditSettings();

		Map<String, Boolean> mapOfAuditSetting = new Map<String, Boolean>();  
		for(AuditSetting__mdt auditSetting: auditSettings){
			mapOfAuditSetting.put(auditSetting.DeveloperName, auditSetting.Calculate_Service_Records__c);
		}

		List<Program_Enrollment__c> listOdPrograms = new List<Program_Enrollment__c>();
		Map<String, List<SObject>> calculateServices = new Map<String, List<SObject>>();
		Map<String, List<SObject>> sObjectsByValue = new Map<String, List<SObject>>();

	
		//calculate sObjects By Value
		for(String item: programs.keySet()){
			if(mapOfAuditSetting.containsKey(item) && mapOfAuditSetting.get(item) == true){
				calculateServices.put(item, programs.get(item));
				listOdPrograms.addAll((List<Program_Enrollment__c>)programs.get(item));
			}else{
				sObjectsByValue.put(item, programs.get(item));
			}
		}

		//check service related list in that quarter
		if(!listOdPrograms.isEmpty()){
			List<Audit_Quarter_Setting__mdt>  auditQuarterSettings = BatchAuditCreateHelper.getAuditQuarterSettings();
			Map<Decimal, List<String>> currentQuarter = BatchAuditCreateHelper.getcurrentQuarter(auditQuarterSettings);

			Date startDate;
			Date endDate;

			for(List<String> dateString: currentQuarter.values()){
				startDate = date.valueOf(dateString[0]);
				endDate = date.valueOf(dateString[1]);
			}

			List<Service__c> services = [select  id, Program_Enrollment__c, Date_of_Service__c FROM Service__c 
										where Program_Enrollment__c  IN:listOdPrograms AND Date_of_Service__c >= :startDate AND Date_of_Service__c <= :endDate];
			
			List<Id> idsOfSeervices = new List<Id>();
			for(Service__c service: services){
				idsOfSeervices.add(service.Program_Enrollment__c);
			}

			for(String sObjectRecord: calculateServices.keySet()){
				for(SObject program: calculateServices.get(sObjectRecord)){
					if(idsOfSeervices.contains(program.Id)){
						if(!sObjectsByValue.containsKey(sObjectRecord)){
							sObjectsByValue.put(sObjectRecord, new List<SObject>());
						}
						sObjectsByValue.get(sObjectRecord).add(program);
					}
				}
			}
		}
		
		//** Randomize List
		Map<String, List<SObject>> randomSObjectsByValue = new Map<String, List<SObject>>();
		for(String value: sObjectsByValue.keySet()){
			randomSObjectsByValue.put(value, ObjectUtils.randomize(sObjectsByValue.get(value)));
		}
		
		//** Get only some % from records
		Map<String, Decimal> percentByValues = new Map<String, Decimal>();  
		for(AuditSetting__mdt auditSetting: auditSettings){
			percentByValues.put(auditSetting.DeveloperName, auditSetting.PercentToShow__c);
		}
		
		List<Audit__c> audits = new List<Audit__c>();
		
		for(String value: randomSObjectsByValue.keySet()){
			if(randomSObjectsByValue.get(value).size() != 0){
				Decimal length = randomSObjectsByValue.get(value).size() * (percentByValues.get(value)/100);
				if(length != 0 && length < 1){
					length = 1;
				}
				Integer size = Math.round(length);
				for(Integer i=0; i<size; i++){
					Audit__c audit = new Audit__c();
					audit.Complete__c = percentByValues.get(value);
					audit.Date_of_Review__c = System.today();
					audit.Reviewer__c = String.valueof(randomSObjectsByValue.get(value).get(i).get('OwnerId'));
					audit.Status__c = GlobalVariable.AuditChart_In_Progress;
					audit.Program_Enrollment__c = randomSObjectsByValue.get(value).get(i).Id;
					audits.add(audit);
				}	
			}
	
		}
		if(!audits.isEmpty()){
			insert audits;
		}
	}

	public static List<AuditSetting__mdt> getAuditSettings(){

		List<AuditSetting__mdt> auditSettings = [SELECT Id, DeveloperName, ObjectName__r.QualifiedAPIName, FieldName__r.QualifiedAPIName, Calculate_Service_Records__c,
												FieldValue__c, Status_Value__c, PercentToShow__c, IsVisible__c, FieldForSelectQuarter__r.QualifiedAPIName  
												FROM AuditSetting__mdt WHERE IsVisible__c = TRUE];

		List<sObject> listOfMtd = new List<sObject>();
		for(AuditSetting__mdt element: auditSettings){
			listOfMtd.add(element);
		}
		List<AuditSetting__mdt> randomeAuditSettings = ObjectUtils.randomize(listOfMtd);
		return randomeAuditSettings;
	} 

	public static List<Audit_Quarter_Setting__mdt> getAuditQuarterSettings(){

		Integer currentMonth = System.now().month();
		List<Audit_Quarter_Setting__mdt> auditQuarterSettings = [SELECT Id, Start_Date__c, Quarter_Number__c, End_Date__c, DeveloperName  
																FROM Audit_Quarter_Setting__mdt];

		List<Audit_Quarter_Setting__mdt> listOfCurrentMtd = new List<Audit_Quarter_Setting__mdt>();
		for(Audit_Quarter_Setting__mdt auditQuarterSetting: auditQuarterSettings){
			if(auditQuarterSetting.Start_Date__c.month()<=currentMonth && currentMonth <=auditQuarterSetting.End_Date__c.month()){
				listOfCurrentMtd.add(auditQuarterSetting);
			}
		}
		return listOfCurrentMtd;
	}
	public static Map<Decimal, List<String>> getcurrentQuarter(List<Audit_Quarter_Setting__mdt> auditQuarterSettings){

		String startDate;
		String endDate;
		Integer year = System.now().year();

		Map<Decimal, List<String>> currentQuarter = new Map<Decimal, List<String>>();
		List<String> listOfDates = new List<String>();

		for(Audit_Quarter_Setting__mdt item: auditQuarterSettings){

			String sMonth = item.Start_Date__c.month() <= 9 ? '0'+ item.Start_Date__c.month() : String.valueof(item.Start_Date__c.month());
			String sDay = item.Start_Date__c.day() <= 9 ? '0'+ item.Start_Date__c.day() : String.valueof(item.Start_Date__c.day());

			String endMonth = item.End_Date__c.month() <= 9 ? '0'+ item.End_Date__c.month() : String.valueof(item.End_Date__c.month());
			String endDay = item.End_Date__c.day()<= 9 ? '0'+ item.End_Date__c.day() : String.valueof(item.End_Date__c.day());
			

			startDate = year+'-'+sMonth+'-'+sDay;
			endDate = year+'-'+endMonth+'-'+endDay;
			if(!currentQuarter.containsKey(item.Quarter_Number__c)){
				currentQuarter.put(item.Quarter_Number__c, new List<String>());
			}
			listOfDates.add(startDate);
			listOfDates.add(endDate);
			currentQuarter.get(item.Quarter_Number__c).addAll(listOfDates);

		}
		return currentQuarter;
	}
	
}