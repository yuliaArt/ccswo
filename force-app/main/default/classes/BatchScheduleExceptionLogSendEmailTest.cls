@isTest
public class BatchScheduleExceptionLogSendEmailTest  {

	@testSetup 
	public static void testSetup() {
		TestFactory.createDataForBatchExceptionLogSendEmail();
	}

	@isTest static void executeBatchScheduleAttendanceCreateTest() {
		Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
		User testUser = (User) TestFactory.createSObject(new User(ProfileId = p.Id), 'TestFactory.UserDefaults', true);
		Group groupExceptionReceivers = [SELECT Id, name FROM Group WHERE DeveloperName =: GlobalVariable.EXCEPTION_RECEIVERS_GROUP LIMIT 1];
		GroupMember groupMember = (GroupMember) TestFactory.createSObject(new GroupMember(UserOrGroupId = testUser.Id, GroupId = groupExceptionReceivers.Id), true);

		Test.startTest();
        String CRON_EXP = '0 0 0 31 12 ? 2050';
        String jobId = System.schedule('BatchScheduleExceptionLogSendEmailTest', CRON_EXP, new BatchScheduleExceptionLogSendEmail());
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
		System.assertEquals(0, ct.TimesTriggered);
		System.assertEquals('2050-12-31 00:00:00', String.valueOf(ct.NextFireTime));
		Test.stopTest();
	}
}